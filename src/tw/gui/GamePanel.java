package tw.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

import javax.swing.JPanel;

import tw.Main;
import tw.game.Level;
import tw.utils.InputHandler;
import tw.utils.SizedDrawable2D;

/**
 * JPanel in wich all game level data is drawn (Map, entities, etc)
 */
public class GamePanel extends JPanel 
{
	public static final int DEFAULT_FPS=120;
	
	private Screen associatedScreen;
	
	private SizedDrawable2D registeredToDrawLoop;
	private DrawingManager2D dm;
	private InputHandler ih;
	
	private BufferStrategy buffStrat=null;
	
	//Variables used to calculate current (Real) FPS
	private long timeCounter=System.currentTimeMillis();
	private int loopCounter=0;
	private int loopsSkipped=0;
	private int currentFPS=DEFAULT_FPS;
	private long timeElapsed=System.currentTimeMillis();
	
	private Thread printThread=null;
	
	private float scale=1f;
	
	private int MAX_FPS=DEFAULT_FPS;
	
	public GamePanel(DrawingManager2D dm,InputHandler ih)
	{
		this.dm=dm;
		this.ih=ih;
		this.setFocusTraversalKeysEnabled(false);
		this.setFocusable(true);
	}
	
	public void scaleOnCenter(boolean fullscreen)
	{
		//I dont know why this happens in windowed mode, but...
		int xaid=fullscreen?0:8;
		int yaid=fullscreen?0:32;
		
		if(this.registeredToDrawLoop!=null){
			float xratio=(float)(this.associatedScreen.getWidth()-xaid)/this.registeredToDrawLoop.getWidth();
			float yratio=(float)((this.associatedScreen.getHeight())-yaid)/this.registeredToDrawLoop.getHeight();
			if(xratio<yratio){
				this.scale=xratio;
				this.setLocation(0, (int)((this.associatedScreen.getHeight()-yaid-(this.registeredToDrawLoop.getHeight()*scale))/2));
			}
			else{
				this.scale=yratio;
				this.setLocation((int)((this.associatedScreen.getWidth()-xaid-(this.registeredToDrawLoop.getWidth()*scale))/2),0);
			}
			System.out.println(this.getLocation().x+" "+this.associatedScreen.getWidth()+" "+this.registeredToDrawLoop.getWidth()*this.scale);
			System.out.println(this.getLocation().y+" "+this.associatedScreen.getHeight()+" "+this.registeredToDrawLoop.getHeight()*this.scale);
			System.out.println(this.getLocation().x+" "+this.getLocation().y+
					" "+this.scale);
			
			setSize((int)(this.registeredToDrawLoop.getWidth()*this.scale),(int)(this.registeredToDrawLoop.getHeight()*this.scale));
		}
		else{
			setSize(this.associatedScreen.getSize());
		}
	}
	/**
	 * Registers a Drawable object to call draw() on him in the paint loop
	 */
	public void registerInDrawLoop(SizedDrawable2D instance)
	{
		this.registeredToDrawLoop=instance;
		if(instance!=null) scaleOnCenter(this.associatedScreen.isFullScreen());
	}
	
	public void setAssociatedScreen(Screen s){
		this.associatedScreen=s;
		scaleOnCenter(this.associatedScreen.isFullScreen());
		this.associatedScreen.addKeyListener(ih);
		
		
		if(this.associatedScreen.isFullScreenNative())
		{
			this.associatedScreen.createBufferStrategy(3);
	    
			this.buffStrat = this.associatedScreen.getBufferStrategy();
	    
			this.printThread=new Thread((new Runnable(){public void run(){while(true) {onPaint(null);}}}));
			this.printThread.start();
		}
	}
	
	public float getScale()
	{
		return this.scale;
	}
	
	public void interruptPrintThread()
	{
		if(this.printThread!=null) this.printThread.interrupt();
		//What the fuck
		else{
			if(this.associatedScreen.isFullScreenNative())
			{
				this.associatedScreen.createBufferStrategy(3);
		    
				this.buffStrat = this.associatedScreen.getBufferStrategy();
		    
				this.printThread=new Thread((new Runnable(){public void run(){while(true) {onPaint(null);}}}));
				this.printThread.run();
			}
		}
	}
	
	/**
	 * Gets the drawing manager
	 */
	public DrawingManager2D getDrawingManager()
	{
		return this.dm;
	}
	
	public InputHandler getInputHandler()
	{
		return this.ih;
	}
	
	/**
	 * Only works if there is not a BufferStrategy defined (java windowed mode, java emulated full screen)
	 */
	public void paint(Graphics g)
	{
		if(this.buffStrat!=null) super.paint(g);
		else {
			onPaint(g);
			repaint();
		}
	}
	
	/**
	 * Paints one game frame in a graphics <g> object. If this object is null, uses the double buffered BufferStrategy created
	 * Graphics != null -> java windowed mode, java emulated full screen
	 * Graphics == null -> native full screen ON, VSYNC ON
	 */
	public void onPaint(Graphics g)
	{	
		Graphics gr=g==null?this.buffStrat.getDrawGraphics():g;
		if(this.buffStrat!=null) gr.fillRect(0, 0, getWidth(), getHeight());
		this.loopCounter++;
		long initTime=System.currentTimeMillis();
		if(initTime>this.timeCounter+1000)
		{
			this.currentFPS=(int) ((1000*(this.loopCounter-this.loopsSkipped))/(initTime-timeCounter));
			System.out.println("DRAW FPS: "+this.currentFPS+" skipped "+this.loopsSkipped);
			this.loopCounter=0;
			this.loopsSkipped=0;
			this.timeCounter=initTime;
		}
		
		long currentTime=System.currentTimeMillis();
		if(!this.associatedScreen.isFullScreenNative())((Graphics2D) gr).scale(this.scale, this.scale); //|TODO graphically ugly
		boolean skipped=true;
		if(currentTime-this.timeElapsed!=0){
			if(this.registeredToDrawLoop!=null) {
				try{
					skipped=false;
					gr.fillRect(0, 0, this.getWidth(), this.getHeight()); //Screen to black
					boolean draw=true;
					if(this.registeredToDrawLoop instanceof Level){
						if(((Level)(this.registeredToDrawLoop)).gameEnded()) draw=false;
					}
					if(draw) this.registeredToDrawLoop.draw2D(dm, (Graphics2D) gr,(currentTime-this.timeElapsed)/1000f);
					if(this.associatedScreen.isFullScreenNative()){
						gr.fillRect(0, 750, this.getWidth(), 758);
						gr.fillRect(1020, 0, 1024, this.getHeight());
					}
				}
				catch(Exception e)
				{
					System.err.println("An error has ocurred. Game will try to continue.");
					System.err.println("A error dump has been created in "+Main.errorDumpFile.getAbsolutePath());
					System.err.println("GAME PANEL ERROR");
					e.printStackTrace(System.err);
					//By default, drawing again and NOT restarting level
					/*if(this.registeredToDrawLoop instanceof Level) ((Level) this.registeredToDrawLoop).setGameEnded();
					else{
						System.err.println("Impossible to recover from the error. Game will exit now");
						System.exit(1);
					}*/
				}
			}
		}
		else loopsSkipped++;
		this.timeElapsed=currentTime;
		
		//Setting FPS
		try 
		{
			//long waitTime=(1000/this.MAX_FPS)-(System.currentTimeMillis()-initTime)-1;
			long waitTime=(this.timeCounter+(int)(this.loopCounter*1000/this.MAX_FPS))-System.currentTimeMillis();
			if(this.buffStrat!=null){
				gr.dispose();
				if(!this.buffStrat.contentsRestored()&&!skipped){ //If the content has been restored (Or the frame has been skipped) we draw nothing
					if(this.buffStrat!=null) this.buffStrat.show();
					//If the contents have been lost we dont sync
					if(this.buffStrat.contentsLost()) Toolkit.getDefaultToolkit().sync(); //Syncs the BufferStrategy with the monitor to avoid stuttering
				}
			}
			if(waitTime>0) Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			//Interrupted, repainting
		}
		//repaint();
	}
}
