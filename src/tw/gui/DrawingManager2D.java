package tw.gui;

import java.awt.Color;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import tw.Main;
import tw.entities.Bullet;
import tw.entities.Character.Direction;
import tw.entities.Entity;
import tw.entities.Character;
import tw.entities.EntityType.Faction;
import tw.entities.IAControlledCharacter;
import tw.gui.animation.AnimationMetrics2D;

/**
 * Manages all drawing operations in a Java2D environment
 */
public class DrawingManager2D extends DrawingManager
{
	public enum ImageID{SOLDIER,BIGSPIDER,SPIDER,TANK,ANIMATIONS,DIRECTEDANIMATIONS};
	public enum MetricDirections
	{
		ALL,LATERAL,ONE,NO;
		public int getNum(){
			switch(this){
			case ALL: return 8;
			case LATERAL: return 4;
			case ONE: return 1;
			case NO: return 0;
			default: return -1;
			}
		}
	};
	
	public Color mousePowerflowColor=null; //|TODO testing
	
	public static final int TILE_SIZE=30;
	private static final String ASSET_MAPTILES="/assets/maptiles_original.png";
	
	private Image mapTiles;
	private Image[] gameImages=new Image[ImageID.values().length];
	
	private BufferedImage precomputedMap;
	private int precomputedMapOffsetX=0;
	private int precomputedMapOffsetY=0;
	
	public DrawingManager2D() throws IOException
	{
		//Loading assets
		this.mapTiles=ImageIO.read(Main.class.getResource(ASSET_MAPTILES));
		this.gameImages[ImageID.BIGSPIDER.ordinal()]=ImageIO.read(Main.class.getResource("/assets/big_spider_arreglado.png")); //TODO TEST
		this.gameImages[ImageID.SOLDIER.ordinal()]=ImageIO.read(Main.class.getResource("/assets/soldier.png")); //TODO TEST
		this.gameImages[ImageID.SPIDER.ordinal()]=ImageIO.read(Main.class.getResource("/assets/spider.png")); //TODO TEST
		this.gameImages[ImageID.TANK.ordinal()]=ImageIO.read(Main.class.getResource("/assets/tank.png")); //TODO TEST
		this.gameImages[ImageID.ANIMATIONS.ordinal()]=ImageIO.read(Main.class.getResource("/assets/animation.png")); //TODO TEST
		this.gameImages[ImageID.DIRECTEDANIMATIONS.ordinal()]=ImageIO.read(Main.class.getResource("/assets/directedAnimations.png")); //TODO TEST
	}
	
	/**
	 * Precomputes map, storing it in memory
	 */
	public void preComputeMap(byte[][] tiles)
	{
		BufferedImage buffered = new BufferedImage(TILE_SIZE*tiles.length,TILE_SIZE*tiles[0].length,BufferedImage.TYPE_3BYTE_BGR);
		drawMap(tiles,(Graphics2D) buffered.getGraphics());
		this.precomputedMap=buffered;
	}
	
	/**
	 * Draws precomputed map if exists. If not, draws nothing and writes a error message to stderr
	 */
	public void drawPreComputedMap(Graphics2D g)
	{
		if(this.precomputedMap==null){
			System.err.println("WARNING: Precomputed map is null");
			return;
		}
		g.drawImage(this.precomputedMap,0,0,null);

	}
	
	/**
	 * Draws the map resulting from the tiles in <tiles> in the graphics <g>
	 */
	public void drawMap(byte[][] tiles,Graphics2D g)
	{
		for(int w=0;w<tiles.length;w++)
		{
			for(int h=0;h<tiles[0].length;h++)
			{
				int tilval=tiles[w][h]<0?tiles[w][h]+256:tiles[w][h];
				int tilex=(tilval%16)*TILE_SIZE; //Img is 16*32 x 16*32 big
				int tiley=(tilval/16)*TILE_SIZE;
				g.drawImage(this.mapTiles, 					//Source img
						w*TILE_SIZE, h*TILE_SIZE,  			//Screen ix, iy drawing coordinates
						(w+1)*TILE_SIZE, (h+1)*TILE_SIZE, 	//Screen fx, fy drawing coordinates
						tilex, tiley, 						//Source img ix, iy tile coordinates
						tilex+TILE_SIZE, tiley+TILE_SIZE, 	//Source img fx, fy tile coordinates
						null);
			}
		}
	}
	
	public void drawCharacterUsingMetrics(IAControlledCharacter c,Metrics2D metrics,Graphics2D g)
	{
		int y=0;
		int x=(int)(c.getInternalCounter()*c.getVel()/metrics.ANIMMOD)%metrics.ANIMNUM;
		int ystop=0; int xstop=0;
    	switch(c.getLookingDirection())
    	{
    	case RIGHT:
    		y=metrics.SIZE*metrics.ANIMSTARTROW;
    		ystop=0/metrics.STAYINGTILESPERROW; xstop=0%metrics.STAYINGTILESPERROW;
    		break;
    	case UP:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+1);
    		ystop=1/metrics.STAYINGTILESPERROW; xstop=1%metrics.STAYINGTILESPERROW;
    		break;
    	case DOWN:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+2);
    		ystop=2/metrics.STAYINGTILESPERROW; xstop=2%metrics.STAYINGTILESPERROW;
    		break;
    	case LEFT:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+3);
    		ystop=3/metrics.STAYINGTILESPERROW; xstop=3%metrics.STAYINGTILESPERROW;
    		break;
    	case UPRIGHT:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+4);
    		ystop=4/metrics.STAYINGTILESPERROW; xstop=4%metrics.STAYINGTILESPERROW;
    		break;
    	case DOWNRIGHT:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+5);
    		ystop=5/metrics.STAYINGTILESPERROW; xstop=5%metrics.STAYINGTILESPERROW;
    		break;
    	case UPLEFT:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+6);
    		ystop=6/metrics.STAYINGTILESPERROW; xstop=6%metrics.STAYINGTILESPERROW;
    		break;
    	case DOWNLEFT:
    		y=metrics.SIZE*(metrics.ANIMSTARTROW+7);
    		ystop=7/metrics.STAYINGTILESPERROW; xstop=7%metrics.STAYINGTILESPERROW;
    		break;	
    	}
    	
    	if(c.getObjDir()==null){
    		x=xstop; y=ystop*metrics.SIZE;
    	}
    	
    	int ix=(int)(c.getX()*TILE_SIZE-(metrics.COLX*c.getSize()*TILE_SIZE/metrics.COLSIZE));
    	int iy=(int)(c.getY()*TILE_SIZE-(metrics.COLY*c.getSize()*TILE_SIZE/metrics.COLSIZE));
    	int fx=(int)((c.getX()+c.getSize())*TILE_SIZE+((metrics.SIZE-(metrics.COLX+metrics.COLSIZE))*c.getSize()*TILE_SIZE/metrics.COLSIZE));
    	int fy=(int)((c.getY()+c.getSize())*TILE_SIZE+((metrics.SIZE-(metrics.COLY+metrics.COLSIZE))*c.getSize()*TILE_SIZE/metrics.COLSIZE));
    	
    	/*g.drawRect(//this.soldierImage, 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx-ix,fy-iy 	//Screen fx, fy drawing coordinates
				//x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);
    	g.drawRect(//this.BIGSPIDERImage, 					//Source img
				(int)(c.getX()*TILE_SIZE),(int)(c.getY()*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)(c.getSize()*TILE_SIZE),(int)(c.getSize()*TILE_SIZE) 	//Screen fx, fy drawing coordinates
				//x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);*/
    	g.drawImage(this.gameImages[metrics.IMAGE_ID.ordinal()], 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx,fy, 	//Screen fx, fy drawing coordinates
				x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				null);
	}
	
	public void drawText(String text,float x,float y,float size,Graphics2D g)
	{
		this.drawText(text, x, y, size,null, g);
	}
	public void drawText(String text,float x,float y,float size,Color c,Graphics2D g)
	{
		Color savedColor=null;
		if(c!=null){
			savedColor=g.getColor();
			g.setColor(c);
		}
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON ); // SETTING ANTIALIASING ON TO SOOTHE BORDERS OF THE TEXT
		g.setFont(new Font("SansSerif", Font.BOLD, (int)Math.ceil(size*TILE_SIZE))); 
		g.drawString(text, x*TILE_SIZE, y*TILE_SIZE);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF ); //SETTING ANTIALIASING OFF TO IMPROVE PERFORMANCE
		
		if(c!=null) g.setColor(savedColor);
	}
	
	public void drawAnimation(int frame,float x,float y,float size,AnimationMetrics2D metrics,Graphics2D g)
	{
		g.drawImage(this.gameImages[ImageID.ANIMATIONS.ordinal()], 					//Source img
				(int)(x*TILE_SIZE),(int)(y*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)((x+size)*TILE_SIZE),(int)((y+size*((float)(metrics.SIZEY)/metrics.SIZEX))*TILE_SIZE), 	//Screen fx, fy drawing coordinates
				(int)(metrics.STARTX+frame*metrics.SIZEX), metrics.STARTY, 						//Source img ix, iy tile coordinates
				(int)(metrics.STARTX+(frame+1)*metrics.SIZEX), metrics.STARTY+metrics.SIZEY, 	//Source img fx, fy tile coordinates
				null);
	}
	public void drawAnimation(int frame,float x,float y,float size,DirectedAnimationMetrics2D metrics,Direction d,Graphics2D g)
	{
		int sourcex=metrics.XSTART+frame*metrics.SIZE;
		int sourcey=metrics.YSTART;
		if(metrics.DIRECTIONS!=MetricDirections.ONE)
		{
			switch(d)
	    	{
	    	case RIGHT:
	    		sourcey+=metrics.SIZE*0;
	    		break;
	    	case UP:
	    		sourcey+=metrics.SIZE*1;
	    		break;
	    	case DOWN:
	    		sourcey+=metrics.SIZE*2;
	    		break;
	    	case LEFT:
	    		sourcey+=metrics.SIZE*3;
	    		break;
	    	case UPRIGHT:
	    		sourcey+=metrics.DIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*0 :
	    															metrics.SIZE*4 ;
	    		break;
	    	case DOWNRIGHT:
	    		sourcey+=metrics.DIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*0 :
																	metrics.SIZE*5 ;
	    		break;
	    	case UPLEFT:
	    		sourcey+=metrics.DIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*3 :
																	metrics.SIZE*6 ;
	    		break;
	    	case DOWNLEFT:
	    		sourcey+=metrics.DIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*3 :
																	metrics.SIZE*7 ;
	    		break;	
	    	}
		}
		//System.out.println(sourcey);
		g.drawImage(this.gameImages[metrics.IMAGE_ID.ordinal()], 					//Source img
				(int)(x*TILE_SIZE),(int)(y*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)((x+size)*TILE_SIZE),(int)((y+size*((float)(metrics.SIZE)/metrics.SIZE))*TILE_SIZE), 	//Screen fx, fy drawing coordinates
				sourcex, sourcey, 						//Source img ix, iy tile coordinates
				sourcex+metrics.SIZE, sourcey+metrics.SIZE, 	//Source img fx, fy tile coordinates
				null);
	}
	public void drawDeathFrameUsingMetrics(int frame,float animx,float animy,float animscale,Direction dir,Metrics2D metrics,Graphics2D g)
	{
		if(metrics.DEATHANIMNUM==1) drawDeathFrameUsingMetricsUnisize(frame,animx,animy,animscale,dir,metrics,g);
		else  drawDeathFrameUsingMetricsMultisize(frame,animx,animy,animscale,dir,metrics,g);
	}
	private void drawDeathFrameUsingMetricsMultisize(int frame,float animx,float animy,float animscale,Direction dir,Metrics2D metrics,Graphics2D g)
	{
		if(frame>=metrics.DEATHANIMNUM) frame=metrics.DEATHANIMNUM-1;
		
		int y=0;
		int x=frame;
		
		if(metrics.DEATHDIRECTIONS==MetricDirections.ONE) y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
		else
		{
			switch(dir)
	    	{
	    	case RIGHT:
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
	    		break;
	    	case UP:
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+9);
	    		break;
	    	case DOWN:
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+10);
	    		break;
	    	case LEFT:
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+11);
	    		break;
	    	case UPRIGHT:
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
	    															metrics.SIZE*(metrics.ANIMSTARTROW+12) ;
	    		break;
	    	case DOWNRIGHT:
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+13) ;
	    		break;
	    	case UPLEFT:
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+11) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+14) ;
	    		break;
	    	case DOWNLEFT:
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+11) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+15) ;
	    		break;	
	    	}
		}
    	
    	int ix=(int)(animx*TILE_SIZE-(metrics.COLX*animscale*TILE_SIZE/metrics.COLSIZE));
    	int iy=(int)(animy*TILE_SIZE-(metrics.COLY*animscale*TILE_SIZE/metrics.COLSIZE));
    	int fx=(int)((animx+animscale)*TILE_SIZE+((metrics.SIZE-(metrics.COLX+metrics.COLSIZE))*animscale*TILE_SIZE/metrics.COLSIZE));
    	int fy=(int)((animy+animscale)*TILE_SIZE+((metrics.SIZE-(metrics.COLY+metrics.COLSIZE))*animscale*TILE_SIZE/metrics.COLSIZE));
    	
    	/*g.drawRect(//this.soldierImage, 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx-ix,fy-iy 	//Screen fx, fy drawing coordinates
				//x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);
    	g.drawRect(//this.BIGSPIDERImage, 					//Source img
				(int)(c.getX()*TILE_SIZE),(int)(c.getY()*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)(c.getSize()*TILE_SIZE),(int)(c.getSize()*TILE_SIZE) 	//Screen fx, fy drawing coordinates
				//x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);*/
    	g.drawImage(this.gameImages[metrics.IMAGE_ID.ordinal()], 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx,fy, 	//Screen fx, fy drawing coordinates
				x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				null);
	}
	private void drawDeathFrameUsingMetricsUnisize(int frame,float animx,float animy,float animscale,Direction dir,Metrics2D metrics,Graphics2D g)
	{
		if(frame>=metrics.DEATHANIMNUM) frame=metrics.DEATHANIMNUM-1;
		
		int y=0;
		int x=frame;
		
		if(metrics.DEATHDIRECTIONS==MetricDirections.ONE) y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
		else
		{
			switch(dir)
	    	{
	    	case RIGHT:
	    		x=(0);
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
	    		break;
	    	case UP:
	    		x=(1);
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
	    		break;
	    	case DOWN:
	    		x=(2);
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
	    		break;
	    	case LEFT:
	    		x=(3);
	    		y=metrics.SIZE*(metrics.ANIMSTARTROW+8);
	    		break;
	    	case UPRIGHT:
	    		x=(0);
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
	    															metrics.SIZE*(metrics.ANIMSTARTROW+9);
	    		break;
	    	case DOWNRIGHT:
	    		x=(1);
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+9);
	    		break;
	    	case UPLEFT:
	    		x=(2);
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+9);
	    		break;
	    	case DOWNLEFT:
	    		x=(3);
	    		y=metrics.DEATHDIRECTIONS==MetricDirections.LATERAL? metrics.SIZE*(metrics.ANIMSTARTROW+8) :
																	metrics.SIZE*(metrics.ANIMSTARTROW+9);
	    		break;	
	    	}
		}
    	
    	int ix=(int)(animx*TILE_SIZE-(metrics.COLX*animscale*TILE_SIZE/metrics.COLSIZE));
    	int iy=(int)(animy*TILE_SIZE-(metrics.COLY*animscale*TILE_SIZE/metrics.COLSIZE));
    	int fx=(int)((animx+animscale)*TILE_SIZE+((metrics.SIZE-(metrics.COLX+metrics.COLSIZE))*animscale*TILE_SIZE/metrics.COLSIZE));
    	int fy=(int)((animy+animscale)*TILE_SIZE+((metrics.SIZE-(metrics.COLY+metrics.COLSIZE))*animscale*TILE_SIZE/metrics.COLSIZE));
    	
    	/*g.drawRect(//this.soldierImage, 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx-ix,fy-iy 	//Screen fx, fy drawing coordinates
				//x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);
    	g.drawRect(//this.BIGSPIDERImage, 					//Source img
				(int)(c.getX()*TILE_SIZE),(int)(c.getY()*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)(c.getSize()*TILE_SIZE),(int)(c.getSize()*TILE_SIZE) 	//Screen fx, fy drawing coordinates
				//x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);*/
    	g.drawImage(this.gameImages[metrics.IMAGE_ID.ordinal()], 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx,fy, 	//Screen fx, fy drawing coordinates
				x*metrics.SIZE, y, 						//Source img ix, iy tile coordinates
				(x+1)*metrics.SIZE, y+metrics.SIZE, 	//Source img fx, fy tile coordinates
				null);
	}
	/*public void drawSoldier(IAControlledCharacter c,Graphics2D g)
	{
		
	}*/
	/*public void drawBigSpider(IAControlledCharacter c,Graphics2D g)
	{
		int y=0;
		int x=(int)(c.getInternalCounter()*Metrics2D.BIGSPIDER_ANIMMOD)%Metrics2D.BIGSPIDER_ANIMNUM;
    	switch(c.getLookingDirection())
    	{
    	case RIGHT:
    		y=Metrics2D.BIGSPIDER_SIZE*0;
    		break;
    	case UP:
    		y=Metrics2D.BIGSPIDER_SIZE*1;
    		break;
    	case DOWN:
    		y=Metrics2D.BIGSPIDER_SIZE*2;
    		break;
    	case LEFT:
    		y=Metrics2D.BIGSPIDER_SIZE*3;
    		break;
    	case DOWNRIGHT:
    		y=Metrics2D.BIGSPIDER_SIZE*4;
    		break;
    	case UPRIGHT:
    		y=Metrics2D.BIGSPIDER_SIZE*5;
    		break;
    	case UPLEFT:
    		y=Metrics2D.BIGSPIDER_SIZE*6;
    		break;
    	case DOWNLEFT:
    		y=Metrics2D.BIGSPIDER_SIZE*7;
    		break;	
    	}
    	
    	int ix=(int)(c.getX()*TILE_SIZE-(Metrics2D.BIGSPIDER_COLX*c.getSize()*TILE_SIZE/Metrics2D.BIGSPIDER_COLSIZE));
    	int iy=(int)(c.getY()*TILE_SIZE-(Metrics2D.BIGSPIDER_COLY*c.getSize()*TILE_SIZE/Metrics2D.BIGSPIDER_COLSIZE));
    	int fx=(int)((c.getX()+c.getSize())*TILE_SIZE+((Metrics2D.BIGSPIDER_SIZE-(Metrics2D.BIGSPIDER_COLX+Metrics2D.BIGSPIDER_COLSIZE))*c.getSize()*TILE_SIZE/Metrics2D.BIGSPIDER_COLSIZE));
    	int fy=(int)((c.getY()+c.getSize())*TILE_SIZE+((Metrics2D.BIGSPIDER_SIZE-(Metrics2D.BIGSPIDER_COLY+Metrics2D.BIGSPIDER_COLSIZE))*c.getSize()*TILE_SIZE/Metrics2D.BIGSPIDER_COLSIZE));
    	
    	/*g.drawRect(//this.BIGSPIDERImage, 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx-ix,fy-iy 	//Screen fx, fy drawing coordinates
				//x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);
    	g.drawRect(//this.BIGSPIDERImage, 					//Source img
				(int)(c.getX()*TILE_SIZE),(int)(c.getY()*TILE_SIZE),  			//Screen ix, iy drawing coordinates
				(int)(c.getSize()*TILE_SIZE),(int)(c.getSize()*TILE_SIZE) 	//Screen fx, fy drawing coordinates
				//x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				//(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				//null);
				);*/
    	/*g.drawImage(this.bigSpiderImage, 					//Source img
				ix,iy,  			//Screen ix, iy drawing coordinates
				fx,fy, 	//Screen fx, fy drawing coordinates
				x*Metrics2D.BIGSPIDER_SIZE, y, 						//Source img ix, iy tile coordinates
				(x+1)*Metrics2D.BIGSPIDER_SIZE, y+Metrics2D.BIGSPIDER_SIZE, 	//Source img fx, fy tile coordinates
				null);
	}*/
	//METHODS USEFUL FOR TESTING
	/**
	 * Draws the map resulting from the tiles in <tiles> in the graphics <g>
	 */
	public void highlightTile(int x,int y,Graphics2D g)
	{
		g.setColor(new Color(255,0,0,100));
        g.fillRect(x*TILE_SIZE, y*TILE_SIZE, TILE_SIZE, TILE_SIZE);
	}
	public void drawContornOfTestPlayer(Character e,Graphics2D g)
	{
		g.setColor(Color.MAGENTA);
        g.fillRect((int)(e.getX()*TILE_SIZE), (int)(e.getY()*TILE_SIZE)-20, (int)(e.getSize()*TILE_SIZE*e.getLifePercent()), 10);
	}
	public void drawContornOf(Entity e,Graphics2D g)
	{
		if(e.getType().getFaction()==Faction.MARINE)g.setColor(Color.BLUE);
		else if(e.getType().getFaction()==Faction.ALIEN)g.setColor(new Color(165,42,42));
		else g.setColor(Color.WHITE);
        g.drawRect((int)(e.getX()*TILE_SIZE), (int)(e.getY()*TILE_SIZE), (int)(e.getSize()*TILE_SIZE), (int)(e.getSize()*TILE_SIZE));
        
        if(e instanceof Character)
        {
        	Character c=(Character) e;
        	float ix=c.getX()+(c.getSize()/2);
        	float iy=c.getY()+(c.getSize()/2);
        	float fx=ix;
        	float fy=iy;
        	switch(c.getLookingDirection())
        	{
        	case UP:
        		fy=c.getY();
        		break;
        	case DOWN:
        		fy=c.getY()+c.getSize();
        		break;
        	case LEFT:
        		fx=c.getX();
        		break;
        	case RIGHT:
        		fx=c.getX()+c.getSize();
        		break;
        	case UPRIGHT:
        		fy=c.getY();
        		fx=c.getX()+c.getSize();
        		break;
        	case UPLEFT:
        		fy=c.getY();
        		fx=c.getX();
        		break;
        	case DOWNRIGHT:
        		fy=c.getY()+c.getSize();
        		fx=c.getX()+c.getSize();
        		break;
        	case DOWNLEFT:
        		fy=c.getY()+c.getSize();
        		fx=c.getX();
        		break;	
        	}
        	g.drawLine((int)(ix*TILE_SIZE),(int)(iy*TILE_SIZE), (int)(fx*TILE_SIZE),(int)(fy*TILE_SIZE));
        }
	}
	public void drawContornOf(Bullet b,Graphics2D g)
	{
		if(b.getFaction()==Faction.MARINE) g.setColor(Color.YELLOW);
		else if(b.getFaction()==Faction.ALIEN) g.setColor(Color.DARK_GRAY);
		else g.setColor(this.mousePowerflowColor);
		//FILLOVAL IS COSTLY!
		//g.fillOval((int)(b.getX()*TILE_SIZE), (int)(b.getY()*TILE_SIZE), 5, 5);
        g.drawRect((int)(b.getX()*TILE_SIZE)-1, (int)(b.getY()*TILE_SIZE)-1, 3,3);
        g.drawRect((int)(b.getX()*TILE_SIZE), (int)(b.getY()*TILE_SIZE), 1,1);
        //g.drawRect((int)(e.getX()*TILE_SIZE), (int)(e.getY()*TILE_SIZE), (int)(e.getSize()*TILE_SIZE), (int)(e.getSize()*TILE_SIZE));
	}
}