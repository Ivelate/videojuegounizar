package tw.gui;

import tw.gui.DrawingManager2D.MetricDirections;
import tw.gui.DrawingManager2D.ImageID;
import tw.gui.animation.AnimationMetrics2D;

public enum DirectedAnimationMetrics2D 
{
	ALIENSPLIT(ImageID.DIRECTEDANIMATIONS,14,0,0,0.2f,MetricDirections.ALL,2,1),
	BULLETSPAWN(ImageID.DIRECTEDANIMATIONS,17,0,112,0.02f,MetricDirections.ALL,2,1),
	PLASMABULLET(ImageID.DIRECTEDANIMATIONS,5,0,248,0.2f,MetricDirections.ONE,2,1),
	NORMALBULLET(ImageID.DIRECTEDANIMATIONS,4,10,248,0.2f,MetricDirections.ONE,2,1),
	FIREBULLET(ImageID.ANIMATIONS,17,0,587,0.02f,MetricDirections.ONE,10,1);
	
	public final ImageID IMAGE_ID;
	public final int SIZE;
	public final int XSTART;
	public final int YSTART;
	public final float ANIMMOD;
	public final MetricDirections DIRECTIONS;
	public final int ANIMNUM;
	public final int DIRECTIONSPERROW;
	private DirectedAnimationMetrics2D(ImageID imageID,int size,int xstart,int ystart,float animmod,MetricDirections directions,int animnum,int directionsPerRow){
		IMAGE_ID=imageID;
		SIZE=size;
		ANIMMOD=animmod;
		XSTART=xstart;
		YSTART=ystart;
		DIRECTIONS=directions;
		ANIMNUM=animnum;
		DIRECTIONSPERROW=directionsPerRow;
	}
}
