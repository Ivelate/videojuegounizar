package tw.gui;

import tw.gui.DrawingManager2D.MetricDirections;
import tw.gui.DrawingManager2D.ImageID;

public enum Metrics2D 
{
	SOLDIER(	DrawingManager2D.ImageID.SOLDIER,
				32,
				0.282f,
				6,
				4,
				2,
				22,
				5,
				9,
				4,
				MetricDirections.LATERAL),
				
	BIGSPIDER(	DrawingManager2D.ImageID.BIGSPIDER,
				62,
				0.4f,
				4,
				4,
				2,
				48,
				8,
				8,
				1,
				MetricDirections.ALL),
	
	SPIDER(	DrawingManager2D.ImageID.SPIDER,
			36,
			0.35f,
			4,
			4,
			2,
			31,
			2,
			3,
			4,
			MetricDirections.ALL),
			
	TANK(	DrawingManager2D.ImageID.TANK,
			42,
			0.4f,
			2,
			2,
			4,
			40,
			1,
			1,
			0,
			MetricDirections.ALL);
	
	
	public final ImageID IMAGE_ID;
	public final int SIZE;
	public final float ANIMMOD;
	public final int ANIMNUM;
	public final int STAYINGTILESPERROW;
	public final int ANIMSTARTROW;
	public final int COLSIZE;
	public final int COLX;
	public final int COLY;
	public final int DEATHANIMNUM;
	public final MetricDirections DEATHDIRECTIONS;
	private Metrics2D(ImageID imageID,int size,float animmod,int animnum,int stayingtilesperrow,int animstartrow,int colsize,int colx,int coly,int deathanimnum,MetricDirections deathDirections){
		IMAGE_ID=imageID;
		SIZE=size;
		ANIMMOD=animmod;
		ANIMNUM=animnum;
		STAYINGTILESPERROW=stayingtilesperrow;
		ANIMSTARTROW=animstartrow;
		COLSIZE=colsize;
		COLX=colx;
		COLY=coly;
		DEATHANIMNUM=deathanimnum;
		DEATHDIRECTIONS=deathDirections;
	}
}