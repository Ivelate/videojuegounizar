package tw.gui.animation;

import java.awt.Graphics2D;

import tw.entities.Character.Direction;
import tw.game.Level;
import tw.gui.DrawingManager2D;

public class DefaultAnimation extends Animation
{
	private AnimationMetrics2D metrics;
	protected float x;
	protected float y;
	private float size;
	private boolean drawCentered;
	
	public DefaultAnimation(float x,float y,float size,AnimationMetrics2D metrics,boolean centered){
		this.metrics=metrics;
		this.x=x;
		this.y=y;
		this.size=size;
		this.drawCentered=centered;
	}
	public DefaultAnimation(float x,float y,float size,AnimationMetrics2D metrics){
		this(x,y,size,metrics,false);
	}
	public DefaultAnimation(float x,float y,AnimationMetrics2D metrics){
		this(x,y,(float)(metrics.SIZEX)/DrawingManager2D.TILE_SIZE,metrics,false);
	}
	public DefaultAnimation(float x,float y,AnimationMetrics2D metrics,boolean centered){
		this(x,y,(float)(metrics.SIZEX)/DrawingManager2D.TILE_SIZE,metrics,centered);
	}
	@Override
	public void onDispose(Level l) {
		//MANUALLY OVERRIDE	IF NEEDED	
	}

	@Override
	protected boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl) {
		float x=drawCentered?this.x-this.size/2:this.x;
		float y=drawCentered?this.y-this.size/2:this.y;
		dm.drawAnimation((int)(this.getTotalTimeElapsed()/metrics.ANIMMOD), x, y, this.size, metrics, g);
		return this.getTotalTimeElapsed()>this.metrics.ANIMMOD*this.metrics.FRAMESNUM;
	}
	@Override
	protected void concreteUpdate(float totalTimeElapsed, Level l) {
		//MANUALLY OVERRIDE	IF NEEDED
	}

}
