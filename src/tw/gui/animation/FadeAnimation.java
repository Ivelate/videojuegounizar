package tw.gui.animation;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;

import tw.game.Level;
import tw.gui.DrawingManager2D;

public class FadeAnimation extends Animation
{
	private AnimationMetrics2D metrics;
	protected float x;
	protected float y;
	private float size;
	private boolean drawCentered;
	private float fadeInTime;
	private float currentAlpha=0f;
	
	public FadeAnimation(float fadeinTime,float x,float y,float size,AnimationMetrics2D metrics,boolean centered){
		this.metrics=metrics;
		this.x=x;
		this.y=y;
		this.size=size;
		this.drawCentered=centered;
		this.fadeInTime=fadeinTime;
	}
	public FadeAnimation(float fadeinTime,float x,float y,float size,AnimationMetrics2D metrics){
		this(fadeinTime,x,y,size,metrics,false);
	}
	public FadeAnimation(float fadeinTime,float x,float y,AnimationMetrics2D metrics){
		this(fadeinTime,x,y,(float)(metrics.SIZEX)/DrawingManager2D.TILE_SIZE,metrics,false);
	}
	public FadeAnimation(float fadeinTime,float x,float y,AnimationMetrics2D metrics,boolean centered){
		this(fadeinTime,x,y,(float)(metrics.SIZEX)/DrawingManager2D.TILE_SIZE,metrics,centered);
	}
	@Override
	public void onDispose(Level l) {
		//MANUALLY OVERRIDE	IF NEEDED	
	}

	@Override
	protected boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl) {
		float x=drawCentered?this.x-this.size/2:this.x;
		float y=drawCentered?this.y-this.size/2:this.y;
		if(this.currentAlpha<0.999f)g.setComposite(AlphaComposite.SrcOver.derive(this.currentAlpha));
		dm.drawAnimation((int)(this.getTotalTimeElapsed()/metrics.ANIMMOD), x, y, this.size, metrics, g);
		g.setComposite(AlphaComposite.SrcOver.derive(1.0f));
		return this.getTotalTimeElapsed()>this.metrics.ANIMMOD*this.metrics.FRAMESNUM;
	}
	@Override
	protected void concreteUpdate(float totalTimeElapsed, Level l) {
		this.currentAlpha=totalTimeElapsed/fadeInTime;
		if(this.currentAlpha>1) this.currentAlpha=1;
	}
}
