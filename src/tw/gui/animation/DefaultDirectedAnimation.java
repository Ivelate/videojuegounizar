package tw.gui.animation;

import java.awt.Graphics2D;

import tw.entities.Character.Direction;
import tw.game.Level;
import tw.gui.DirectedAnimationMetrics2D;
import tw.gui.DrawingManager2D;

public class DefaultDirectedAnimation extends Animation
{
	private DirectedAnimationMetrics2D metrics;
	private Direction direction;
	protected float x;
	protected float y;
	private float size;
	private boolean drawCentered;
	
	public DefaultDirectedAnimation(float x,float y,float size,DirectedAnimationMetrics2D metrics,Direction direction,boolean centered){
		this.metrics=metrics;
		this.direction=direction;
		this.x=x;
		this.y=y;
		this.size=size;
		this.drawCentered=centered;
	}
	public DefaultDirectedAnimation(float x,float y,float size,DirectedAnimationMetrics2D metrics,Direction direction){
		this(x,y,size,metrics,direction,false);
	}
	public DefaultDirectedAnimation(float x,float y,DirectedAnimationMetrics2D metrics,Direction direction){
		this(x,y,(float)(metrics.SIZE)/DrawingManager2D.TILE_SIZE,metrics,direction,false);
	}
	public DefaultDirectedAnimation(float x,float y,DirectedAnimationMetrics2D metrics,Direction direction,boolean centered){
		this(x,y,(float)(metrics.SIZE)/DrawingManager2D.TILE_SIZE,metrics,direction,centered);
	}
	@Override
	public void onDispose(Level l) {
		//MANUALLY OVERRIDE	IF NEEDED	
	}

	@Override
	protected boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl) {
		float x=drawCentered?this.x-this.size/2:this.x;
		float y=drawCentered?this.y-this.size/2:this.y;
		dm.drawAnimation((int)(this.getTotalTimeElapsed()/metrics.ANIMMOD), x, y, this.size, metrics,this.direction, g);
		return this.getTotalTimeElapsed()>this.metrics.ANIMMOD*this.metrics.ANIMNUM;
	}
	@Override
	protected void concreteUpdate(float totalTimeElapsed, Level l) {
		//MANUALLY OVERRIDE	IF NEEDED
	}

}
