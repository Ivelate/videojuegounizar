package tw.gui.animation;

import java.awt.Graphics2D;

import tw.entities.Character.Direction;
import tw.game.Level;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;

public class DeathAnimation extends Animation
{
	private Metrics2D metrics;
	private float x;
	private float y;
	private float scale;
	private Direction direction;
	
	private float lifetime;
	private float animmod;
	private float cont=0;
	
	public DeathAnimation(float x,float y,float scale,Direction direction,float animmod,float lifetime,Metrics2D metrics){
		this.metrics=metrics;
		this.x=x;
		this.y=y;
		this.scale=scale;
		this.direction=direction;
		this.animmod=animmod;
		this.lifetime=lifetime;
	}
	@Override
	public void onDispose(Level l) {
		//MANUALLY OVERRIDE	IF NEEDED	
	}

	@Override
	protected boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawDeathFrameUsingMetrics((int)(this.getTotalTimeElapsed()/animmod), this.x, this.y, this.scale,this.direction, metrics, g);
		
		return this.getTotalTimeElapsed()>lifetime;
	}
	@Override
	protected void concreteUpdate(float totalTimeElapsed, Level l) {
		//MANUALLY OVERRIDE	IF NEEDED
	}


}
