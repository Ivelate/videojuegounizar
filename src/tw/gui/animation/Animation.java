package tw.gui.animation;

import java.awt.Graphics2D;

import tw.game.Level;
import tw.gui.DrawingManager2D;
import tw.utils.Drawable2D;

public abstract class Animation implements Drawable2D
{
	private boolean finished=false;
	private float totalTimeElapsed=0;
	
	public boolean hasFinished(){
		return this.finished;
	}
	public final boolean update(float tEl,Level l){
		this.totalTimeElapsed+=tEl;
		this.concreteUpdate(totalTimeElapsed, l);
		return this.finished;
	}
	public float getTotalTimeElapsed(){
		return this.totalTimeElapsed;
	}
	public abstract void onDispose(Level l);
	protected abstract void concreteUpdate(float totalTimeElapsed,Level l);
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		if(!finished){
			this.finished=drawAnimation(dm,g,tEl);
		}
		
	}
	protected abstract boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl);
}
