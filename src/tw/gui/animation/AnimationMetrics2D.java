package tw.gui.animation;

import tw.gui.DrawingManager2D.MetricDirections;
import tw.gui.DrawingManager2D.ImageID;

public enum AnimationMetrics2D 
{
	BIGEXPLOSION(	55,
					10,
					0,
					0,
					0.05f),
	GRENADE(	10,
				16,
				0,
				55,
				0.05f),
				
	GRENADE_EXPLOSION(	32,
						7,
						0,
						65,
						0.05f),
	SPIDERNEST_SPAWN( 	39,
						12,
						0,
						97,
						0.1f),
	SPIDERNEST_DEATH( 	39,
						4,
						0,
						136,
						0.1f),
	
	REINFORCEMENTS_BRONZE(	23,
							2,
							0,
							175,
							0.03f),
							
	REINFORCEMENTS_SILVER(	23,
							2,
							46,
							175,
							0.03f),
							
	REINFORCEMENTS_GOLD(	23,
							2,
							92,
							175,
							0.03f),
													
	REINFORCEMENTS_SUPER(	23,
							3,
							138,
							175,
							0.03f),
							
	TEXT_VICTORY(			364,
							117,
							1,
							0,
							198,
							8.2f),
			
	TEXT_DEFEAT(			324,
							117,
							1,
							0,
							315,
							8.2f),
	
	WEAPON_BASIC(			13,
							2,
							0,
							432,
							0.03f),
							
	WEAPON_PLASMA(			13,
							2,
							26,
							432,
							0.03f),
	
	WEAPON_FIRE(			13,
							2,
							52,
							432,
							0.03f),
							
	WEAPON_GRENADE(			13,
							1,
							78,
							432,
							0.03f),
							
	PLAYER_ONE(				9,
							2,
							0,
							445,
							0.03f),
							
	PLAYER_TWO(				9,
							2,
							18,
							445,
							0.03f),
							
	PLAYER_THREE(			9,
							2,
							36,
							445,
							0.03f),
							
	PLAYER_FOUR(			9,
							2,
							54,
							445,
							0.03f),
							
	SPIDER_EVOLVE(			52,
							5,
							0,
							454,
							0.35f),
							
	SPIDERPLANT(			26,
							7,
							0,
							506,
							0.2f),
	
	BLOOD(					22,
							6,
							0,
							532,
							0.03f),
						
	BLOODSPIDER(			22,
							6,
							132,
							532,
							0.03f),						
	
	SMOKE(					25,
							7,
							0,
							554,
							0.1f),		
	
	MACHINEGUN_EXP(			8,
							3,
							0,
							579,
							0.05f),	
							
	PLASMA_EXP(				8,
							3,
							24,
							579,
							0.05f),
	//ADD 17 to the Y for the flame bullet	
	GUI_GRENADE(			54,
							12,
							9,
							0,
							604,
							1f),
							
	GUI_PLAYER(				18,
							4,
							0,
							616,
							1f),
	
	GUI_BORDER(				93,
							25,
							1,
							0,
							634,
							1f),
							
	GUI_ALLIESCOUNT(		31,
							16,
							1,
							93,
							634,
							1f);				
	
	
	
	public final int SIZEX;
	public final int SIZEY;
	public final int FRAMESNUM;
	public final int STARTX;
	public final int STARTY;
	public final float ANIMMOD;
	private AnimationMetrics2D(int size,int framesnum,int startx,int starty,float animmod){
		SIZEX=size;
		SIZEY=size;
		ANIMMOD=animmod;
		STARTX=startx;
		STARTY=starty;
		FRAMESNUM=framesnum;
	}
	private AnimationMetrics2D(int sizex,int sizey,int framesnum,int startx,int starty,float animmod){
		SIZEX=sizex;
		SIZEY=sizey;
		ANIMMOD=animmod;
		STARTX=startx;
		STARTY=starty;
		FRAMESNUM=framesnum;
	}
}
