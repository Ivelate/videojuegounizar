package tw.gui.animation;

import tw.entities.EntitySpawner;
import tw.entities.Character.Direction;
import tw.game.Level;
import tw.gui.Metrics2D;

public class SpawningDeathAnimation extends DeathAnimation
{
	private int spawned=0;
	private float spawnmod;
	private float spawnx;
	private float spawny;
	private EntitySpawner.CHARACTER_TYPE charType;
	public SpawningDeathAnimation(float x, float y, float scale,Direction direction, float animmod, float lifetime,float spawnmod,EntitySpawner.CHARACTER_TYPE charType,float spawnx,float spawny, Metrics2D metrics) {
		super(x, y, scale, direction, animmod, lifetime, metrics);
		this.spawnmod=spawnmod;
		this.charType=charType;
		this.spawnx=spawnx;
		this.spawny=spawny;
	}
	@Override
	public void onDispose(Level l) {
		//MANUALLY OVERRIDE	IF NEEDED	
	}
	@Override
	public void concreteUpdate(float totalTimeElapsed,Level l){
		while(spawned*spawnmod<this.getTotalTimeElapsed()){
			l.spawnCharacter(this.charType, this.spawnx,this.spawny);
			spawned++;
		}
	}
}
