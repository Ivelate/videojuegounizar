package tw.gui.animation;

import java.awt.Color;
import java.awt.Graphics2D;

import tw.game.Level;
import tw.gui.DrawingManager2D;

public class BlackCourtainAnimation extends Animation
{
		private boolean isOpening;
		private float closeTime;
		private float width;
		private float height;
		private float closeAmount;
		private float lifetime;
		private Level level;
		
		public BlackCourtainAnimation(float closeTime,float duration,float width,float height,boolean open,Level l){
			this.closeTime=closeTime;
			this.width=width;
			this.height=height;
			this.isOpening=open;
			this.closeAmount=isOpening?1:0;
			this.lifetime=duration;
			this.level=l;
		}
		public BlackCourtainAnimation(float closeTime,float width,float height,boolean open,Level l){
			this(closeTime,closeTime,width,height,open,l);
		}
		@Override
		public void onDispose(Level l) {
			//MANUALLY OVERRIDE	IF NEEDED	
		}

		@Override
		protected boolean drawAnimation(DrawingManager2D dm, Graphics2D g, float tEl) {
			this.lifetime-=tEl;
			
			if(!this.isOpening){
				this.closeAmount+=tEl/closeTime;
				if(this.closeAmount>1) this.closeAmount=1;
			}
			else{
				this.closeAmount-=tEl/closeTime;
				if(this.closeAmount<0) this.closeAmount=0;
			}
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, (int)((width/2)*this.closeAmount), (int)this.height);
			g.fillRect((int)((width/2)*(2-this.closeAmount)), 0,(int)this.width , (int)this.height);
			if(this.lifetime<=0) onDisposeAfterDraw();
			return this.lifetime<=0;
		}
		public void onDisposeAfterDraw()
		{
			if(this.level!=null) this.level.setGameEnded();
		}
		@Override
		protected void concreteUpdate(float totalTimeElapsed, Level l) {
			//MANUALLY OVERRIDE IF NEEDED
		}

}
