package tw.gui;

import java.awt.Color;
import java.awt.Graphics2D;

import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.game.Player;
import tw.gui.animation.AnimationMetrics2D;
import tw.utils.Drawable2D;

public class GameGUI implements Drawable2D
{
	private Player[] players;
	private Level level;
	private int mapWidth;
	private float gameCounter=0;
	public GameGUI(Player[] players,Level l)
	{
		this.players=players;
		this.mapWidth=l.getMap().getWidth();
		this.level=l;
	}

	public void update(float tEl)
	{
		this.gameCounter+=tEl;
	}
	
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) 
	{
		float playerguisize=(float)(AnimationMetrics2D.GUI_PLAYER.SIZEX)/DrawingManager2D.TILE_SIZE;
		float borderguisize=(float)(AnimationMetrics2D.GUI_BORDER.SIZEX)/DrawingManager2D.TILE_SIZE;
		float alliescountguisize=(float)(AnimationMetrics2D.GUI_ALLIESCOUNT.SIZEX)/DrawingManager2D.TILE_SIZE;
		float grenadeguisize=(float)(AnimationMetrics2D.GUI_GRENADE.SIZEX)/DrawingManager2D.TILE_SIZE;
		float grenadeguisizey=(float)(AnimationMetrics2D.GUI_GRENADE.SIZEY)/DrawingManager2D.TILE_SIZE;
		
		dm.drawAnimation(0, this.mapWidth*((float)(1)/25)-(playerguisize*3/5), playerguisize*1/8, borderguisize, AnimationMetrics2D.GUI_BORDER, g);
		dm.drawAnimation(0, this.mapWidth*((float)(1)/25)+(playerguisize/4), playerguisize*4/8, alliescountguisize, AnimationMetrics2D.GUI_ALLIESCOUNT, g);
		dm.drawText(""+this.level.getTeamMembers()[Faction.MARINE.ordinal()], this.level.getTeamMembers()[Faction.MARINE.ordinal()]>9?this.mapWidth/10-playerguisize/3:this.mapWidth/10, playerguisize*23/16, playerguisize*4/3, g);
		
		for(int i=0;i<this.players.length;i++)
		{
			if(this.players[i]==null) continue;
			dm.drawAnimation(0, this.mapWidth*((float)(1)/3)+(borderguisize+playerguisize/2)*i-(playerguisize*3/5), playerguisize*1/8, borderguisize, AnimationMetrics2D.GUI_BORDER, g);
			dm.drawAnimation(i, this.mapWidth*((float)(1)/3)+(borderguisize+playerguisize/2)*i, playerguisize*4/9, playerguisize, AnimationMetrics2D.GUI_PLAYER, g);
			dm.drawAnimation(this.players[i].getAvaillableSecondaryAmmo(), this.mapWidth*((float)(1)/3)+(borderguisize+playerguisize/2)*i + playerguisize*7/6, playerguisize*4/9+ (playerguisize-grenadeguisizey)/4, grenadeguisize, AnimationMetrics2D.GUI_GRENADE, g);
		}
		
		dm.drawText(getAsTimeFormat(this.gameCounter),this.mapWidth*6/7,playerguisize*20/16, playerguisize, new Color(0,0.8f,0),g);
	}
	
	private static String getAsTimeFormat(float time)
	{
		int seconds=(int)time;
		int sec=seconds%60;
		seconds=seconds/60;
		int min=seconds%60;
		int hours=seconds/60;
		return (hours<10?"0"+hours:hours)+":"+(min<10?"0"+min:min)+":"+(sec<10?"0"+sec:sec);
	}
}
