package tw.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import tw.utils.Drawable2D;

/**
 * Game screen
 */
public class Screen extends JFrame
{
	private final int DEFAULT_STATE;
	private boolean fullscreen=false;
	private boolean nativeFullscreenEnabled=false;
	private Dimension windowDimension;
	private Dimension fullScreenDimension;
	
	private GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
	private GraphicsDevice device = env.getScreenDevices()[0];
	private DisplayMode oldMode = device.getDisplayMode();
	private DisplayMode newMode = new DisplayMode(1024,768,oldMode.getBitDepth(),oldMode.getRefreshRate()); //NATIVE FULLSCREEN
	
	private JPanel activeView;

	public Screen(String title,boolean fullscreen,boolean forceNativeFullscreen,int width,int height)
	{
		DEFAULT_STATE=this.getExtendedState();
		Toolkit toolkit=Toolkit.getDefaultToolkit();
		this.fullScreenDimension=toolkit.getScreenSize();
		this.windowDimension=new Dimension(width,height);
		setTitle(title);
		//setResizable(false);
		this.addComponentListener(new ComponentAdapter() {
		    public void componentResized(ComponentEvent e) {
		    	windowDimension.width=superWidth();
		        windowDimension.height=superHeight();
		        if(activeView!=null)setActiveView(activeView);
		    }
		});
		setFullScreen(fullscreen,forceNativeFullscreen);
		this.getContentPane().setBackground(Color.BLACK);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Sets the active JPanel of the screen (Menu, game jpanel, etc)
	 */
	public void setActiveView(JPanel jpanel)
	{
		if(this.activeView!=null) this.getContentPane().remove(this.activeView);
		this.activeView=jpanel;
		this.add(jpanel);
		//TODO BAD CODE BAD BAD BAD BAD
		if(jpanel instanceof GamePanel){
			((GamePanel) jpanel).setAssociatedScreen(this);
		}
		else{
			jpanel.setSize(this.getSize());
			jpanel.requestFocusInWindow();
		}
	}
	
	/**
	 * Goes to fullscreen if <val> is true, goes to windowed mode if val isnt
	 */
	public void setFullScreen(boolean val,boolean forceNativeFullscreen)
	{
		if(val && !fullscreen){
			this.fullscreen=true;
			//By default, tries to enable native full screen (VSYNC, DOUBLE BUFFERING)
			if(!this.nativeFullscreenEnabled&&forceNativeFullscreen){
				this.setUndecorated(true);
				try{
					device.setFullScreenWindow(this);
					device.setDisplayMode(newMode);
					this.nativeFullscreenEnabled=true;
				}
				catch(Exception e){
					System.err.println("Unsupported screen resolution ("+this.newMode.getWidth()+","+this.newMode.getHeight()+"). Switching to MANUAL FULLSCREEN MODE (Graphic quality may be affected)");
					try{device.setFullScreenWindow(null);device.setDisplayMode(oldMode);}catch(Exception ex){}
				}
			}
			//If it fails, activates java-emulated full-screen
			if(!this.nativeFullscreenEnabled){
				this.setSize(this.fullScreenDimension);
				this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
				try{this.setUndecorated(true);}catch(Exception e){}
			}
		}
		else if(!val){
			this.fullscreen=false;
			if(this.nativeFullscreenEnabled){
				this.nativeFullscreenEnabled=false;
				try{device.setDisplayMode(oldMode);}catch(Exception ex){}
			}
			this.setExtendedState(DEFAULT_STATE); 
			this.setSize(this.windowDimension);
			this.setUndecorated(false);
			setBounds((this.fullScreenDimension.width-this.getWidth())/2,(this.fullScreenDimension.height-this.getHeight())/2,this.getWidth(),this.getHeight());
		}
		this.setVisible(true);
	}
	
	public boolean isFullScreen()
	{
		return this.fullscreen;
	}
	public boolean isFullScreenNative()
	{
		return this.nativeFullscreenEnabled;
	}
	
	public int getWidth()
	{
		if(this.fullscreen&&this.nativeFullscreenEnabled) return newMode.getWidth();
		else if(this.fullscreen) return fullScreenDimension.width;
		else return windowDimension.width;
	}
	public int getHeight()
	{
		if(this.fullscreen&&this.nativeFullscreenEnabled) return newMode.getHeight();
		else if(this.fullscreen) return fullScreenDimension.height;
		else return windowDimension.height;
	}
	public final int superWidth()
	{
		return super.getWidth();
	}
	public final int superHeight()
	{
		return super.getHeight();
	}
	
}
