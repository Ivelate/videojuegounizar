package tw.utils3d;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import ivengine.shaders.SimpleShaderProgram;

public class LevelShaderProgram extends SimpleShaderProgram
{

	private int projectionMatrixLoc;
	private int viewMatrixLoc;
	private int modelMatrixLoc;
	
	private int locAttrib;
	private int tileIDAttrib;
	private int tilesUniform;
	
	public LevelShaderProgram()
	{
		this(false);
	}
	public LevelShaderProgram(boolean verbose)
	{
		super("/shaders/levelShader.vshader","/shaders/levelShader.fshader",verbose);
		
		this.locAttrib = glGetAttribLocation(this.getID(), "location");
		this.tileIDAttrib = glGetAttribLocation(this.getID(), "tileID");
		
		this.tilesUniform= glGetUniformLocation(this.getID(),"tiles");
		
		this.projectionMatrixLoc=glGetUniformLocation(this.getID(),"projectionMatrix");
		this.viewMatrixLoc=glGetUniformLocation(this.getID(),"viewMatrix");
		this.modelMatrixLoc=glGetUniformLocation(this.getID(),"modelMatrix");
	}
	public int getProjectionMatrixLoc() {
		return projectionMatrixLoc;
	}
	public int getViewMatrixLoc() {
		return viewMatrixLoc;
	}
	public int getModelMatrixLoc() {
		return modelMatrixLoc;
	}
	public int getTilesTextureLocation()
	{
		return this.tilesUniform;
	}
	@Override
	public void setupAttributes() {
		glVertexAttribPointer(locAttrib,3,GL_FLOAT,false,getSize()*4,0);
		glEnableVertexAttribArray(locAttrib);
		glVertexAttribPointer(tileIDAttrib,1,GL_FLOAT,false,getSize()*4,3*4);
		glEnableVertexAttribArray(tileIDAttrib);
	}
	@Override
	public int getSize()
	{
		return 4;
	}
	@Override
	protected void dispose() {
		// TODO Auto-generated method stub
		
	}
}
