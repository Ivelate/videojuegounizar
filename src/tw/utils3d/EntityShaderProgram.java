package tw.utils3d;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import ivengine.shaders.SimpleShaderProgram;

public class EntityShaderProgram extends SimpleShaderProgram
{

	private int projectionMatrixLoc;
	private int viewMatrixLoc;
	private int modelMatrixLoc;
	
	private int locAttrib;
	
	private int basecolorUniform;
	private int colorDegradesUniform;
	
	public EntityShaderProgram()
	{
		this(false);
	}
	public EntityShaderProgram(boolean verbose)
	{
		super("/shaders/entityShader.vshader","/shaders/entityShader.fshader",verbose);
		
		this.locAttrib = glGetAttribLocation(this.getID(), "location");
		
		this.basecolorUniform= glGetUniformLocation(this.getID(),"baseColor");
		this.colorDegradesUniform= glGetUniformLocation(this.getID(),"colorDegrades");
		
		this.projectionMatrixLoc=glGetUniformLocation(this.getID(),"projectionMatrix");
		this.viewMatrixLoc=glGetUniformLocation(this.getID(),"viewMatrix");
		this.modelMatrixLoc=glGetUniformLocation(this.getID(),"modelMatrix");
	}
	public int getProjectionMatrixLoc() {
		return projectionMatrixLoc;
	}
	public int getViewMatrixLoc() {
		return viewMatrixLoc;
	}
	public int getModelMatrixLoc() {
		return modelMatrixLoc;
	}
	public int getBaseColorLocation()
	{
		return this.basecolorUniform;
	}
	public int getColorDegrades()
	{
		return this.colorDegradesUniform;
	}
	@Override
	public void setupAttributes() {
		glVertexAttribPointer(locAttrib,3,GL_FLOAT,false,getSize()*4,0);
		glEnableVertexAttribArray(locAttrib);
	}
	@Override
	public int getSize()
	{
		return 3;
	}
	@Override
	protected void dispose() {
		// TODO Auto-generated method stub
		
	}
}
