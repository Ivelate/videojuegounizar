package tw.utils3d;

public interface KeyToggleListener {
	public void notifyKeyToggle(int code);
}
