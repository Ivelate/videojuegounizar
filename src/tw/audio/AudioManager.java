package tw.audio;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.FloatControl.Type;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import tw.Main;

/**
 * Manages audio reproductions
 */
public abstract class AudioManager 
{
	//Sounds of the game
	public enum Sound{	SHOOT_MACHINEGUN,
						SOLDIER_DEATH,
						BIGSPIDER_DEATH,
						REINFORCEMENTS_BRONZE,
						REINFORCEMENTS_SILVER,
						REINFORCEMENTS_GOLD,
						REINFORCEMENTS_SUPER,
						EXPLOSION,
						EXPLOSION_BIG,
						GRENADE,
						SPAWN,
						SPIDERNEST_DEATH,
						BIGSPIDER_MUTATE,
						PLANTCRUSH;
						public static final int size = Sound.values().length;
						};
	
	//Music of the game (Will play looped)
	public enum Music{	MUSIC_GAME;
						public static final int size = Music.values().length;
						};

	//Current playing music
	private Music currentlyPlayedMusic=null;
	
	
	/**
	 * SINGLETON PATTERN. All calls should be done with a getInstance() over a static AudioManager. If there is no
	 * manager set, it will initcialize muted
	 */
	public static AudioManager instance=null;
	public static AudioManager getInstance() 
	{
		if(instance==null){
			instance=new MutedAudioManager();
		}
		
		return instance;
	}
	
	/**
	 * Sets audio manager
	 */
	public static void setAudioManager(AudioManager am)
	{
		instance=am;
	}
	
	/**
	 * Plays a sound
	 */
	public void playSound(Sound s)
	{
		if(this.getSoundClipsTable()==null) return;
		
		int sel=0;
		if(this.getSoundClipsTable()[s.ordinal()][1].isRunning()){
			//if((float)(this.getSoundClipsTable()[s.ordinal()][1].getFramePosition())/this.getSoundClipsTable()[s.ordinal()][1].getFrameLength()<0.7f) return;
			this.getSoundClipsTable()[s.ordinal()][1].stop();
			this.getSoundClipsTable()[s.ordinal()][1].flush();
			this.getSoundClipsTable()[s.ordinal()][1].setFramePosition(0);
			sel=0;
		}
		else if(this.getSoundClipsTable()[s.ordinal()][0].isRunning()){
			//if((float)(this.getSoundClipsTable()[s.ordinal()][0].getFramePosition())/this.getSoundClipsTable()[s.ordinal()][0].getFrameLength()<0.7f) return;
			this.getSoundClipsTable()[s.ordinal()][0].stop();
			this.getSoundClipsTable()[s.ordinal()][0].flush();
			this.getSoundClipsTable()[s.ordinal()][0].setFramePosition(0);
			sel=1;
		}
		
		//if(this.getSoundClipsTable()[s.ordinal()][sel].)
		this.getSoundClipsTable()[s.ordinal()][sel].setFramePosition(0);
		//this.getSoundClipsTable()[s.ordinal()][sel].setFramePosition(0);
		this.getSoundClipsTable()[s.ordinal()][sel].start();
		/*for(int i=0;i<this.getSoundClipsTable()[0].length;i++){
			System.out.println(i+" "+this.getSoundClipsTable()[s.ordinal()][i].isRunning());
			if(!this.getSoundClipsTable()[s.ordinal()][i].isRunning()){
				this.getSoundClipsTable()[s.ordinal()][i].setFramePosition(0);
				this.getSoundClipsTable()[s.ordinal()][i].start();
				break;
			}
			else{
				//this.getSoundClipsTable()[s.ordinal()][i].stop();
			}
		}*/
		/*if(this.getSoundClipsTable()[s.ordinal()][0].isRunning()){
			this.getSoundClipsTable()[s.ordinal()][0].stop();
			this.getSoundClipsTable()[s.ordinal()][0].flush();
		}*/
		/*this.getSoundClipsTable()[s.ordinal()][0].stop();
		this.getSoundClipsTable()[s.ordinal()][0].flush();
		this.getSoundClipsTable()[s.ordinal()][0].setFramePosition(0);
		this.getSoundClipsTable()[s.ordinal()][0].start();*/
	}
	
	/**
	 * Plays music
	 */
	public void playMusic(Music m)
	{
		if(this.getMusicClipsTable()==null) return;
		
		if(currentlyPlayedMusic!=null) this.getMusicClipsTable()[currentlyPlayedMusic.ordinal()].stop();
		
		this.currentlyPlayedMusic=m;
		
		if(m!=null){
			this.getMusicClipsTable()[m.ordinal()].setFramePosition(0);
			this.getMusicClipsTable()[m.ordinal()].loop(Clip.LOOP_CONTINUOUSLY);
		}
	}
	
	/**
	 * Stops currently played music
	 */
	public void stopAllMusic()
	{
		if(this.getMusicClipsTable()==null) return;
		
		if(currentlyPlayedMusic!=null) {
			this.getMusicClipsTable()[currentlyPlayedMusic.ordinal()].stop();
			this.currentlyPlayedMusic=null;
		}
	}
	
	/**
	 * Stops all currently playing sounds
	 */
	public void stopAllSounds()
	{
		if(this.getSoundClipsTable()==null) return;
		
		for(Clip[] ct:this.getSoundClipsTable())
		{
			for(Clip c:ct) c.stop();
		}
	}
	
	public void addSoundVolume(float amount)
	{
		if(this.getSoundClipsTable()==null) return;
		
		for(Clip[] ct:this.getSoundClipsTable())
		{
			for(Clip c:ct) 
			{
				FloatControl fc=(FloatControl)c.getControl(Type.MASTER_GAIN);
				fc.setValue(amount);
			}
		}
	}
	
	/**
	 * Returns a sound Clip for a file <f>
	 */
	protected Clip getClipFor(String route) throws LineUnavailableException, UnsupportedAudioFileException, IOException
	{
		Clip clip = AudioSystem.getClip();
		AudioInputStream ais = AudioSystem. getAudioInputStream(Main.class.getResource(route));
		clip.open(ais);
		
		return clip;
	}
	
	protected abstract Clip[][] getSoundClipsTable();
	protected abstract Clip[] getMusicClipsTable();
	
}
