package tw.audio;

import javax.sound.sampled.Clip;

public class MutedAudioManager extends AudioManager
{

	@Override
	protected Clip[][] getSoundClipsTable() {
		return null;
	}

	@Override
	protected Clip[] getMusicClipsTable() {
		return null;
	}
	
}
