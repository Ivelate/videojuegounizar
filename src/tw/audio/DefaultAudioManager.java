package tw.audio;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl.Type;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import tw.Main;

public class DefaultAudioManager extends AudioManager
{
	private Clip[][] soundClipsTable=new Clip[AudioManager.Sound.size][2];
	private Clip[] musicClipsTable=new Clip[AudioManager.Music.size];
	
	private float defaultSoundVolumeOffset=-10;
	
	public DefaultAudioManager() throws LineUnavailableException, UnsupportedAudioFileException, IOException
	{
		this.musicClipsTable[Music.MUSIC_GAME.ordinal()]=getClipFor("/sounds/gamemusic.wav");
		
		addBiClipFor(Sound.SHOOT_MACHINEGUN,"/sounds/soldier_shot.wav",-12);
		
		addBiClipFor(Sound.SOLDIER_DEATH,"/sounds/soldier_death.wav");
		
		addBiClipFor(Sound.BIGSPIDER_DEATH,"/sounds/bigspider_death.wav");
	
		addBiClipFor(Sound.REINFORCEMENTS_BRONZE,"/sounds/reinforcements_bronze.wav");

		addBiClipFor(Sound.REINFORCEMENTS_SILVER,"/sounds/reinforcements_silver.wav");

		addBiClipFor(Sound.REINFORCEMENTS_GOLD,"/sounds/reinforcements_gold.wav");

		addBiClipFor(Sound.REINFORCEMENTS_SUPER,"/sounds/reinforcements_super.wav");
		
		addBiClipFor(Sound.GRENADE,"/sounds/grenade.wav");
		
		addBiClipFor(Sound.EXPLOSION,"/sounds/explosion.wav",-15);
		
		addBiClipFor(Sound.EXPLOSION_BIG,"/sounds/bigexplosion.wav");
		
		addBiClipFor(Sound.SPAWN,"/sounds/spawn.wav");
		
		addBiClipFor(Sound.SPIDERNEST_DEATH,"/sounds/spidernest_death.wav",-15);
		
		addBiClipFor(Sound.BIGSPIDER_MUTATE,"/sounds/bigspider_mutate.wav");
		
		addBiClipFor(Sound.PLANTCRUSH,"/sounds/plantcrush.wav",-11);
	}
	
	private void addBiClipFor(Sound s,String route) throws LineUnavailableException, UnsupportedAudioFileException, IOException
	{
			this.addBiClipFor(s, route,this.defaultSoundVolumeOffset);
	}
	
	private void addBiClipFor(Sound s,String route,float volMod) throws LineUnavailableException, UnsupportedAudioFileException, IOException{
		for(int i=0;i<2;i++) {
			this.soundClipsTable[s.ordinal()][i]=getClipFor(route);
			FloatControl fc=(FloatControl)this.soundClipsTable[s.ordinal()][i].getControl(Type.MASTER_GAIN);
			fc.setValue(volMod);
		}

	}
	
	@Override
	protected Clip[][] getSoundClipsTable() {
		return this.soundClipsTable;
	}

	@Override
	protected Clip[] getMusicClipsTable() {
		return this.musicClipsTable;
	}

}
