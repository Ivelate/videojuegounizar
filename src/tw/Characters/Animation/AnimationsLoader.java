package tw.Characters.Animation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class AnimationsLoader {
	BufferedImage img;
	ArrayList<BufferedImage[]> animaciones;
	int rows;
	int[] columns;
	int Title_size;
	
	public AnimationsLoader(int rows,int[] columns_size,int Title_size,String path) {
		// TODO Auto-generated constructor stub
		this.rows=rows;
		this.columns=columns_size;
		this.Title_size=Title_size;
		this.animaciones= new ArrayList<BufferedImage[]>();
		LoadSpriteIntoMatrix(path);
	}
	
	private void LoadSpriteIntoMatrix(String path){
		try {
			BufferedImage img= ImageIO.read(new File(path));
			//cargamos en la matriz todos los titles
			for(int i=0;i<rows;i++){
				BufferedImage[] animacion=new BufferedImage[columns[i]];
				for(int j=0;j<columns[i];j++){
					animacion[j]=img.getSubimage(j*61, i*121, 61, 121);
				}
				animaciones.add(animacion);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  BufferedImage[] GetAnimation(int n){
		return animaciones.get(n);
	}
	
	public BufferedImage getFrame(int i,int j){
		return animaciones.get(i)[j];
	}

}
