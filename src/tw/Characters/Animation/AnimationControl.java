package tw.Characters.Animation;

import java.awt.image.BufferedImage;
import javax.swing.JComponent;

public class AnimationControl extends JComponent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 144703821890514897L;
	private int duration;
	private int current_duration;
	private boolean stop;
	private int n_frame;
	public AnimationControl(int duration){
		this.duration=duration;
		this.current_duration=0;
		
		this.stop=true;
		this.n_frame=0;
	}
	
	
	public void start(){
		if (!stop) return;
		stop=false;
	}	
	public void stop(){
		stop=true;
	}	
	public void initialState(){
		n_frame=0;
	}
	public int GetFrame(){
		return n_frame;
	}
	
	public void animate(BufferedImage[] frames){
		if(!stop){
			current_duration++;
			
			if(current_duration>duration){
				current_duration=0;
				n_frame++;
			}
			if(n_frame>frames.length-1)	n_frame=0;
			else if(n_frame<0) n_frame=frames.length-1;
			
		}
		
	}	
	
}
