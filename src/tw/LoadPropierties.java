package tw;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import tw.gui3d.Gui3DModule;

public class LoadPropierties {
	
	
	
	
	
	
	
	public void saveParamChanges(boolean Full_Screen,boolean forceNativeFullscreen,boolean VSYNC,boolean Visor) {
	    try {
	        Properties props = new Properties();
	        props.setProperty("Full_Screen", String.valueOf(Full_Screen));
	        props.setProperty("forceNativeFullscreen", String.valueOf(forceNativeFullscreen));
	        props.setProperty("VSYNC", String.valueOf(VSYNC));
	        props.setProperty("Visor", String.valueOf(Visor));
	        File f = new File("config.propierties");
	        OutputStream out = new FileOutputStream( f );
	        props.store(out,null);
	    }
	    catch (Exception e ) {
	        e.printStackTrace();
	    }
	}
	
	
	public void loadParams() {
		try {
			FileInputStream fileInput = new FileInputStream("config.propierties");
			Properties properties = new Properties();
			try {
				properties.load(fileInput);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 Main.forceNativeFullscreen=Boolean.valueOf(properties.getProperty("forceNativeFullscreen"));
			 Main.Full_Screen=Boolean.valueOf(properties.getProperty("Full_Screen"));
			 Gui3DModule.VSYNC=Boolean.valueOf(properties.getProperty("VSYNC"));
			 Main.Visor=Boolean.valueOf(properties.getProperty("Visor"));
			 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
