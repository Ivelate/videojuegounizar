package tw;


import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.io.File;
import java.io.FileOutputStream;
import java.awt.BorderLayout;
import java.io.IOException;
import java.io.PrintStream;

import tw.audio.AudioManager;
import tw.audio.DefaultAudioManager;
import tw.game.GameManager;
import tw.gui.DrawingManager2D;
import tw.gui.Screen;
import tw.gui3d.Gui3DModule;
import tw.menu.CrearFondo;
import tw.menu.GestorMenu;
import tw.menu.GestorMenu.Pantallas;
import tw.utils.BinaryPrintStream;

/**
 * Main class with main method
 */
public class Main {
	public static int screen_Width = 1020;
	public static int screen_Height = 750;
	public static boolean Full_Screen;
	public static boolean forceNativeFullscreen;
	public static boolean Visor;
	public static Screen sc;
	public static GestorMenu Menu = new GestorMenu();
	public static File errorDumpFile = null;
	public static boolean initiateGame=false;
	public static int Jugadores=1;
	public static LoadPropierties properties=new LoadPropierties();
	
	
	
	public static void main(String[] args) throws IOException {
		properties.loadParams();
		sc=new Screen("triumphy wars", Full_Screen, forceNativeFullscreen,
				screen_Width, screen_Height);
		sc.setActiveView(Menu.selecionarPantalla(Pantallas.MENUINICIO));
		//inciarPartida();
		synchronized(Menu){
			try {
				Menu.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		sc.removeAll();
		sc.dispose();
		inciarPartida();
	}
	
	

	public static void inciarPartida() {
		do {
			errorDumpFile = new File("triumphwarErrorDump_" + Math.random()
					+ ".dump");
		} while (errorDumpFile.exists());
		try {
			System.out.println("Inicio partida");
			sc=new Screen("triumphy wars", Full_Screen, forceNativeFullscreen,
					screen_Width, screen_Height);
			AudioManager.setAudioManager(new DefaultAudioManager());
			DrawingManager2D dm = new DrawingManager2D();
			new GameManager(null, sc, dm, Visor);
		} catch (Exception e) {
			System.err.println("A fatal error has ocurred. Game will exit now");
			System.err.println("A error dump has been created in "
					+ errorDumpFile.getAbsolutePath());
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}
	
	
}
