package tw.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class BinaryPrintStream extends PrintStream
{
	private PrintStream auxPrintStream=null;
	private File outputFile;
	public BinaryPrintStream(PrintStream p1,File f) throws FileNotFoundException {
		super(p1);
		this.outputFile=f;
	}
	@Override
	public void println(String s)
	{
		super.println(s);
		if(this.auxPrintStream==null){
			try {
				this.auxPrintStream= new PrintStream(new FileOutputStream(this.outputFile));
			} catch (FileNotFoundException e) {}
		}
		this.auxPrintStream.println(s);
	}
	@Override
	public void println()
	{
		super.println();
		if(this.auxPrintStream==null){
			try {
				this.auxPrintStream= new PrintStream(new FileOutputStream(this.outputFile));
			} catch (FileNotFoundException e) {}
		}
		this.auxPrintStream.println();
	}
	@Override
	public void println(java.lang.Object o)
	{
		super.println(o);
		if(this.auxPrintStream==null){
			try {
				this.auxPrintStream= new PrintStream(new FileOutputStream(this.outputFile));
			} catch (FileNotFoundException e) {}
		}
		this.auxPrintStream.println(o);
	}
	
}
