package tw.utils;

import tw.entities.Bullet;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public interface Shootable 
{
	public boolean receiveShoot(Bullet b,Level l);
	public Faction getFaction();
	public boolean occludesSameFactionBullets();
	public boolean canBeShooted();
}
