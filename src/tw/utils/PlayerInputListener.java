package tw.utils;

public interface PlayerInputListener 
{
	public enum KEY_TYPE{SHOOT,ORDER};
	
	public void onPlayerInputPressed(KEY_TYPE kt);
}
