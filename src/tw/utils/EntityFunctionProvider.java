package tw.utils;

import tw.entities.Bullet;
import tw.entities.Entity;

public interface EntityFunctionProvider 
{
	public void applyFunctionToEntity(Entity e);
	public void applyFunctionToBullet(Bullet b);
}
