package tw.utils;

import tw.game.Map;

/**
 * Loads and instantiates maps
 */
public class MapManager 
{
	public enum MapCode{TEST1,TEST2,TEST_BIG,TEST_SMALL,REAL1,Level1,Level2,Level3,Level4,Level5}
	
	public static Map getMap(MapCode code)
	{
		Map m=null;
		switch(code)
		{	
		case Level1:
			try {
				m=new Map("assets/Maps/Level1.tmx");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;
		
		case Level2:
			try {
				m=new Map("assets/maps/Level2.tmx");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;	
		
		case Level3:
			try {
				m=new Map("assets/maps/Level3.tmx");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;		
		
		case Level4:
			try {
				m=new Map("assets/maps/Level4.tmx");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;		
		
		
		case Level5:
			try {
				m=new Map("assets/maps/Level5.tmx");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;		
		
		
		case TEST1:
			byte[][] testmapTiles = new byte[43][26];
			//Random map
			for(int w=0;w<testmapTiles.length;w++) for(int h=0;h<testmapTiles[0].length;h++)
			{
				if(Math.random()<0.2) {
					int ch=(int)(Math.random()*4);
					if(ch>0)ch=ch+2;
					testmapTiles[w][h]=(byte)(ch); 
				}
				else testmapTiles[w][h]=2; //Mapa aleatorio usando 18 tiles (Muchos en blanco)
			}
			m=new Map(testmapTiles);
			break;
		case REAL1:
			testmapTiles = new byte[34][25];
			//Random map
			for(int w=0;w<testmapTiles.length;w++) for(int h=0;h<testmapTiles[0].length;h++)
			{
				if(Math.random()<0.2) {
					int ch=(int)(Math.random()*4);
					ch=ch==0?101:ch==1?113:ch==2?129:150;
					if(ch==129&&h>0) testmapTiles[w][h-1]=65;
					testmapTiles[w][h]=(byte)(ch); 
				}
				else testmapTiles[w][h]=4; //Mapa aleatorio usando 18 tiles (Muchos en blanco)
			}
			m=new Map(testmapTiles);
			break;
		case TEST2:
			byte[][] testmap2Tiles = new byte[43][26];
			//Random map
			for(int w=0;w<testmap2Tiles.length;w++) for(int h=0;h<testmap2Tiles[0].length;h++)
			{
				testmap2Tiles[w][h]=2; //Mapa aleatorio usando 18 tiles (Muchos en blanco)
			}
			testmap2Tiles[3][7]=3;
			testmap2Tiles[3][6]=5;
			
			testmap2Tiles[10][7]=6;
			testmap2Tiles[10][6]=4;
			
			testmap2Tiles[6][3]=5;
			testmap2Tiles[7][3]=4;
			
			testmap2Tiles[6][10]=3;
			testmap2Tiles[7][10]=6;
			m=new Map(testmap2Tiles);
			break;
		case TEST_BIG:
			testmapTiles = new byte[80][45];
			//Random map
			for(int w=0;w<testmapTiles.length;w++) for(int h=0;h<testmapTiles[0].length;h++)
			{
				if(Math.random()<0.2) {
					int ch=(int)(Math.random()*4);
					if(ch>0)ch=ch+2;
					testmapTiles[w][h]=(byte)(ch); 
				}
				else testmapTiles[w][h]=2; //Mapa aleatorio usando 18 tiles (Muchos en blanco)
			}
			m=new Map(testmapTiles);
			break;
		case TEST_SMALL:
			testmapTiles = new byte[21][13];
			//Random map
			for(int w=0;w<testmapTiles.length;w++) for(int h=0;h<testmapTiles[0].length;h++)
			{
				if(Math.random()<0.2) {
					int ch=(int)(Math.random()*4);
					if(ch>0)ch=ch+2;
					testmapTiles[w][h]=(byte)(ch); 
				}
				else testmapTiles[w][h]=2; //Mapa aleatorio usando 18 tiles (Muchos en blanco)
			}
			m=new Map(testmapTiles);
			break;
		}
		return m;
	}
}
