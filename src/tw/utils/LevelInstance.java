package tw.utils;

public interface LevelInstance 
{
	public void update(float tEl);
	public boolean gameEnded();
	public void setGameEnded();
}
