package tw.utils;

public interface SizedDrawable2D extends Drawable2D{
	public int getWidth();
	public int getHeight();
}
