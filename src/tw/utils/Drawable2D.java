package tw.utils;

import java.awt.Graphics2D;

import tw.gui.DrawingManager2D;

public interface Drawable2D {
	public void draw2D(DrawingManager2D dm,Graphics2D g,float tEl);
}
