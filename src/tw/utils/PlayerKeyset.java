package tw.utils;

public class PlayerKeyset 
{
	public static final int UP_CODE_INDEX=0;
	public static final int DOWN_CODE_INDEX=1;
	public static final int LEFT_CODE_INDEX=2;
	public static final int RIGHT_CODE_INDEX=3;
	public static final int SHOOT_CODE_INDEX=4;
	public static final int ORDER_CODE_INDEX=5;
	
	private int[] keysets=new int[6];
	public PlayerKeyset(int up_code,int down_code,int left_code,int right_code,int shoot_code,int order_code)
	{
		this.keysets[UP_CODE_INDEX]=up_code;
		this.keysets[DOWN_CODE_INDEX]=down_code;
		this.keysets[LEFT_CODE_INDEX]=left_code;
		this.keysets[RIGHT_CODE_INDEX]=right_code;
		this.keysets[SHOOT_CODE_INDEX]=shoot_code;
		this.keysets[ORDER_CODE_INDEX]=order_code;
	}
	
	public int getKeysetForCode(int code)
	{
		return this.keysets[code];
	}
}
