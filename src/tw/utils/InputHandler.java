package tw.utils;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;

public class InputHandler implements KeyListener
{
	private Hashtable<Integer,Boolean> pressedButtons=new Hashtable<Integer,Boolean>();
	
	private Hashtable<Integer,PlayerInputListenerInstance> listeners=new Hashtable<Integer,PlayerInputListenerInstance>();
	
	private PlayerKeyset[] playerKeysets;
	
	public InputHandler(int maxPlayers)
	{
		this.playerKeysets=new PlayerKeyset[maxPlayers];
		//DEFAULT KEYSETS
		if(maxPlayers>=1){
			this.playerKeysets[0]=new PlayerKeyset(KeyEvent.VK_W,KeyEvent.VK_S,KeyEvent.VK_A,KeyEvent.VK_D,KeyEvent.VK_V,KeyEvent.VK_B);
		}
		if(maxPlayers>=2){
			this.playerKeysets[1]=new PlayerKeyset(KeyEvent.VK_UP,KeyEvent.VK_DOWN,KeyEvent.VK_LEFT,KeyEvent.VK_RIGHT,111,106);
		}
		if(maxPlayers>=3){
			this.playerKeysets[2]=new PlayerKeyset(KeyEvent.VK_I,KeyEvent.VK_K,KeyEvent.VK_J,KeyEvent.VK_L,KeyEvent.VK_COMMA,KeyEvent.VK_PERIOD);
		}
		//I really hope you arent trying to do a 4-man play in the same keyboard...
		if(maxPlayers>=4){
			this.playerKeysets[3]=new PlayerKeyset(KeyEvent.VK_UP,KeyEvent.VK_DOWN,KeyEvent.VK_LEFT,KeyEvent.VK_RIGHT,111,106);
		}
		
		updateButtonsHandler();
	}
	public void registerKeysForPlayer(int playernum,PlayerKeyset playerKeyset)
	{
		if(playernum>=this.playerKeysets.length)
		{
			System.err.println("Warning: Keyset table limit exceeded. Auto extending...");
			PlayerKeyset[] newTable=new PlayerKeyset[playernum];
			for(int i=0;i<this.playerKeysets.length;i++) 
			{
				newTable[i]=this.playerKeysets[i];
				this.playerKeysets[i]=null;
			}
			this.playerKeysets=newTable;
		}
		
		this.playerKeysets[playernum]=playerKeyset;
		
		updateButtonsHandler();
	}
	private void updateButtonsHandler()
	{
		this.pressedButtons=new Hashtable<Integer,Boolean>();
		for(int i=0;i<this.playerKeysets.length;i++)
		{
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.UP_CODE_INDEX),false);
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.DOWN_CODE_INDEX),false);
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.LEFT_CODE_INDEX),false);
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.RIGHT_CODE_INDEX),false);
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.SHOOT_CODE_INDEX),false);
			this.pressedButtons.put(this.playerKeysets[i].getKeysetForCode(PlayerKeyset.ORDER_CODE_INDEX),false);
		}
	}
	
	public void registerPlayerToInput(PlayerInputListener pi,int playerID)
	{
		this.listeners.put(this.playerKeysets[playerID].getKeysetForCode(PlayerKeyset.SHOOT_CODE_INDEX),new PlayerInputListenerInstance(PlayerInputListener.KEY_TYPE.SHOOT,pi));
		this.listeners.put(this.playerKeysets[playerID].getKeysetForCode(PlayerKeyset.ORDER_CODE_INDEX),new PlayerInputListenerInstance(PlayerInputListener.KEY_TYPE.ORDER,pi));
	}
	public void registerPlayerListenerToKey(PlayerInputListener pi,int keyevent)
	{
		this.listeners.put(keyevent,new PlayerInputListenerInstance(PlayerInputListener.KEY_TYPE.SHOOT,pi));
		this.pressedButtons.put(keyevent,false);
	}
	
	public boolean getUpFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.UP_CODE_INDEX,playerCode);
	}
	public boolean getDownFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.DOWN_CODE_INDEX,playerCode);
	}
	public boolean getLeftFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.LEFT_CODE_INDEX,playerCode);
	}
	public boolean getRightFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.RIGHT_CODE_INDEX,playerCode);
	}
	public boolean getShootFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.SHOOT_CODE_INDEX,playerCode);
	}
	public boolean getOrderFor(int playerCode)
	{
		return getKeyStateByCodeFor(PlayerKeyset.ORDER_CODE_INDEX,playerCode);
	}
	public boolean getKeyStateByCodeFor(int keycode,int playercode)
	{
		return this.pressedButtons.get(this.playerKeysets[playercode].getKeysetForCode(keycode));
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) 
	{
		Boolean value=this.pressedButtons.get(arg0.getKeyCode());
		if(value!=null && value!=true)
		{
			this.pressedButtons.put(arg0.getKeyCode(), true);
			
			PlayerInputListenerInstance pili=this.listeners.get(arg0.getKeyCode());
			if(pili!=null){
				pili.playerInputListener.onPlayerInputPressed(pili.keyType);
			}
		}	
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		Boolean value=this.pressedButtons.get(arg0.getKeyCode());
		if(value!=null && value!=false)
		{
			this.pressedButtons.put(arg0.getKeyCode(), false);
		}	
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	class PlayerInputListenerInstance
	{
		public final PlayerInputListener playerInputListener;
		public final PlayerInputListener.KEY_TYPE keyType;
		
		public PlayerInputListenerInstance(PlayerInputListener.KEY_TYPE kt,PlayerInputListener pil){
			this.playerInputListener=pil;
			this.keyType=kt;
		}
	}
}