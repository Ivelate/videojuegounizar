package tw.utils;


import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

import tw.entities.TestCharacter;

public class LineVision extends Path2D.Double{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double x,y,rotation;
	
	public LineVision(TestCharacter entity){
		this.x=entity.getX();
		this.y=entity.getY();
		this.rotation=0;
		moveTo(entity.getX(), entity.getY());
        lineTo(entity.getX()+200, entity.getY()-50);
        lineTo(entity.getX()+200, entity.getY()+50);
        closePath();
	}
	
	public LineVision(double x,double y){
		this.x=x;
		this.y=y;
		this.rotation=0;
		moveTo(x, y);
        lineTo(x+200, y-50);
        lineTo(x+200, y+50);
        closePath();
	}
	
	/*
	 * Rota siempre desde el eje 0
	 */
	public void Rotate(double degress){
		
		    AffineTransform rotation = new AffineTransform();
		    double angleInRadians = ((degress-this.rotation) * Math.PI / 180);
		    System.out.println(this.x+"		"+this.y);
		    rotation.rotate(-angleInRadians, this.x, this.y);
		    this.transform(rotation);
		    this.rotation=degress;
	}
	
	public void MoveTo(double x,double y){
		//this.x=this.x-x;
		//this.y=this.y-y;
		AffineTransform transform= AffineTransform.getTranslateInstance(this.x-x, this.y-y);
		this.x=this.x-x;
		this.y=this.y-y;
//		transform.translate(this.x=,this.y=);
//		this.x=transform.getTranslateX();
//		this.y=transform.getTranslateY();
		this.transform(transform);
		
	}
	public boolean ColisionDeteted(){
		return false;
	}
	
	public void DrawVision(Graphics2D g2){
		 g2.draw(this);
	}


}
