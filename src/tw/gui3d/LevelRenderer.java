package tw.gui3d;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import ivengine.view.Camera;
import ivengine.view.MatrixHelper;

import java.nio.FloatBuffer;

import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.tiles.Tile.TileState;
import tw.tiles.TileLibrary;
import tw.utils3d.LevelShaderProgram;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class LevelRenderer 
{
	private LevelShaderProgram LSP;
	private int vbo=-1;
	
	private int pointsNum=0;
	private Matrix4f levelModelMatrix;
	
	public LevelRenderer(byte[][] maptiles,LevelShaderProgram LSP)
	{
		this.levelModelMatrix=new Matrix4f();
		this.LSP=LSP;
		
		this.vbo=glGenBuffers();
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
	
		LSP.enable();
		LSP.setupAttributes();
		//CREATE FLOAT BUFFER
		FloatBuffer fb=BufferUtils.createFloatBuffer(maptiles.length*maptiles[0].length*4*5*2*8);
		//POPULATE VBO
		for(int w=maptiles.length-1;w>=0;w--)
		{
			for(int h=0;h<maptiles[0].length;h++)
			{
				float[] cube=new float[this.LSP.getSize()*6];
				int c=0;
				if(w==0){
					insertXRightFace(cube,w,h,maptiles[w][h],0,0,1,2);
					fb.put(cube);
					c+=LSP.getSize()*6;
				}
				else if(w==maptiles.length-1){
					insertXLeftFace(cube,w,h,maptiles[w][h],1,0,1,2);
					fb.put(cube);
					c+=LSP.getSize()*6;
				}
				if(h==0){
					insertZFarFace(cube,w,h,maptiles[w][h],0,0,1,2);
					fb.put(cube);
					c+=LSP.getSize()*6;
				}
				else if(h==maptiles[0].length-1){
					insertZNearFace(cube,w,h,maptiles[w][h],1,0,1,2);
					fb.put(cube);
					c+=LSP.getSize()*6;
				}				
				TileState ts=TileLibrary.get(maptiles[w][h]).getLimitsFor(Role.GROUND, Faction.MARINE);
				switch(ts)
				{
				case EMPTY:
					insertCeiling(cube,w,h,maptiles[w][h],0,0,0,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case FULL:
					insertCeiling(cube,w,h,maptiles[w][h],1,0,0,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZFarFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZNearFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case L_HALF:
					insertCeiling(cube,w,h,maptiles[w][h],1,0,0,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertCeiling(cube,w,h,maptiles[w][h],0,0.5f,0,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],0.5f,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZFarFace(cube,w,h,maptiles[w][h],1,0,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZNearFace(cube,w,h,maptiles[w][h],0,0,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case R_HALF:
					insertCeiling(cube,w,h,maptiles[w][h],1,0.5f,0,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertCeiling(cube,w,h,maptiles[w][h],0,0,0,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0.5f,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZFarFace(cube,w,h,maptiles[w][h],1,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZNearFace(cube,w,h,maptiles[w][h],0,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case D_HALF:
					insertCeiling(cube,w,h,maptiles[w][h],1,0,0.5f,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertCeiling(cube,w,h,maptiles[w][h],0,0,0,1,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZNearFace(cube,w,h,maptiles[w][h],0.5f,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZFarFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0.5f,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case U_HALF:
					insertCeiling(cube,w,h,maptiles[w][h],0,0,0.5f,1,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertCeiling(cube,w,h,maptiles[w][h],1,0,0,1,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZNearFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertZFarFace(cube,w,h,maptiles[w][h],0.5f,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0,0.5f);
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case LU_CORNER:
					//Ceiling
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Ground
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Walls
					insertZNearFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Face
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*0)]=w+1; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case LD_CORNER:
					//Ceiling
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Ground
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Walls
					insertZFarFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXLeftFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Face
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*0)]=w+1; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case RU_CORNER:
					//Ceiling
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Ground
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Walls
					insertZNearFace(cube,w,h,maptiles[w][h],0,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Face
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*0)]=w+1; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				case RD_CORNER:
					//Ceiling
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=0; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Ground
					cube[0+(LSP.getSize()*0)]=w; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h+1; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w+1; cube[1+(LSP.getSize()*1)]=1; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Walls
					insertZFarFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					insertXRightFace(cube,w,h,maptiles[w][h],1,0,1);
					fb.put(cube);
					c+=LSP.getSize()*6;
					
					//Face
					cube[0+(LSP.getSize()*0)]=w+1; cube[1+(LSP.getSize()*0)]=1; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*0)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*2)]=w; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h+1; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					cube[0+(LSP.getSize()*2)]=w+1; cube[1+(LSP.getSize()*2)]=1; cube[2+(LSP.getSize()*2)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					cube[0+(LSP.getSize()*1)]=w; cube[1+(LSP.getSize()*1)]=0; cube[2+(LSP.getSize()*1)]=h+1; 
					cube[3+(LSP.getSize()*1)]=maptiles[w][h];
					cube[0+(LSP.getSize()*0)]=w+1; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h; 
					cube[3+(LSP.getSize()*2)]=maptiles[w][h];
					fb.put(cube);
					c+=LSP.getSize()*6;
					break;
				}
				
				this.pointsNum+=c;
			}
		}
		
		fb.flip();
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
		LSP.setupAttributes();
		glBufferData(GL15.GL_ARRAY_BUFFER,this.pointsNum*4,GL15.GL_STATIC_DRAW);
		this.pointsNum=this.pointsNum/this.LSP.getSize();
		glBufferSubData(GL15.GL_ARRAY_BUFFER,0,fb);
		glBindBuffer(GL15.GL_ARRAY_BUFFER,0);
	}
	public void insertXLeftFace(float[] cube,int w,int h,byte tile,float xadd,float zinit,float zend)
	{
		this.insertXLeftFace(cube, w, h, tile, xadd, zinit, zend,1);
	}
	public void insertXLeftFace(float[] cube,int w,int h,byte tile,float xadd,float zinit,float zend,float ylim)
	{
		cube[0+(LSP.getSize()*0)]=w+xadd; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+zinit; 
		cube[3+(LSP.getSize()*0)]=tile;
		cube[0+(LSP.getSize()*1)]=w+xadd; cube[1+(LSP.getSize()*1)]=ylim; cube[2+(LSP.getSize()*1)]=h+zend; 
		cube[3+(LSP.getSize()*1)]=tile;
		cube[0+(LSP.getSize()*2)]=w+xadd; cube[1+(LSP.getSize()*2)]=ylim; cube[2+(LSP.getSize()*2)]=h+zinit; 
		cube[3+(LSP.getSize()*2)]=tile;
		cube[0+(LSP.getSize()*3)]=w+xadd; cube[1+(LSP.getSize()*3)]=ylim; cube[2+(LSP.getSize()*3)]=h+zend; 
		cube[3+(LSP.getSize()*3)]=tile;
		cube[0+(LSP.getSize()*4)]=w+xadd; cube[1+(LSP.getSize()*4)]=0; cube[2+(LSP.getSize()*4)]=h+zinit; 
		cube[3+(LSP.getSize()*4)]=tile;
		cube[0+(LSP.getSize()*5)]=w+xadd; cube[1+(LSP.getSize()*5)]=0; cube[2+(LSP.getSize()*5)]=h+zend; 
		cube[3+(LSP.getSize()*5)]=tile;
	}
	public void insertXRightFace(float[] cube,int w,int h,byte tile,float xadd,float zinit,float zend)
	{
		this.insertXRightFace(cube, w, h, tile, xadd, zinit, zend,1);
	}
	public void insertXRightFace(float[] cube,int w,int h,byte tile,float xadd,float zinit,float zend,float ylim)
	{
		cube[0+(LSP.getSize()*0)]=w+xadd; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+zinit; 
		cube[3+(LSP.getSize()*0)]=tile;
		cube[0+(LSP.getSize()*1)]=w+xadd; cube[1+(LSP.getSize()*1)]=ylim; cube[2+(LSP.getSize()*1)]=h+zinit; 
		cube[3+(LSP.getSize()*1)]=tile;
		cube[0+(LSP.getSize()*2)]=w+xadd; cube[1+(LSP.getSize()*2)]=ylim; cube[2+(LSP.getSize()*2)]=h+zend; 
		cube[3+(LSP.getSize()*2)]=tile;
		cube[0+(LSP.getSize()*3)]=w+xadd; cube[1+(LSP.getSize()*3)]=ylim; cube[2+(LSP.getSize()*3)]=h+zend; 
		cube[3+(LSP.getSize()*3)]=tile;
		cube[0+(LSP.getSize()*4)]=w+xadd; cube[1+(LSP.getSize()*4)]=0; cube[2+(LSP.getSize()*4)]=h+zend; 
		cube[3+(LSP.getSize()*4)]=tile;
		cube[0+(LSP.getSize()*5)]=w+xadd; cube[1+(LSP.getSize()*5)]=0; cube[2+(LSP.getSize()*5)]=h+zinit; 
		cube[3+(LSP.getSize()*5)]=tile;
	}
	public void insertZNearFace(float[] cube,int w,int h,byte tile,float yadd,float xinit,float xend)
	{
		this.insertZNearFace(cube, w, h, tile, yadd, xinit, xend,1);
	}
	public void insertZNearFace(float[] cube,int w,int h,byte tile,float yadd,float xinit,float xend,float ylim)
	{
		cube[0+(LSP.getSize()*0)]=w+xinit; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+yadd; 
		cube[3+(LSP.getSize()*0)]=tile;
		cube[0+(LSP.getSize()*1)]=w+xinit; cube[1+(LSP.getSize()*1)]=ylim; cube[2+(LSP.getSize()*1)]=h+yadd; 
		cube[3+(LSP.getSize()*1)]=tile;
		cube[0+(LSP.getSize()*2)]=w+xend; cube[1+(LSP.getSize()*2)]=ylim; cube[2+(LSP.getSize()*2)]=h+yadd; 
		cube[3+(LSP.getSize()*2)]=tile;
		cube[0+(LSP.getSize()*3)]=w+xend; cube[1+(LSP.getSize()*3)]=ylim; cube[2+(LSP.getSize()*3)]=h+yadd; 
		cube[3+(LSP.getSize()*3)]=tile;
		cube[0+(LSP.getSize()*4)]=w+xend; cube[1+(LSP.getSize()*4)]=0; cube[2+(LSP.getSize()*4)]=h+yadd; 
		cube[3+(LSP.getSize()*4)]=tile;
		cube[0+(LSP.getSize()*5)]=w+xinit; cube[1+(LSP.getSize()*5)]=0; cube[2+(LSP.getSize()*5)]=h+yadd; 
		cube[3+(LSP.getSize()*5)]=tile;
	}
	public void insertZFarFace(float[] cube,int w,int h,byte tile,float zadd,float xinit,float xend)
	{
		this.insertZFarFace(cube, w, h, tile, zadd, xinit, xend,1);
	}
	public void insertZFarFace(float[] cube,int w,int h,byte tile,float zadd,float xinit,float xend,float ylim)
	{
		cube[0+(LSP.getSize()*0)]=w+xinit; cube[1+(LSP.getSize()*0)]=0; cube[2+(LSP.getSize()*0)]=h+zadd; 
		cube[3+(LSP.getSize()*0)]=tile;
		cube[0+(LSP.getSize()*1)]=w+xend; cube[1+(LSP.getSize()*1)]=ylim; cube[2+(LSP.getSize()*1)]=h+zadd; 
		cube[3+(LSP.getSize()*1)]=tile;
		cube[0+(LSP.getSize()*2)]=w+xinit; cube[1+(LSP.getSize()*2)]=ylim; cube[2+(LSP.getSize()*2)]=h+zadd; 
		cube[3+(LSP.getSize()*2)]=tile;
		cube[0+(LSP.getSize()*3)]=w+xend; cube[1+(LSP.getSize()*3)]=ylim; cube[2+(LSP.getSize()*3)]=h+zadd; 
		cube[3+(LSP.getSize()*3)]=tile;
		cube[0+(LSP.getSize()*4)]=w+xinit; cube[1+(LSP.getSize()*4)]=0; cube[2+(LSP.getSize()*4)]=h+zadd; 
		cube[3+(LSP.getSize()*4)]=tile;
		cube[0+(LSP.getSize()*5)]=w+xend; cube[1+(LSP.getSize()*5)]=0; cube[2+(LSP.getSize()*5)]=h+zadd; 
		cube[3+(LSP.getSize()*5)]=tile;
	}
	public void insertCeiling(float[] cube,int w,int h,byte tile,float yadd,float xi,float zi,float xf,float zf)
	{
		cube[0+(LSP.getSize()*0)]=w+xi; cube[1+(LSP.getSize()*0)]=yadd; cube[2+(LSP.getSize()*0)]=h+zi; 
		cube[3+(LSP.getSize()*0)]=tile;
		cube[0+(LSP.getSize()*1)]=w+xi; cube[1+(LSP.getSize()*1)]=yadd; cube[2+(LSP.getSize()*1)]=h+zf; 
		cube[3+(LSP.getSize()*1)]=tile;
		cube[0+(LSP.getSize()*2)]=w+xf; cube[1+(LSP.getSize()*2)]=yadd; cube[2+(LSP.getSize()*2)]=h+zi; 
		cube[3+(LSP.getSize()*2)]=tile;
		cube[0+(LSP.getSize()*3)]=w+xf; cube[1+(LSP.getSize()*3)]=yadd; cube[2+(LSP.getSize()*3)]=h+zi; 
		cube[3+(LSP.getSize()*3)]=tile;
		cube[0+(LSP.getSize()*4)]=w+xi; cube[1+(LSP.getSize()*4)]=yadd; cube[2+(LSP.getSize()*4)]=h+zf; 
		cube[3+(LSP.getSize()*4)]=tile;
		cube[0+(LSP.getSize()*5)]=w+xf; cube[1+(LSP.getSize()*5)]=yadd; cube[2+(LSP.getSize()*5)]=h+zf; 
		cube[3+(LSP.getSize()*5)]=tile;
	}
	public void draw(Camera c)
	{
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
		LSP.setupAttributes();
		MatrixHelper.uploadMatrix(this.levelModelMatrix,this.LSP.getModelMatrixLoc());
		glDrawArrays(GL_TRIANGLES, 0, this.pointsNum);
		glBindBuffer(GL15.GL_ARRAY_BUFFER,0);
	}
	
	public void fullClean()
	{
		try{glDeleteBuffers(this.vbo);} catch(Exception e){}
	}
}
