package tw.gui3d;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import ivengine.view.Camera;
import ivengine.view.MatrixHelper;

import java.nio.FloatBuffer;

import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.tiles.Tile.TileState;
import tw.tiles.TileLibrary;
import tw.utils3d.EntityShaderProgram;
import tw.utils3d.LevelShaderProgram;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class EntityRenderer 
{
	private EntityShaderProgram ESP;
	private int vbo=-1;
	
	private int pointsNum=0;
	
	public EntityRenderer(EntityShaderProgram ESP)
	{
		this.ESP=ESP;
		
		this.vbo=glGenBuffers();
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
	
		ESP.enable();
		ESP.setupAttributes();
		//CREATE FLOAT BUFFER
		FloatBuffer fb=BufferUtils.createFloatBuffer(this.ESP.getSize()*3*2*6);
		//POPULATE VBO
		
		float[] cube=new float[this.ESP.getSize()*6];
		
		insertCeiling(cube,1,0,0,1,1);
		fb.put(cube);
		this.pointsNum+=ESP.getSize()*6;
		insertXLeftFace(cube,0,0,1,1);
		fb.put(cube);
		this.pointsNum+=ESP.getSize()*6;
		insertXRightFace(cube,1,0,1,1);
		fb.put(cube);
		this.pointsNum+=ESP.getSize()*6;
		insertZNearFace(cube,0,0,1,1);
		fb.put(cube);
		this.pointsNum+=ESP.getSize()*6;
		insertZFarFace(cube,1,0,1,1);
		fb.put(cube);
		this.pointsNum+=ESP.getSize()*6;
		
		fb.flip();
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
		ESP.setupAttributes();
		glBufferData(GL15.GL_ARRAY_BUFFER,this.pointsNum*4,GL15.GL_STATIC_DRAW);
		this.pointsNum=this.pointsNum/this.ESP.getSize();
		glBufferSubData(GL15.GL_ARRAY_BUFFER,0,fb);
		glBindBuffer(GL15.GL_ARRAY_BUFFER,0);
	}
	public void insertXLeftFace(float[] cube,float xadd,float zinit,float zend,float ylim)
	{
		cube[0+(ESP.getSize()*0)]=xadd; cube[1+(ESP.getSize()*0)]=0; cube[2+(ESP.getSize()*0)]=zinit; 
		cube[0+(ESP.getSize()*1)]=xadd; cube[1+(ESP.getSize()*1)]=ylim; cube[2+(ESP.getSize()*1)]=zend; 
		cube[0+(ESP.getSize()*2)]=xadd; cube[1+(ESP.getSize()*2)]=ylim; cube[2+(ESP.getSize()*2)]=zinit; 
		cube[0+(ESP.getSize()*3)]=xadd; cube[1+(ESP.getSize()*3)]=ylim; cube[2+(ESP.getSize()*3)]=zend; 
		cube[0+(ESP.getSize()*4)]=xadd; cube[1+(ESP.getSize()*4)]=0; cube[2+(ESP.getSize()*4)]=zinit; 
		cube[0+(ESP.getSize()*5)]=xadd; cube[1+(ESP.getSize()*5)]=0; cube[2+(ESP.getSize()*5)]=zend; 
	}
	public void insertXRightFace(float[] cube,float xadd,float zinit,float zend,float ylim)
	{
		cube[0+(ESP.getSize()*0)]=xadd; cube[1+(ESP.getSize()*0)]=0; cube[2+(ESP.getSize()*0)]=zinit; 
		cube[0+(ESP.getSize()*1)]=xadd; cube[1+(ESP.getSize()*1)]=ylim; cube[2+(ESP.getSize()*1)]=zinit; 
		cube[0+(ESP.getSize()*2)]=xadd; cube[1+(ESP.getSize()*2)]=ylim; cube[2+(ESP.getSize()*2)]=zend; 
		cube[0+(ESP.getSize()*3)]=xadd; cube[1+(ESP.getSize()*3)]=ylim; cube[2+(ESP.getSize()*3)]=zend; 
		cube[0+(ESP.getSize()*4)]=xadd; cube[1+(ESP.getSize()*4)]=0; cube[2+(ESP.getSize()*4)]=zend; 
		cube[0+(ESP.getSize()*5)]=xadd; cube[1+(ESP.getSize()*5)]=0; cube[2+(ESP.getSize()*5)]=zinit; 
	}
	public void insertZNearFace(float[] cube,float yadd,float xinit,float xend,float ylim)
	{
		cube[0+(ESP.getSize()*0)]=xinit; cube[1+(ESP.getSize()*0)]=0; cube[2+(ESP.getSize()*0)]=yadd; 
		cube[0+(ESP.getSize()*1)]=xinit; cube[1+(ESP.getSize()*1)]=ylim; cube[2+(ESP.getSize()*1)]=yadd; 
		cube[0+(ESP.getSize()*2)]=xend; cube[1+(ESP.getSize()*2)]=ylim; cube[2+(ESP.getSize()*2)]=yadd; 
		cube[0+(ESP.getSize()*3)]=xend; cube[1+(ESP.getSize()*3)]=ylim; cube[2+(ESP.getSize()*3)]=yadd; 
		cube[0+(ESP.getSize()*4)]=xend; cube[1+(ESP.getSize()*4)]=0; cube[2+(ESP.getSize()*4)]=yadd; 
		cube[0+(ESP.getSize()*5)]=xinit; cube[1+(ESP.getSize()*5)]=0; cube[2+(ESP.getSize()*5)]=yadd; 
	}
	public void insertZFarFace(float[] cube,float zadd,float xinit,float xend,float ylim)
	{
		cube[0+(ESP.getSize()*0)]=xinit; cube[1+(ESP.getSize()*0)]=0; cube[2+(ESP.getSize()*0)]=zadd; 
		cube[0+(ESP.getSize()*1)]=xend; cube[1+(ESP.getSize()*1)]=ylim; cube[2+(ESP.getSize()*1)]=zadd; 
		cube[0+(ESP.getSize()*2)]=xinit; cube[1+(ESP.getSize()*2)]=ylim; cube[2+(ESP.getSize()*2)]=zadd; 
		cube[0+(ESP.getSize()*3)]=xend; cube[1+(ESP.getSize()*3)]=ylim; cube[2+(ESP.getSize()*3)]=zadd; 
		cube[0+(ESP.getSize()*4)]=xinit; cube[1+(ESP.getSize()*4)]=0; cube[2+(ESP.getSize()*4)]=zadd; 
		cube[0+(ESP.getSize()*5)]=xend; cube[1+(ESP.getSize()*5)]=0; cube[2+(ESP.getSize()*5)]=zadd; 
	}
	public void insertCeiling(float[] cube,float yadd,float xi,float zi,float xf,float zf)
	{
		cube[0+(ESP.getSize()*0)]=xi; cube[1+(ESP.getSize()*0)]=yadd; cube[2+(ESP.getSize()*0)]=zi; 
		cube[0+(ESP.getSize()*1)]=xi; cube[1+(ESP.getSize()*1)]=yadd; cube[2+(ESP.getSize()*1)]=zf; 
		cube[0+(ESP.getSize()*2)]=xf; cube[1+(ESP.getSize()*2)]=yadd; cube[2+(ESP.getSize()*2)]=zi; 
		cube[0+(ESP.getSize()*3)]=xf; cube[1+(ESP.getSize()*3)]=yadd; cube[2+(ESP.getSize()*3)]=zi; 
		cube[0+(ESP.getSize()*4)]=xi; cube[1+(ESP.getSize()*4)]=yadd; cube[2+(ESP.getSize()*4)]=zf; 
		cube[0+(ESP.getSize()*5)]=xf; cube[1+(ESP.getSize()*5)]=yadd; cube[2+(ESP.getSize()*5)]=zf; 
	}
	public void draw(Camera c,float scale,float xpos,float ypos,float zpos,float r,float g,float b,float rf,float gf,float bf)
	{
		this.draw(c, scale, xpos, ypos, zpos, new Vector3f(r,g,b), new Vector3f(rf,gf,bf));
	}
	public void draw(Camera c,float scale,float xpos,float ypos,float zpos,Vector3f baseColor,Vector3f degradeColor)
	{
		glBindBuffer(GL15.GL_ARRAY_BUFFER,this.vbo);
		ESP.setupAttributes();
		Matrix4f entityModelMatrix=new Matrix4f();
		Vector3f scaleVector=new Vector3f(scale,scale,scale);
		Vector3f translateVector=new Vector3f(xpos,ypos,zpos);
		Matrix4f.translate(translateVector, entityModelMatrix, entityModelMatrix);
		Matrix4f.scale(scaleVector, entityModelMatrix, entityModelMatrix);
		MatrixHelper.uploadMatrix(entityModelMatrix,this.ESP.getModelMatrixLoc());
		MatrixHelper.uploadVector(baseColor, this.ESP.getBaseColorLocation());
		MatrixHelper.uploadVector(degradeColor, this.ESP.getColorDegrades());
		glDrawArrays(GL_TRIANGLES, 0, this.pointsNum);
		glBindBuffer(GL15.GL_ARRAY_BUFFER,0);
	}
	
	public void fullClean()
	{
		try{glDeleteBuffers(this.vbo);} catch(Exception e){}
	}
}
