package tw.gui3d;

import ivengine.view.Camera;

import org.lwjgl.input.Mouse;

import tw.utils3d.InputHandler3d;

public class CameraInstance
{
	private static final float SQRT2=(float)Math.sqrt(2);
	private static final float DEFAULT_SPEED=2.7f;
	private static final int DEFAULT_MOUSE_SENSIVITY=100;
	private static final float PIMEDIOS=(float)Math.PI/2;
	
	private float pitch=1;
	private float yaw=0;
	private float xpos=5;
	private float ypos=5;
	private float zpos=5;
	
	private Camera cam;
	
	public CameraInstance(float xi,float yi,float zi,Camera cam)
	{
		this.xpos=xi;
		this.ypos=yi;
		this.zpos=zi;
		this.cam=cam;
	}
	
	public void update(float delta, boolean cameraauto, float camobjx, float camobjy, float camobjz, float camobjpitch, float camobjyaw)
	{
		if(!cameraauto)
		{
			float yAxisPressed=InputHandler3d.isWPressed()||InputHandler3d.isSPressed()?SQRT2:1;
			float xAxisPressed=InputHandler3d.isAPressed()||InputHandler3d.isDPressed()?SQRT2:1;
			if(InputHandler3d.isWPressed()){
				moveForward(DEFAULT_SPEED*delta/xAxisPressed);
			}
			if(InputHandler3d.isAPressed()){
				moveLateral(-DEFAULT_SPEED*delta/yAxisPressed);
			}
			if(InputHandler3d.isSPressed()){
				moveForward(-DEFAULT_SPEED*delta/xAxisPressed);
			}
			if(InputHandler3d.isDPressed()){
				moveLateral(DEFAULT_SPEED*delta/yAxisPressed);
			}
		
			if(InputHandler3d.isSPACEPressed()) this.ypos+=DEFAULT_SPEED*delta;
			if(InputHandler3d.isSHIFTPressed()) this.ypos-=DEFAULT_SPEED*delta;
			
			this.addPitch(-(float)(Mouse.getDY())/DEFAULT_MOUSE_SENSIVITY);
			this.addYaw((float)(Mouse.getDX())/DEFAULT_MOUSE_SENSIVITY);
		}
		else
		{
			double yawtoobj=Math.atan2( camobjx-this.xpos,camobjz-this.zpos);
			float planedist=(float)Math.sqrt((camobjx-this.xpos)*(camobjx-this.xpos) + (camobjz-this.zpos)*(camobjz-this.zpos));
			
			float accel=planedist/3>3?2.5f:planedist/3<1?1f:planedist/3;
			float difx=(float)(Math.sin(yawtoobj))*DEFAULT_SPEED*accel*delta;
			float difz=(float)(Math.cos(yawtoobj))*DEFAULT_SPEED*accel*delta;
			float dify=(camobjy-this.ypos)*delta*2;
			
			if(Math.abs(camobjx-this.xpos)<Math.abs(difx)) difx=camobjx-this.xpos;
			if(Math.abs(camobjz-this.zpos)<Math.abs(difz)) difz=camobjz-this.zpos;
			
			this.xpos+=difx;
			this.zpos+=difz;
			this.ypos+=dify;
			
			float yawdif=(float)(camobjyaw)-this.yaw;
			if(yawdif>Math.PI) yawdif-=Math.PI*2;
			else if(yawdif<-Math.PI) yawdif+=Math.PI*2;
			this.addYaw(yawdif*delta*5);
			this.addPitch(((float)(camobjpitch)-this.pitch)*delta*5);
		}
		
		
		updateCamera();
	}
	private void updateCamera()
	{
		this.cam.moveTo(this.xpos, this.ypos, this.zpos);
		this.cam.setPitch(pitch);
		this.cam.setYaw(yaw);
	}
	void addPitch(float amount)
	{
		this.pitch+=amount;
		if(this.pitch>PIMEDIOS) this.pitch=(PIMEDIOS);
		else if(this.pitch<-PIMEDIOS) this.pitch=(-PIMEDIOS);
	}
	private void addYaw(float amount)
	{
		this.yaw+=amount;
	}
	protected void moveForward(float amount)
	{
		this.xpos+=(float)(Math.sin(this.yaw))*amount;
		this.zpos-=(float)(Math.cos(this.yaw))*amount;
	}
	protected void moveLateral(float amount)
	{
		this.zpos+=(float)(Math.sin(this.yaw)*amount);
		this.xpos+=(float)(Math.cos(this.yaw)*amount);
	}
}
