package tw.gui3d;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL20.glUniform1i;
import tw.Main;
import tw.entities.Bullet;
import tw.entities.Entity;
import tw.entities.EntityType.Faction;
import tw.entities.IAControlledCharacter;
import tw.entities.ShooterCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.utils.EntityFunctionProvider;
import tw.utils.MapManager;
import tw.utils.MapManager.MapCode;
import tw.utils3d.EntityShaderProgram;
import tw.utils3d.KeyToggleListener;
import tw.utils3d.LevelShaderProgram;
import tw.utils3d.InputHandler3d;
import tw.utils3d.TimeManager;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import ivengine.IvEngine;
import ivengine.Util;
import ivengine.properties.Cleanable;
import ivengine.view.Camera;
import ivengine.view.MatrixHelper;

public class Gui3DModule implements EntityFunctionProvider, KeyToggleListener
{
	private static final String TITLE="Triumph! War: The remake (3D)";
	private static final int X_RES=800;
	private static final int Y_RES=600;
	
	private LevelShaderProgram LSP;
	private EntityShaderProgram ESP;
	
	private TimeManager TM;
	private Camera cam;
	private CameraInstance camInstance;
	private LevelRenderer levelRenderer;
	private EntityRenderer entityRenderer;
	
	private Level level;
	private float camobjx=5;
	private float camobjy=5;
	private float camobjz=5;
	private float camobjpitch=0;
	private float camobjyaw=0;
	private boolean cameraauto=true;
	private Entity followUnit=null;
	private boolean found=false;
	public static  boolean VSYNC=true;
	private boolean hasFocus=true;
	private Thread shutdownHook;
	
	public Gui3DModule() throws LWJGLException 
	{
		IvEngine.configDisplay(X_RES, Y_RES, "Triumph! War: The remake (3D)", VSYNC, false, false);
	}
	public void run(Level level)
	{
		this.level=level;
		//Start all resources
		initResources();
		
		//Main loop
		boolean running=true;
		shutdownHook=new Thread() {
			   @Override
			   public void run() {
			    destroyApp();
			   }
			};
			InputHandler3d.addKeyToggleListener(InputHandler3d.E_VALUE, this);
		Runtime.getRuntime().addShutdownHook(shutdownHook);
		TM.getDeltaTime();
		try
		{
			while (running && this.level!=null && !this.level.gameEnded()) 
			{
				if(Display.isCloseRequested()||InputHandler3d.isESCPressed()) System.exit(0);
				Display.isActive();
				while(Keyboard.next()){
					handleKeyboardInput();
				}
				/*while(Mouse.next()){
					handleMouseInput();
				}*/
				update(TM.getDeltaTime());
				render();
				if(Display.isActive()&&!hasFocus)
				{
					hasFocus=true;
					if(!this.cameraauto) Mouse.setGrabbed(true);
				}
				else if(!Display.isActive()&&hasFocus)
				{
					hasFocus=false;
					Mouse.setGrabbed(false);
				}
				// Flip the buffers and sync to 60 FPS
				Display.update();
				Display.sync(60);
			
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		closeApp();
	}
	private void update(float delta)
	{
		TM.updateFPS();
		if(delta>0.2)delta=0.2f;
		Display.setTitle(TITLE+" FPS:"+TM.getFPS()+" CAM: "+(this.cameraauto?"FOLLOWING":"FREE"));
		//Update cam pos
		//this.world.update(delta);
		this.camInstance.update(delta,this.cameraauto,this.camobjx,this.camobjy,this.camobjz,this.camobjpitch,this.camobjyaw);
	}
	private void render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		this.LSP.enable();
		
		Matrix4f newv=new Matrix4f(this.cam.getViewMatrix());
		MatrixHelper.uploadMatrix(this.cam.getProjectionMatrix(), this.LSP.getProjectionMatrixLoc());
		MatrixHelper.uploadMatrix(newv, this.LSP.getViewMatrixLoc());
		//this.world.draw();
		this.levelRenderer.draw(this.cam);
		this.LSP.disable();
		this.ESP.enable();	
		MatrixHelper.uploadMatrix(this.cam.getProjectionMatrix(), this.ESP.getProjectionMatrixLoc());
		MatrixHelper.uploadMatrix(newv, this.ESP.getViewMatrixLoc());
		this.found=false;
		this.level.applyFunctionToAllEntities(this);
		if(!found) {
			this.followUnit=null; 
			this.camobjy=25; 
			this.camobjpitch=(float)(Math.PI/2);
			this.camobjyaw=0;
			this.camobjx=this.level.getMap().getWidth()/2;
			this.camobjz=this.level.getMap().getHeight()/2;
		}
		this.ESP.disable();
	}
	@Override
	public void applyFunctionToEntity(Entity e) {
		if(this.followUnit==e||(followUnit==null&& e instanceof ShooterCharacter &&this.level.isPartOfPlayers((ShooterCharacter)(e)))){
			if(followUnit==null) this.followUnit=e;
			found=true;
			this.camobjy=e.getSize()/2;
			this.camobjpitch=0;
			
			if(e instanceof IAControlledCharacter)
			{
				switch(((IAControlledCharacter)(e)).getLookingDirection())
				{
						case UP:
							this.camobjx=e.getX()+e.getSize()/2;
							this.camobjz=e.getY()-0.001f;
							this.camobjyaw=0;
							break;
						case DOWN:
							this.camobjx=e.getX()+e.getSize()/2;
							this.camobjz=e.getY()+e.getSize()+0.001f;
							this.camobjyaw=(float) (Math.PI);
							break;
						case LEFT:
							this.camobjx=e.getX()-0.001f;
							this.camobjz=e.getY()+e.getSize()/2;
							this.camobjyaw=(float)(-Math.PI/2);
							break;
						case RIGHT:
							this.camobjx=e.getX()+e.getSize()+0.001f;
							this.camobjz=e.getY()+e.getSize()/2;
							this.camobjyaw=(float)(Math.PI/2);
							break;
						case UPRIGHT:
							this.camobjx=e.getX()+e.getSize()+0.001f;
							this.camobjz=e.getY()-0.001f;
							this.camobjyaw=(float)(Math.PI/4);
							break;
						case UPLEFT:
							this.camobjx=e.getX()-0.001f;
							this.camobjz=e.getY()-0.001f;
							this.camobjyaw=(float)(-Math.PI/4);
							break;
						case DOWNRIGHT:
							this.camobjx=e.getX()+e.getSize()+0.001f;
							this.camobjz=e.getY()+e.getSize()+0.001f;
							this.camobjyaw=(float)(Math.PI*3/4);
							break;
						case DOWNLEFT:
							this.camobjx=e.getX()-0.001f;
							this.camobjz=e.getY()+e.getSize()+0.001f;
							this.camobjyaw=(float)(-Math.PI*3/4);
							break;
				}
			}
		}
		float r=e.getType().getFaction()==Faction.ALIEN?0.5f:0.2f;
		float b=e.getType().getFaction()==Faction.MARINE?0.5f:0.2f;
		float g=0.2f;
		this.entityRenderer.draw(this.cam, e.getSize(), e.getX(), 0, e.getY(), r,g,b,r+0.3f,g+0.3f,b+0.3f);	
	}
	@Override
	public void applyFunctionToBullet(Bullet b) 
	{
		Vector3f bulletColor=null;
		if(b.getFaction()==Faction.MARINE) bulletColor=new Vector3f(0,0,0.8f);
		else if(b.getFaction()==Faction.ALIEN) bulletColor=new Vector3f(0.8f,0,0);
		else bulletColor=new Vector3f(0,1.0f,0);
		this.entityRenderer.draw(this.cam, 0.15f, b.getX()-0.075f, 0.425f, b.getY()-0.075f,bulletColor,bulletColor);	
	}
	private void initResources()
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL11.GL_CULL_FACE);
		//GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		//glEnable( GL11.GL_BLEND );
		//(0.6, 0.8, 1.0, 1.0);
		glClearColor(0.6f, 0.8f, 1.0f, 0f);
		//glClearColor(0.5f, 0.7f, 1.0f, 0f);
		//Set-up shader program
		this.LSP=new LevelShaderProgram(false);
		this.ESP=new EntityShaderProgram(false);
		this.TM=new TimeManager();
		this.cam=new Camera(0.1f,1000,80f,X_RES/Y_RES);
		this.camInstance=new CameraInstance(5,5,5,this.cam);
		this.levelRenderer=new LevelRenderer(this.level.getMap().getTiles(),this.LSP);
		this.entityRenderer=new EntityRenderer(this.ESP);
		//this.world=new World(this.VSP,this.cam);
		//this.hud=new Hud(this.HSP,X_RES,Y_RES);
		//Load textures here
		Util.loadPNGTexture(Main.class.getResource("/assets/maptiles_original.png"), GL13.GL_TEXTURE0);
		
		glUniform1i(this.LSP.getTilesTextureLocation(), 0);
	}
	private void handleKeyboardInput()
	{
		switch(Keyboard.getEventKey())
		{
		case Keyboard.KEY_W:
			InputHandler3d.setW(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_A:
			InputHandler3d.setA(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_S:
			InputHandler3d.setS(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_D:
			InputHandler3d.setD(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_SPACE:
			InputHandler3d.setSPACE(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_LSHIFT:
			InputHandler3d.setSHIFT(Keyboard.getEventKeyState());
			break;
		case Keyboard.KEY_ESCAPE:
			InputHandler3d.setESC(Keyboard.getEventKeyState());
			break;	
		case Keyboard.KEY_E:
			InputHandler3d.setE(Keyboard.getEventKeyState());
			break;	
		}
	}
	private void closeApp()
	{
		this.fullClean();
		// Dispose any resources and destroy our window
		this.level=null;
	}
	private void destroyApp()
	{
		try{Runtime.getRuntime().removeShutdownHook(shutdownHook);}catch(Exception e){}
		closeApp();
		try{Display.destroy();} catch(Exception e){}
	}

	public void fullClean() {
		//world.fullClean();
		//VSP.fullClean();
		this.entityRenderer.fullClean();
		this.levelRenderer.fullClean();
		try
		{
			InputHandler3d.removeKeyToggleListener(InputHandler3d.E_VALUE, this);
			this.LSP.fullClean();
			this.ESP.fullClean();
			this.TM=null;
			this.cam=null;
		}
		catch(Exception e){}
	}
	@Override
	public void notifyKeyToggle(int code) 
	{
		this.cameraauto=!this.cameraauto;
		if(!this.cameraauto) Mouse.setGrabbed(true);
		else Mouse.setGrabbed(false);
	}
}
