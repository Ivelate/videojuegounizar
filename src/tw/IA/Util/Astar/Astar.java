package  tw.IA.Util.Astar;
import java.util.ArrayList;

import tw.IA.Util.BinaryHeap.MonticuloBinario;


/**
 * 
 * Algoritmo A estrella que busca el mejor camino entre dos puntos(nodos) en un mapa
 * @author joseangel
 *
 */
public class Astar {
	
	private Nodo[][] mapa;
	@SuppressWarnings("unused")
	private Nodo nodoInicial;
	private Nodo nodoFinal;
	private byte[][] byteMatrix;
	
	public Astar(Nodo[][] mapa){
		this.mapa=mapa;
		
	}
	public Astar(byte[][] map){
		this.mapa=new Nodo[map.length][map[0].length];
		this.byteMatrix=map;
		for(int i=0;i<this.mapa.length;i++){
			for(int j=0;j<this.mapa[0].length;j++) {
				if(		   map[i][j]==0 
						|| map[i][j]==-118
						|| map[i][j]==-96
						|| map[i][j]==-95
						|| map[i][j]==5 
						|| map[i][j]==9 
						|| map[i][j]==17 
						|| map[i][j]==20 
						|| map[i][j]==-128 
						|| map[i][j]==112 
						|| map[i][j]==105 
						|| map[i][j]==99 
						|| map[i][j]==33
						) {this.mapa[i][j]=new Nodo(map[i][j],i,j);}
				else this.mapa[i][j]=null;
				}
		}
	}
	
	
	public Astar(Nodo[][] mapa,Nodo PInicial, Nodo PFinal){
		this.mapa=mapa;
		this.nodoInicial=PInicial;
		this.nodoFinal=PFinal;
	}
	public Nodo[][] GetNodeMatrix(){
		return this.mapa;
	}
	public boolean existNode(int x,int y){
		return GetNodeMap(x,y)!=null;
	}
	public Nodo GetNodeMap(int i,int j){
		if(i>=this.mapa.length||j>=this.mapa[0].length||i<0||j<0) return null;
		return this.mapa[i][j];
	}
	
	public Nodo GetNodeAtPosition(double x,double y){
		return this.mapa[(int) (x/mapa[0][0].getXTileSize())][(int) (y/mapa[0][0].getYTileSize())];
	}
	public ArrayList<Nodo> calcularCamino(Nodo NInicial,Nodo NFinal,boolean HayDiagonales){
		MonticuloBinario<Nodo> frontera= new MonticuloBinario<Nodo>(new ComparadorNodos(NFinal,new HeuristicaDistancia()),50);
		ArrayList<Nodo> explorados=new ArrayList<Nodo>();
		this.nodoFinal=NFinal;
		Nodo nodoActual=null;
		boolean CaminoEncontrado=false;
		
		//a�adimos el nodo actual a la lista de nodos a expandir
		frontera.insertar(NInicial);
		
		//mientras no hayamos encontrado el camino y hayamos explorado todos los nodos seguimos expandiendo nodos
		while(!frontera.EsVacia() && !CaminoEncontrado){
			
			//extraemos el mejor nodo
			nodoActual=frontera.getCima();
			// y lo a�adimos a los nodos explorados
			explorados.add(nodoActual);
			//Expandimos los nodos al nodo actual;
			ArrayList<Nodo> nodosAdyacentes=GetNodosAdyacentes(nodoActual,HayDiagonales);	
			
			//Comprobamos si en los nodos expandidos esta la solucion
			while(!nodosAdyacentes.isEmpty() && !CaminoEncontrado){
				
				//comprobamos el nodo adyacente
				Nodo NodoAdyacente=nodosAdyacentes.remove(0);
				
				//si el nodo no esta explorado o en la frontera lo a�adimos a explorados
				if(!explorados.contains(NodoAdyacente) && !frontera.contiene(NodoAdyacente)){
					
					NodoAdyacente.SetNodoAnteriorCamino(nodoActual);
					
					frontera.insertar(NodoAdyacente);
					
					//si el nodo expandido es un nodo final se ha encontrado un camino
					
					if(this.nodoFinal==NodoAdyacente) CaminoEncontrado=true;
				}
				//hay un nodo con mayor coste por lo tanto lo reemplazamos
				else if(!explorados.contains(NodoAdyacente) && frontera.contiene(NodoAdyacente)){
					
					int nuevoCoste=NodoAdyacente.GetcosteG();
					
					//si los nodos son ortogonales a�adimos coste 10
					if(NodoAdyacente.GetFila()==nodoActual.GetFila() || NodoAdyacente.GetColumna()==nodoActual.GetColumna()){
						
						nuevoCoste+=10;
					}
					//si los nodos son diagonales a�adimos coste 14
					else nuevoCoste+=14;
					//si el nuevo conste es mejor que el anterior
					if(nuevoCoste<NodoAdyacente.GetcosteG()){						
						NodoAdyacente.SetNodoAnteriorCamino(nodoActual);
					}
				}
				
			}			
		}
		//si hemos econtrado un camino lo reordenamos
		/*
		 * Establecemos los puntos por los que tiene que ir
		 */
		if(CaminoEncontrado) {
			ArrayList<Nodo> camino = new ArrayList<Nodo>();
			Nodo aux= this.nodoFinal;
			while(aux!=null) {
				//a�adimos el camino
				camino.add(0,aux);
				//obtenemos el paso anterior
				aux=aux.getNodoAnteriorCamino();
			}
			//Reseteamos los posibles caminos
			for(int i=0;i<mapa.length;i++)
				for (int j=0;j<mapa[0].length;j++) if(mapa[i][j]!=null) mapa[i][j].SetNodoAnteriorCamino(null);
			return camino;			
		}
		//si no hemos encontrado el camino devolvemos null
		
		else {
			return null;			
		}
	}
	

	
	private ArrayList<Nodo> GetNodosAdyacentes(Nodo nodo,boolean Diagonales){
		ArrayList<Nodo> Adyacentes= new ArrayList<Nodo>();
		//voy a la izquierda(no expando nodos derechos)			
			/*
			 * Expando todos los nodos de la columna actual
			 */
		if(nodo==null) return new ArrayList<Nodo>(); // | TODO ESTO LO HE A�ADIDO YO A VER SI VA PERO CREO QUE NO EH
			if(nodo.GetFila()+1 <mapa.length 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& mapa[nodo.GetFila()+1][nodo.GetColumna()]!=null){
				Adyacentes.add(mapa[nodo.GetFila()+1][nodo.GetColumna()]);
				mapa[nodo.GetFila()+1][nodo.GetColumna()].SetCosteG(nodo.GetcosteG()+10);
			}				
			if(0<=nodo.GetFila()-1  
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
											&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& mapa[nodo.GetFila()-1][nodo.GetColumna()]!=null){
				Adyacentes.add(mapa[nodo.GetFila()-1][nodo.GetColumna()]);
				mapa[nodo.GetFila()-1][nodo.GetColumna()].SetCosteG(nodo.GetcosteG()+10);
			}		
			
			if(0<=nodo.GetColumna()-1 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
											&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& mapa[nodo.GetFila()][nodo.GetColumna()-1]!=null){
					Adyacentes.add(mapa[nodo.GetFila()][nodo.GetColumna()-1]);
					mapa[nodo.GetFila()][nodo.GetColumna()-1].SetCosteG(nodo.GetcosteG()+10);
				
			}		
			if(nodo.GetColumna()+1 <mapa[0].length 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
											&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& mapa[nodo.GetFila()][nodo.GetColumna()+1]!=null){
					Adyacentes.add(mapa[nodo.GetFila()][nodo.GetColumna()+1]);
					mapa[nodo.GetFila()][nodo.GetColumna()+1].SetCosteG(nodo.GetcosteG()+10);
			}	
			
			if(Diagonales  && nodo.GetFila()+1 <mapa.length  
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
											&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& nodo.GetColumna()+1 <mapa[0].length 
					&& mapa[nodo.GetFila()+1][nodo.GetColumna()+1]!=null){						
					Adyacentes.add(mapa[nodo.GetFila()+1][nodo.GetColumna()+1]);
					mapa[nodo.GetFila()+1][nodo.GetColumna()+1].SetCosteG(nodo.GetcosteG()+15);
			}
			
			if(Diagonales && 0<=nodo.GetFila()-1 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
											&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& 0<=nodo.GetColumna()-1  
					&& mapa[nodo.GetFila()-1][nodo.GetColumna()-1]!=null){
					Adyacentes.add(mapa[nodo.GetFila()-1][nodo.GetColumna()-1]);
					mapa[nodo.GetFila()-1][nodo.GetColumna()-1].SetCosteG(nodo.GetcosteG()+14);
			}	
								
			if(Diagonales && nodo.GetColumna()+1 <mapa[0].length 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& 0<=nodo.GetFila()-1  
					&& mapa[nodo.GetFila()-1][nodo.GetColumna()+1]!=null){
					Adyacentes.add(mapa[nodo.GetFila()-1][nodo.GetColumna()+1]);
					mapa[nodo.GetFila()-1][nodo.GetColumna()+1].SetCosteG(nodo.GetcosteG()+15);
			}	
			
			if(Diagonales && 0<=nodo.GetColumna()-1 
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=105
					&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=-128
							&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=99
									&& byteMatrix[nodo.GetFila()][nodo.GetColumna()]!=112
					&& nodo.GetFila()+1 <mapa.length
					&& mapa[nodo.GetFila()+1][nodo.GetColumna()-1]!=null){
					Adyacentes.add(mapa[nodo.GetFila()+1][nodo.GetColumna()-1]);
					mapa[nodo.GetFila()+1][nodo.GetColumna()-1].SetCosteG(nodo.GetcosteG()+15);
			}				
		return Adyacentes;
	}
	

}
