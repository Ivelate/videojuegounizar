package tw.IA.Util.Astar;

import java.util.Comparator;

public class ComparadorNodos implements Comparator<Nodo>{
	
	private Nodo nodoF;
	private Heursitica h;
	
	public ComparadorNodos(Nodo nodoF,Heursitica h) {
		// TODO Auto-generated constructor stub
		this.nodoF=nodoF;
		this.h=h;
	}
	
	
	public int compare(Nodo lhs, Nodo rhs) {
		if(lhs.costeNodo(nodoF, h)<rhs.costeNodo(nodoF, h)) return -1; //este nodo es mejor
		else if(lhs.costeNodo(nodoF, h)>rhs.costeNodo(nodoF, h)) return 1;
		else return 0;
	}

}
