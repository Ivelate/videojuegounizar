package tw.IA.Util.Astar;




/**
 * 
 * Clase que define un nodo para el algoritmo a star
 * los nodos ortogonales sumaran 10 y los diagonales 14 al coste
 * 
 */
public class Nodo{
	
	private boolean transitable;
//	private int PesoNodo; //coste del nodo(barro, agua , etc)
	
	private int costeG; //peso actual	
	private int fila,columna;
	private Nodo NodoAnteriorCamino;  //nodo anterior del camino
	private Nodo NodoSigCamino; //siguiente nodo del camino
	private byte Tilebyte;
	
	/*
	 * 
	 */
	public Nodo(byte Tile,int fila,int columna){
		this.Tilebyte=Tile;
		this.fila=fila;
		this.columna=columna;
		this.transitable=true;
		this.NodoAnteriorCamino=null;
		this.NodoSigCamino=null;
	}
	public byte GetTile(){
		return this.Tilebyte;
		
	}
	public int GetFila(){
		return this.fila;
	}
	
	public int GetColumna(){
		return this.columna;
	}
	public double costeNodo(Nodo NodoFinal,Heursitica h){
		return this.costeG+h.Distancia(this,NodoFinal);
	}
	
	public int getXTileSize(){
		return 30;
	}
	public int getYTileSize(){
		return 30;
	}

	public float GetCenterX(){
		return (float) ((2*(fila+1)-2)*1.0/2);
	}
	public float GetCenterY(){
		return (float) ((2*(columna+1)-2)*1.0/2);
	}
	
	public int GetcosteG(){
		return this.costeG;
	}
	
	public void SetCosteG(int coste){
		this.costeG=coste;		
	}
	public boolean esTransitable(){
		return this.transitable;
	}
	
	public Nodo getNodoAnteriorCamino(){
		return this.NodoAnteriorCamino;
	}
	public void SetNodoAnteriorCamino(Nodo node){
		this.NodoAnteriorCamino=node;
	}
	
	public Nodo getSigNodoCamino(){
		return this.NodoSigCamino;
	}
	public void SetSigNodoCamino(Nodo node){
		this.NodoSigCamino=node;
	}
	
	public String GetStringPosition(){
		return "Nodo["+this.GetFila()+","+this.GetColumna()+"] ("+this.GetCenterX()+"-"+this.GetCenterY()+")";
	}
}
