package tw.IA.BinaryHeap;

import java.util.Comparator;

public class MonticuloBinario<T> implements OperacionesArbol<T> {
	
	private Comparator<T> comparador;
	private int nodosInsertados;
	private int MaxSize;
	private T[] arbol;
	
	@SuppressWarnings("unchecked")
	public MonticuloBinario(Comparator<T> comparator,int size){
		this.MaxSize=size;
		this.arbol= (T[]) new Object[MaxSize];
		this.comparador=comparator;
		this.nodosInsertados=0;
		
	}
	public boolean insertar(T dato) {
		boolean debeSubir;
		if(nodosInsertados==MaxSize) return false;
		else{
			nodosInsertados++;
			arbol[nodosInsertados-1]=dato;
			int i=nodosInsertados;
			//-1 para monticulo minimos d[i]<d[i/2]
			if(nodosInsertados>1) debeSubir=comparador.compare(arbol[i-1],arbol[(i/2)-1])==-1;
			else debeSubir=false;
			while(i>1 && debeSubir){
				T aux=arbol[i-1];
				arbol[i-1]=arbol[(i/2)-1];
				arbol[(i/2)-1]=aux;
				i=i/2;
				if(i>1) debeSubir=comparador.compare(arbol[i-1],arbol[(i/2)-1])==-1;
				else debeSubir=false;
			}
			return true;
		}		
	}
	private boolean BorrarMinimo(){
		if(arbol[0]==null) return false;
		else{
			arbol[0]=arbol[nodosInsertados-1];
			nodosInsertados=nodosInsertados-1;
			int i=1;
			int j;
			while(i<=(nodosInsertados/2)){
				if((comparador.compare(arbol[(2*i)-1],arbol[2*i])==-1) || (2*i==nodosInsertados)){
					j=2*i;
				}
				else j=(2*i)+1;
				if(comparador.compare(arbol[i-1],arbol[j-1])==1){
					T aux=arbol[i-1];
					arbol[i-1]=arbol[j-1];
					arbol[j-1]=aux;
					i=j;
				}
				else i=nodosInsertados;
			}
			return true;
		}		
	}
	
	public T getCima(){
		T aux=arbol[0];
		BorrarMinimo();
		return aux;
	}
	
	public boolean borrar(T nodo) {
		return false;
	}
	public boolean modificarValor(int indice, T dato) {
		// TODO Auto-generated method stub
		return false;
	}
	public String listar() {
		String lista="";
		for(int i=0;i<arbol.length;i++){
			lista+=arbol[i]+"\n";
		}
		return lista;
	}
	
	public boolean contiene(T dato){
		
		boolean encontrado=false;
		for(T d:arbol) if(dato.equals(d)) encontrado=true;
		return encontrado;
	}
	
	public boolean MonticuloLleno(){
		return nodosInsertados==MaxSize;
	}
	public boolean EsVacia(){
		return nodosInsertados==0;
	}

}
