package tw.IA.BinaryHeap;

public interface OperacionesArbol<T> {
	
	
	public boolean insertar(T nodo);
	public boolean borrar(T nodo);
	public boolean modificarValor(int indice,T dato);
	public String listar();
	

}
