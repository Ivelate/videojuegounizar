package tw.menu;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;

import tw.Main;

public class MenuJuego extends JComponent{
	
	private final Rectangle Resume= new Rectangle(Main.sc.getWidth()/2+62, Main.sc.getHeight()/2-170, 190,35);
	private final Rectangle Options= new Rectangle(Main.sc.getWidth()/2+62, Main.sc.getHeight()/2-120, 190, 35);
	private final Rectangle Exit= new Rectangle(Main.sc.getWidth()/2+62, Main.sc.getHeight()/2-70, 190, 35);
	private Rectangle selectedBounds;
	boolean visible=false;
	private Font fuente; 
	private MouseAdapter mouse;
	
	public MenuJuego(){
		try {
			fuente=Font.createFont(Font.TRUETYPE_FONT, new File("rsc/assets/font/DeBorstel Brush-Reduced.ttf"));
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.visible=false;
		Main.sc.add(this);
		IniciarRaton();
		repaint();		
	}	
	
	private void IniciarRaton(){
		mouse= new MouseAdapter() {
			/*
			 * Metdodo que sirve para que cuando el raton pase por un boton este cambie de color
			 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
			 */
			public void mouseMoved(MouseEvent e){
				 if (getResumeBounds().contains(e.getPoint())) {
                     selectedBounds = getResumeBounds();
                 } else if (getOptionsBounds().contains(e.getPoint())) {
                     selectedBounds = getOptionsBounds();
                 } 
                 else if(getExitBounds().contains(e.getPoint())){
                	 selectedBounds = getExitBounds();
                 }else {
                     selectedBounds = null;
                 }
                 repaint();
			}
				/*
				 * Evento de pulsacion de boton
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
			  @Override
              public void mouseClicked(MouseEvent e) {
                  if (getResumeBounds().contains(e.getPoint())) {
                	  Acticar_Desactivar();
                  }else if (getOptionsBounds().contains(e.getPoint())) {
                      
                  }else if (getExitBounds().contains(e.getPoint())) {
                     System.exit(0);
                  }
              }
			
		};
		
		 addMouseListener(mouse);
         addMouseMotionListener(mouse);
	}
	
	
	 protected void drawText(Graphics2D g2d, String text, Rectangle bounds) {

         FontMetrics fm = g2d.getFontMetrics();
         g2d.setColor(Color.GRAY);
         g2d.fill(bounds);
         g2d.setColor(new Color(240,240,240));
         if (selectedBounds != null) {
             if (bounds.contains(selectedBounds)) {
            	 g2d.setColor(new Color(200,200,200,0));
                 Rectangle fill = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
                 g2d.fill(fill);
                 g2d.setColor(Color.BLUE);
             }
         }         
        	g2d.setFont(fuente.deriveFont(Font.PLAIN, 28)); 
			g2d.drawString(
	                 text,
	                 bounds.x + ((bounds.width - fm.stringWidth(text)) / 2),
	                 bounds.y + ((bounds.height - fm.getHeight()) / 2) + fm.getAscent()+5);
         

     }
	
	
	 protected Rectangle getResumeBounds() {
         return getButtonBounds(Resume);
     }

     protected Rectangle getOptionsBounds() {
         return getButtonBounds(Options);
     }
     
     protected Rectangle getExitBounds() {
         return getButtonBounds(Exit);
     }
     
     protected Rectangle getButtonBounds(Rectangle masterBounds) {
         Rectangle bounds = new Rectangle(masterBounds);
         Point p = getImageOffset();
         bounds.translate(p.x, p.y);
         return bounds;
     }

     
     protected Point getImageOffset() {

         Point p = new Point();
             p.x = (getWidth() - tw.Main.sc.getWidth()) / 2;
             p.y = (getHeight() - tw.Main.sc.getHeight()) / 2;        

         return p;

     }
     
	
	public void Draw(Graphics2D g){
		 Graphics2D g2 = (Graphics2D) g;
		if(this.visible)paintComponent(g2);
	}
	
	
     public void paintComponent(Graphics2D g2)
     {
		 
		  g2.setColor(new Color(240,240,240));
		  g2.fillRect(Main.sc.getWidth()/2+50, Main.sc.getHeight()/2-250, 200, 300);
		  drawText(g2, "Resume_________", getResumeBounds());
		  drawText(g2, "Options", getOptionsBounds());
		  drawText(g2, "Exit", getExitBounds());
		 
		  g2.setColor(Color.BLUE);
		  //g2.setFont(arg0);
		  g2.setFont(fuente.deriveFont(Font.PLAIN, 24));
		  g2.drawString("Game Options", Main.sc.getWidth()/2+62, Main.sc.getHeight()/2-225);
		  
		  
		  g2.dispose();
     }

	 public  boolean Acticar_Desactivar(){
		 System.out.println("Pausa detectada");
		 this.visible=!this.visible;
		 System.out.println(this.visible);
		 this.setVisible(visible);
		 return this.visible;
	 }

}
