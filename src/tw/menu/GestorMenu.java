package tw.menu;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JComponent;
import javax.swing.JPanel;

import tw.Main;
import tw.audio.AudioManager;
import tw.audio.DefaultAudioManager;
import tw.game.GameManager;
import tw.gui.DrawingManager2D;

public class GestorMenu extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static enum Pantallas{MENUINICIO,OPCIONESJUEGO,MENUOPCIONESJUEGO,INTROUNJUGADOR,INTRODOSJUGADORES
		,PARIDAUNJUGADR,PARTIDADOSJUGADORES};
	private static CrearFondo fondo= new CrearFondo("rsc/assets/Menu/Pagina_inicio.jpg");
	private static JPanel pantalla= new JPanel();
	private static JComponent recursos;
	public KeyAdapter key;
	
	
	public GestorMenu(){
		final GestorMenu gm=this;
		key= new KeyAdapter() {
			 public void keyPressed(KeyEvent e){
				synchronized(gm){ gm.notifyAll();}
				 
			 }
		};
	}
	
	public JPanel selecionarPantalla(Pantallas p){
		
		
		if(fondo.fondoCargado()){
			pantalla.setSize(Main.sc.getSize());
			fondo.CargarRecursos();
			pantalla=fondo;
		}
		if(recursos!=null){
			pantalla.remove(recursos);
			pantalla.revalidate();
			recursos.removeAll();
			
		}
		
		switch (p){
		case MENUINICIO: 
		System.out.println("Dibujo");
		recursos= new tw.menu.MenuInicio();
		pantalla.add(recursos,BorderLayout.CENTER);
		break;
		
		case MENUOPCIONESJUEGO:
		recursos= new tw.menu.MenuOpcionesJuego();
		pantalla.add(recursos,BorderLayout.CENTER);
		break;
		
		//Intro un Jugador
		case INTROUNJUGADOR:
			Main.sc.remove(pantalla);
			fondo=new CrearFondo("rsc/assets/Menu/Game_intro.png");
			pantalla=fondo;
			Main.sc.add(pantalla);
			Main.sc.addKeyListener(key);
			Main.sc.requestFocusInWindow();

		break;
		
		//Intro Dos Jugadores
		case INTRODOSJUGADORES:
			Main.sc.remove(pantalla);
			fondo=new CrearFondo("rsc/assets/Menu/Game_intro.png");
			pantalla=fondo;
			Main.sc.add(pantalla);
			Main.sc.addKeyListener(key);
			Main.sc.requestFocusInWindow();
		break;
	 
		case PARIDAUNJUGADR:
			Main.sc.getContentPane().removeAll();
			Main.sc.repaint();
			Main.sc.removeKeyListener(key);
			
			
		break;
		
		case PARTIDADOSJUGADORES:
			Main.sc.getContentPane().removeAll();
			Main.sc.repaint();
			Main.sc.removeKeyListener(key);
		break;
		case OPCIONESJUEGO:
			recursos= new tw.menu.GameOptions();
			pantalla.add(recursos,BorderLayout.CENTER);
			break;
		default:
			break;
				
		}
		
		return pantalla;
	}
	
	
	
	
}
