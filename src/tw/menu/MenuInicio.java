package tw.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;

import tw.Main;
import tw.menu.GestorMenu.Pantallas;

public class MenuInicio extends JComponent implements InterfazMenu{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Rectangle NEW_GAME_BOUNDS = new Rectangle((Main.sc.getWidth()/2)-180,Main.sc.getHeight()-300, 262, 85);
	private static Rectangle GAME_OPTIONS = new Rectangle((Main.sc.getWidth()/2)-180,Main.sc.getHeight()-200, 262, 85);	
    private static Rectangle EXIT_GAME_BOUNDS = new Rectangle((Main.sc.getWidth()/2)-180,Main.sc.getHeight()-100, 262, 85);
    private Rectangle selectedBounds;
	private Font fuente; 
    private MouseAdapter mouse;
    
    
	public MenuInicio(){
		try {
			fuente=Font.createFont(Font.TRUETYPE_FONT, new File("rsc/assets/font/DeBorstel Brush-Reduced.ttf"));
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setPreferredSize(Main.sc.getSize());
		IniciarRaton();
		repaint();
		
	}
	
	
	private void IniciarRaton(){
		mouse= new MouseAdapter() {
			/*
			 * Metdodo que sirve para que cuando el raton pase por un boton este cambie de color
			 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
			 */
			public void mouseMoved(MouseEvent e){
				 if (getNewGameBounds().contains(e.getPoint())) {
                     selectedBounds = getNewGameBounds();
                 } else if (getExitGameBounds().contains(e.getPoint())) {
                     selectedBounds = getExitGameBounds();
                 }else if (getOptionsBounds().contains(e.getPoint())) {
                	 selectedBounds=getOptionsBounds();
				 }else {
                     selectedBounds = null;
                 }
                 repaint();
			}
				/*
				 * Evento de pulsacion de boton
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
			  @Override
              public void mouseClicked(MouseEvent e) {
                  if (getNewGameBounds().contains(e.getPoint())) {
                	  Main.Menu.selecionarPantalla(Pantallas.MENUOPCIONESJUEGO);
                  }else if (getOptionsBounds().contains(e.getPoint())) {
                      Main.Menu.selecionarPantalla(Pantallas.OPCIONESJUEGO);
                  }else if (getExitGameBounds().contains(e.getPoint())) {
                     System.exit(0);
                  }
              }
			
		};
		
		 addMouseListener(mouse);
         addMouseMotionListener(mouse);
	}
	
	protected Point getImageOffset() {

        Point p = new Point();
            p.x = (getWidth() - Main.sc.getWidth()) / 2;
            p.y = (getHeight() - Main.sc.getHeight()) / 2;        

        return p;

    }
	
	/*
	 * Metodo que dibuja las componentes
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g){
//		int width = this.getSize().width;
//		int height = this.getSize().height;
		super.paintComponent(g);
	    Graphics2D g2d = (Graphics2D) g.create();			
	    drawText(g2d, "Start Game", getNewGameBounds());
	    drawText(g2d, "         Options", getOptionsBounds());
		drawText(g2d, "_________Exit", getExitGameBounds());
		g2d.dispose();
		setOpaque(false);		
	}
	
	 /*
	  * Metodo que dibuja un rectangulo y su texto, si el raton esta encima del boton este cambia de forma
	  */
	 protected void drawText(Graphics2D g2d, String text, Rectangle bounds) {

         FontMetrics fm = g2d.getFontMetrics();
         g2d.setColor(new Color(0,0,0,0));
         g2d.fill(bounds);
         g2d.setColor(Color.WHITE);
         if (selectedBounds != null) {
             if (bounds.contains(selectedBounds)) {
            	 g2d.setColor(new Color(200,200,200,0));
                 Rectangle fill = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
                 g2d.fill(fill);
                 g2d.setColor(Color.BLACK);
             }
         }         
        	g2d.setFont(fuente.deriveFont(Font.PLAIN, 28)); 
			g2d.drawString(
	                 text,
	                 bounds.x + ((bounds.width - fm.stringWidth(text)) / 2),
	                 bounds.y + ((bounds.height - fm.getHeight()) / 2) + fm.getAscent());
         

     }
	 	 
	 protected Rectangle getNewGameBounds() {
         return getButtonBounds(NEW_GAME_BOUNDS);
     }

     protected Rectangle getExitGameBounds() {
         return getButtonBounds(EXIT_GAME_BOUNDS);
     }
     
     protected Rectangle getOptionsBounds() {
         return getButtonBounds(GAME_OPTIONS);
     }
     
     
     /*
      * Crea un rectangulo copia
      */
     protected Rectangle getButtonBounds(Rectangle masterBounds) {
         Rectangle bounds = new Rectangle(masterBounds);
         Point p = getImageOffset();
         bounds.translate(p.x, p.y);
         return bounds;
     }


	@Override
	public void CargarRecursos() {
		repaint();
		
	}


	@Override
	public void DescargarRecursos() {
		NEW_GAME_BOUNDS = null;
	    EXIT_GAME_BOUNDS = null;
	    selectedBounds=null;
		fuente=null; 
	    mouse=null;
		
	}
}
