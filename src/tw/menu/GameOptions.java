package tw.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;

import tw.Main;
import tw.gui3d.Gui3DModule;
import tw.menu.GestorMenu.Pantallas;

public class GameOptions extends JComponent implements InterfazMenu{
	
	/**
	 * 
	 */
	private boolean tempFullScreen;
	private boolean tempNative;
	private boolean tempVSYNC;
	private boolean tempVisor;
	
	private static final long serialVersionUID = 1L;
	private static Rectangle FullScreen = new Rectangle(10,150, 262, 85);
	private static Rectangle forceNativeFullscreen = new Rectangle(20,200, 262, 85);	
    private static Rectangle VSYNC = new Rectangle(20,250, 262, 85);
    private static Rectangle Visor = new Rectangle(20,320, 262, 85);
    private static Rectangle Acept = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-200, 262, 85);
    private static Rectangle Back  = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-150, 262, 85);
    private Rectangle selectedBounds;
	private Font fuente; 
    private MouseAdapter mouse;
    
    
	public GameOptions(){
		// TODO Auto-generated constructor stub
		this.tempFullScreen=Main.Full_Screen;
		this.tempNative=Main.forceNativeFullscreen;
		this.tempVSYNC=Gui3DModule.VSYNC;
		this.tempVisor=Main.Visor;
		try {
			fuente=Font.createFont(Font.TRUETYPE_FONT, new File("rsc/assets/font/DeBorstel Brush-Reduced.ttf"));
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setPreferredSize(Main.sc.getSize());
		IniciarRaton();
		repaint();
		
	}
	
	
	private void IniciarRaton(){
		mouse= new MouseAdapter() {
			/*
			 * Metdodo que sirve para que cuando el raton pase por un boton este cambie de color
			 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
			 */
			public void mouseMoved(MouseEvent e){
				 if (getFullScreenBounds().contains(e.getPoint())) {
                     selectedBounds = getFullScreenBounds();
                 } else if (getforceNativeFullscreenBounds().contains(e.getPoint())) {
                     selectedBounds = getforceNativeFullscreenBounds();
                 } else if (getVSYNCBounds().contains(e.getPoint())) {
                     selectedBounds = getVSYNCBounds();
                 } else if (getAceptBounds().contains(e.getPoint())) {
                     selectedBounds = getAceptBounds();
                 } else if (getBackBounds().contains(e.getPoint())) {
                     selectedBounds = getBackBounds();
                 } else if (getVisorBounds().contains(e.getPoint())) {
                     selectedBounds = getVisorBounds();
                 }  
                 else {
                     selectedBounds = null;
                 }
                 repaint();
			}
				/*
				 * Evento de pulsacion de boton
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
			  @Override
              public void mouseClicked(MouseEvent e) {
                  if (getFullScreenBounds().contains(e.getPoint())) {
                	  tempFullScreen=! tempFullScreen;
                  }else if (getforceNativeFullscreenBounds().contains(e.getPoint())) {
                	  tempNative=! tempNative;
                  }else if (getVSYNCBounds().contains(e.getPoint())) {
                	  tempVSYNC=! tempVSYNC;
                  }else if (getVisorBounds().contains(e.getPoint())) {
                	  tempVisor=! tempVisor;
                  }
                  else if (getAceptBounds().contains(e.getPoint())) {
                	  Main.properties.saveParamChanges(tempFullScreen,tempNative,tempVSYNC,tempVisor);
                	  System.exit(0);
                	  	
                  }
                 
                  else if (getBackBounds().contains(e.getPoint())) {
                	  Main.Menu.selecionarPantalla(Pantallas.MENUINICIO);
                  }
              }
			
		};
		
		 addMouseListener(mouse);
         addMouseMotionListener(mouse);
	}
	
	protected Point getImageOffset() {

        Point p = new Point();
            p.x = (getWidth() - Main.sc.getWidth()) / 2;
            p.y = (getHeight() - Main.sc.getHeight()) / 2;        

        return p;

    }
	
	/*
	 * Metodo que dibuja las componentes
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g){

		super.paintComponent(g);
	    Graphics2D g2d = (Graphics2D) g.create();			
	    drawText(g2d, "FullScreen: "+this.tempFullScreen, getFullScreenBounds());
	    drawText(g2d, "Native Screen: "+this.tempNative, getforceNativeFullscreenBounds());
		drawText(g2d, "VSYNC: "+this.tempVSYNC, getVSYNCBounds());
		drawText(g2d, "3D visor: "+this.tempVisor, getVisorBounds());
		drawText(g2d, "Acept ", getAceptBounds());
		drawText(g2d, "Back ", getBackBounds());
		g2d.dispose();
		setOpaque(false);		
	}
	
	 /*
	  * Metodo que dibuja un rectangulo y su texto, si el raton esta encima del boton este cambia de forma
	  */
	 protected void drawText(Graphics2D g2d, String text, Rectangle bounds) {

         FontMetrics fm = g2d.getFontMetrics();
         g2d.setColor(new Color(0,0,0,0));
         g2d.fill(bounds);
         g2d.setColor(Color.WHITE);
         if (selectedBounds != null) {
             if (bounds.contains(selectedBounds)) {
            	 g2d.setColor(new Color(200,200,200,0));
                 Rectangle fill = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
                 g2d.fill(fill);
                 g2d.setColor(Color.BLACK);
             }
         }         
        	g2d.setFont(fuente.deriveFont(Font.PLAIN, 28)); 
			g2d.drawString(
	                 text,
	                 bounds.x + ((bounds.width - fm.stringWidth(text)) / 2),
	                 bounds.y + ((bounds.height - fm.getHeight()) / 2) + fm.getAscent());
         

     }
	 	 
	 protected Rectangle getFullScreenBounds() {
         return getButtonBounds(FullScreen);
     }

     protected Rectangle getforceNativeFullscreenBounds() {
         return getButtonBounds(forceNativeFullscreen);
     }
     
     protected Rectangle getVSYNCBounds() {
         return getButtonBounds(VSYNC);
     }
     
     protected Rectangle getAceptBounds() {
         return getButtonBounds(Acept);
     }
     
     protected Rectangle getBackBounds() {
         return getButtonBounds(Back);
     }
     
     protected Rectangle getVisorBounds() {
         return getButtonBounds(Visor);
     }
     
     

     
     
     /*
      * Crea un rectangulo copia
      */
     protected Rectangle getButtonBounds(Rectangle masterBounds) {
         Rectangle bounds = new Rectangle(masterBounds);
         Point p = getImageOffset();
         bounds.translate(p.x, p.y);
         return bounds;
     }


	@Override
	public void CargarRecursos() {
		repaint();
		
	}


	@Override
	public void DescargarRecursos() {
	    selectedBounds=null;
		fuente=null; 
	    mouse=null;
		
	}

}
