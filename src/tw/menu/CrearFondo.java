package tw.menu;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class CrearFondo extends JPanel implements InterfazMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage imagen=null;
	
	
	
	
	public CrearFondo(String ruta){
		try {
			this.imagen=ImageIO.read(new File(ruta));
			repaint();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void CargarRecursos() {
		// TODO Auto-generated method stub
		repaint();
	}



	public boolean fondoCargado(){
		return imagen!=null;
	}
	@Override
	public void DescargarRecursos() {
		// TODO Auto-generated method stub
		imagen=null;
	}
	
	
	/*
	 * Metodo que dibuja las componentes
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g){
		int width = this.getSize().width;
		int height = this.getSize().height;
		super.paintComponent(g);
		if (this.imagen != null) {			
			Graphics2D g2d = (Graphics2D) g.create();			
			g2d.drawImage(this.imagen, 0, 0, width, height, null);			
			g2d.dispose();
			setOpaque(false);
		}
	}
		
	

}
