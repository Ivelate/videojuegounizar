package tw.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;

import tw.Main;
import tw.menu.GestorMenu.Pantallas;

public class MenuOpcionesJuego  extends JComponent{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Rectangle ONE_PLAYER = new Rectangle((Main.sc.getWidth()/2)-185,Main.sc.getHeight()-450, 262, 85);
	 private static Rectangle TWO_PLAYERS = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-400, 262, 85);
	 private static Rectangle THREE_PLAYERS = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-350, 262, 85);
	 private static Rectangle FOUR_PLAYERS = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-300, 262, 85);
	 private static Rectangle FIVE_PLAYERS = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-250, 262, 85);
	 private static Rectangle BACK = new Rectangle((Main.sc.getWidth()/2)-135,Main.sc.getHeight()-170, 262, 85);
	 private Rectangle selectedBounds;
	 private Font fuente; 
	 private MouseAdapter mouse;
	
	public MenuOpcionesJuego(){
	
		try {
			fuente=Font.createFont(Font.TRUETYPE_FONT, new File("rsc/assets/font/DeBorstel Brush-Reduced.ttf"));
		} catch (FontFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setPreferredSize(Main.sc.getSize());
		IniciarRaton();
		repaint();
		
	}
	
	
	
	
	private void IniciarRaton(){
		mouse= new MouseAdapter() {
			/*
			 * Metdodo que sirve para que cuando el raton pase por un boton este cambie de color
			 * @see java.awt.event.MouseAdapter#mouseMoved(java.awt.event.MouseEvent)
			 */
			public void mouseMoved(MouseEvent e){
				 if (getOnePlayerBounds().contains(e.getPoint())) {
                     selectedBounds = getOnePlayerBounds();
				 }
				 else if(getTwoPlayersBounds().contains(e.getPoint())){
					 selectedBounds=getTwoPlayersBounds();
				 }else if(getBackBounds().contains(e.getPoint())){
					selectedBounds=getBackBounds();
				}else if(getThreePlayersBounds().contains(e.getPoint())){
					selectedBounds=getThreePlayersBounds();
				}else if(getFourPlayersBounds().contains(e.getPoint())){
					selectedBounds=getFourPlayersBounds();
				}else if(getFivePlayersBounds().contains(e.getPoint())){
					selectedBounds=getFivePlayersBounds();
				}else {
                     selectedBounds = null;
                 }
                 repaint();
			}
				/*
				 * Evento de pulsacion de boton
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
			  @Override
              public void mouseClicked(MouseEvent e) {
                  if (getOnePlayerBounds().contains(e.getPoint())) {
                	  Main.Menu.selecionarPantalla(Pantallas.INTROUNJUGADOR);
                  }
                  else  if (getTwoPlayersBounds().contains(e.getPoint())) {
                	  Main.Menu.selecionarPantalla(Pantallas.INTRODOSJUGADORES);
                  }
                  else  if (getThreePlayersBounds().contains(e.getPoint())) {
                	  Main.Jugadores=3;
                	  Main.Menu.selecionarPantalla(Pantallas.INTRODOSJUGADORES);
                  }
                  else if(getFourPlayersBounds().contains(e.getPoint())){
                	  Main.Jugadores=4;
                	  Main.Menu.selecionarPantalla(Pantallas.INTRODOSJUGADORES);
                  }
                  else if(getFivePlayersBounds().contains(e.getPoint())){
                	  Main.Jugadores=5;
                	  Main.Menu.selecionarPantalla(Pantallas.INTRODOSJUGADORES);
                  }
                  else if(getBackBounds().contains(e.getPoint())){
                	  Main.Menu.selecionarPantalla(Pantallas.MENUINICIO);
                  }
                 
              }
			
		};
		
		 addMouseListener(mouse);
         addMouseMotionListener(mouse);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
	    Graphics2D g2d = (Graphics2D) g.create();			
	    drawText(g2d, "One Player", getOnePlayerBounds());
	    drawText(g2d, "Two Players", getTwoPlayersBounds());
	    drawText(g2d, "Three Players", getThreePlayersBounds());
	    drawText(g2d, "Player marine VS Player spider", getFourPlayersBounds());
	    drawText(g2d, "Massive Battle", getFivePlayersBounds());
	    drawText(g2d, "Back",getBackBounds());
		g2d.dispose();
		setOpaque(false);		
	}
	
	protected void drawText(Graphics2D g2d, String text, Rectangle bounds) {

        FontMetrics fm = g2d.getFontMetrics();
        g2d.setColor(new Color(0,0,0,0));
        g2d.fill(bounds);
        g2d.setColor(Color.WHITE);
        if (selectedBounds != null) {
            if (bounds.contains(selectedBounds)) {
           	 g2d.setColor(new Color(200,200,200,0));
                Rectangle fill = new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
                g2d.fill(fill);
                g2d.setColor(Color.BLACK);
            }
        }         
       	g2d.setFont(fuente.deriveFont(Font.PLAIN, 28)); 
			g2d.drawString(
	                 text,
	                 bounds.x + ((bounds.width - fm.stringWidth(text)) / 2),
	                 bounds.y + ((bounds.height - fm.getHeight()) / 2) + fm.getAscent());
        

    }
	
	
	
	protected Point getImageOffset() {

        Point p = new Point();
            p.x = (getWidth() - Main.sc.getWidth()) / 2;
            p.y = (getHeight() - Main.sc.getHeight()) / 2;        

        return p;

    }
	
	 protected Rectangle getButtonBounds(Rectangle masterBounds) {
         Rectangle bounds = new Rectangle(masterBounds);
         Point p = getImageOffset();
         bounds.translate(p.x, p.y);
         return bounds;
     }
	 
	 protected Rectangle getOnePlayerBounds() {
	        return getButtonBounds(ONE_PLAYER);
	 }
	 
	 protected Rectangle getTwoPlayersBounds(){
		 return getButtonBounds(TWO_PLAYERS);
	 }
	 protected Rectangle getThreePlayersBounds(){
		 return getButtonBounds(THREE_PLAYERS);
	 }
	 protected Rectangle getFourPlayersBounds(){
		 return getButtonBounds(FOUR_PLAYERS);
	 }
	 protected Rectangle getFivePlayersBounds(){
		 return getButtonBounds(FIVE_PLAYERS);
	 }
	 protected Rectangle getBackBounds(){
		 return getButtonBounds(BACK);
	 }
	 

}
