package tw.game;

import java.awt.Graphics2D;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Character.Direction;
import tw.entities.EntitySpawner;
import tw.entities.EntityType.Faction;
import tw.entities.IAControlledCharacter;
import tw.entities.ShooterCharacter;
import tw.entities.TestPlayableCharacter;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.utils.Drawable2D;
import tw.utils.InputHandler;
import tw.utils.PlayerInputListener;

public class Player implements Drawable2D, PlayerInputListener
{
	private int playerCode;
	private AnimationMetrics2D playerIconMetric;
	
	private ShooterCharacter playerCharacter;
	private EntitySpawner.CHARACTER_TYPE characterType;
	private EntitySpawner.CHARACTER_TYPE pilotCharacterType;
	private InputHandler ih;
	private boolean alive=true;
	private float respawnCounter=0;
	private Faction respawnFaction=null;
	private int savedSecondaryAmmo=0;
	
	private boolean shootRequested=false;
	private boolean orderRequested=false;
	
	public Player(int playerCode,ShooterCharacter playerCharacter,EntitySpawner.CHARACTER_TYPE characterType,EntitySpawner.CHARACTER_TYPE pilotCharacterType,InputHandler ih)
	{
		this.playerCode=playerCode;
		switch(this.playerCode)
		{
		case 0:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_ONE;
			break;
		case 1:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_TWO;
			break;
		case 2:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_THREE;
			break;
		default:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_FOUR;
			break;
		}
		this.playerCharacter=playerCharacter;
		this.characterType=characterType;
		this.pilotCharacterType=pilotCharacterType;
		this.ih=ih;
		
		if(this.playerCharacter==null) this.alive=false;
		else this.playerCharacter.setIA(false);
		
		this.ih.registerPlayerToInput(this, playerCode);
	}
	public Player(int playerCode,ShooterCharacter playerCharacter,EntitySpawner.CHARACTER_TYPE characterType,InputHandler ih)
	{
		this(playerCode,playerCharacter,characterType,null,ih);
	}
	public Player(int playerCode,EntitySpawner.CHARACTER_TYPE characterType,EntitySpawner.CHARACTER_TYPE pilotCharacterType,InputHandler ih)
	{
		this.playerCode=playerCode;
		switch(this.playerCode)
		{
		case 0:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_ONE;
			break;
		case 1:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_TWO;
			break;
		case 2:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_THREE;
			break;
		default:
			this.playerIconMetric=AnimationMetrics2D.PLAYER_FOUR;
			break;
		}
		this.characterType=characterType;
		this.pilotCharacterType=pilotCharacterType;
		this.ih=ih;
		this.alive=false;
		
		this.ih.registerPlayerToInput(this, playerCode);
	}
	public Player(int playerCode,EntitySpawner.CHARACTER_TYPE characterType,InputHandler ih)
	{
		this(playerCode,characterType,null,ih);
	}
	public ShooterCharacter getCharacter()
	{
		return this.playerCharacter;
	}
	public int getAvaillableSecondaryAmmo()
	{
		if(this.alive){
			if(this.playerCharacter!=null && this.playerCharacter.hasSecondaryWeapon()) return this.playerCharacter.getSecondaryWeapon().getCurrentAmmo();
		}
		return this.savedSecondaryAmmo;
	}
	public synchronized void setCharacter(ShooterCharacter c)
	{
		if(this.playerCharacter!=null){
			this.playerCharacter.clean();
		}
		
		this.alive=true;
		this.playerCharacter=c;
		this.playerCharacter.setIA(false);
		if(this.playerCharacter.hasSecondaryWeapon()&&this.savedSecondaryAmmo>=0) this.playerCharacter.getSecondaryWeapon().setAmmo(savedSecondaryAmmo);
	}
	public synchronized void update(float tEl,Level level)
	{
		if(this.alive)
		{
			if(this.playerCharacter.getLife()<=0) {
				ShooterCharacter c=(ShooterCharacter)this.playerCharacter.onDeath(level);
				this.playerCharacter.clean();
				if(c==null){
					if(this.playerCharacter.getSecondaryWeapon()!=null) this.savedSecondaryAmmo=this.playerCharacter.getSecondaryWeapon().getCurrentAmmo();
					this.respawnCounter=0;
					this.respawnFaction=this.playerCharacter.getFaction();
					this.playerCharacter=null;
					this.alive=false;
				}
				else {
					this.playerCharacter=c;
					this.playerCharacter.setIA(false);
					c.registerEntityToMap();
				}
			}
			else
			{
				Direction movementDir=getDirectionForKeys(this.ih,this.playerCode);
				this.playerCharacter.setMovementObj(movementDir);
				
				if(this.shootRequested){
					this.playerCharacter.setShootingTo(true);
				}
				else this.playerCharacter.setShootingTo(ih.getShootFor(this.playerCode));
				
				if(this.orderRequested&&(this.shootRequested||ih.getShootFor(playerCode))){
					this.playerCharacter.setShootingSecondaryTo(true);
				}
				else this.playerCharacter.setShootingSecondaryTo(false);
				
				this.shootRequested=false;
				this.orderRequested=false;
				
				this.playerCharacter.update(tEl, level);
				
			}
		}
		else{
			this.respawnCounter+=tEl;
			if(level.canRespawn(this.respawnFaction, this.respawnCounter))
			{
				AudioManager.getInstance().playSound(Sound.SPAWN);
				this.alive=true;
				level.respawnPlayer(this,this.respawnFaction);
			}
		}
	}
	
	private static Direction getDirectionForKeys(InputHandler ih,int playerCode)
	{
		int y=0;
		int x=0;
		
		if(ih.getDownFor(playerCode)) y++;
		if(ih.getUpFor(playerCode)) y--;
		if(ih.getLeftFor(playerCode)) x--;
		if(ih.getRightFor(playerCode)) x++;
		
		if(x==1)
		{
			if(y==1) return Direction.DOWNRIGHT;
			else if(y==0) return Direction.RIGHT;
			else if(y==-1) return Direction.UPRIGHT;
		}
		else if(x==0){
			if(y==1) return Direction.DOWN;
			else if(y==0) return null;
			else if(y==-1) return Direction.UP;
		}
		else if(x==-1){
			if(y==1) return Direction.DOWNLEFT;
			else if(y==0) return Direction.LEFT;
			else if(y==-1) return Direction.UPLEFT;
		}
		
		return null;
	}
	public EntitySpawner.CHARACTER_TYPE getCharacterType()
	{
		return this.characterType;
	}
	public EntitySpawner.CHARACTER_TYPE getPilotCharacterType()
	{
		return this.pilotCharacterType;
	}
	@Override
	public synchronized void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		
		if(this.playerCharacter!=null) {
			dm.drawAnimation((int)((this.playerCharacter.getInternalCounter()/this.playerIconMetric.ANIMMOD)%this.playerIconMetric.FRAMESNUM),
					this.playerCharacter.getX()+(this.playerCharacter.getSize()-((float)(this.playerIconMetric.SIZEX)/DrawingManager2D.TILE_SIZE))/2, this.playerCharacter.getY()-((float)(this.playerIconMetric.SIZEX)/DrawingManager2D.TILE_SIZE), ((float)(this.playerIconMetric.SIZEX)/DrawingManager2D.TILE_SIZE), this.playerIconMetric, g);
		}
		
	}
	@Override
	public void onPlayerInputPressed(KEY_TYPE kt) {
		if(kt==KEY_TYPE.SHOOT) this.shootRequested=true;
		if(kt==KEY_TYPE.ORDER) this.orderRequested=true;
	}
}