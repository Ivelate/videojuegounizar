package tw.game;

import java.awt.Graphics;
import java.awt.Graphics2D;

import tw.Main;
import tw.utils.LevelInstance;

public class UpdateThread extends Thread
{
	private LevelInstance level;
	private int MAX_FPS;
	private long timeCounter=System.currentTimeMillis();
	private int loopCounter=0;
	private int loopsSkipped=0;
	private int currentFPS;
	private long timeElapsed=System.currentTimeMillis();
	
	public UpdateThread(LevelInstance l,int fpsmax)
	{
		this.level=l;
		this.MAX_FPS=fpsmax;
		this.currentFPS=MAX_FPS;
	}
	public void run()
	{
		try
		{
			while(!this.level.gameEnded())
			{
				this.loopCounter++;
				long initTime=System.currentTimeMillis();
				if(initTime>this.timeCounter+1000)
				{
					this.currentFPS=(int) ((1000*(this.loopCounter-this.loopsSkipped))/(initTime-timeCounter));
					System.out.println("UPDATE FPS: "+this.currentFPS+" skipped "+this.loopsSkipped);
					this.loopCounter=0;
					this.loopsSkipped=0;
					this.timeCounter=initTime;
				}
			
				long currentTime=System.currentTimeMillis();
				if(currentTime-this.timeElapsed!=0){
					this.level.update((float)(currentTime-this.timeElapsed)/1000);
				}
				else loopsSkipped++;
				this.timeElapsed=currentTime;
			
				//Setting FPS
				try 
				{
					//long waitTime=(1000/this.MAX_FPS)-(System.currentTimeMillis()-initTime)-1;
					long waitTime=(this.timeCounter+(int)(this.loopCounter*1000/this.MAX_FPS))-System.currentTimeMillis();
					if(waitTime>0) Thread.sleep(waitTime);
				} catch (InterruptedException e) {
					//Interrupted, repainting
				}
			}
		}
		catch(Exception e)
		{
			synchronized(this.level)
			{
				this.level.setGameEnded();
				this.level.notifyAll();
			}
			System.err.println("An error has ocurred. Game will try to continue.");
			System.err.println("A error dump has been created in "+Main.errorDumpFile.getAbsolutePath());
			System.err.println("UPDATE THREAD ERROR");
			e.printStackTrace(System.err);
		}
	}
}
