package tw.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import tw.Main;
import tw.IA.Util.Astar.Astar;
import tw.audio.AudioManager;
import tw.audio.AudioManager.Music;
import tw.entities.*;
import tw.entities.Character;
import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntitySpawner.POWERUP_TYPE;
import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.gui.DrawingManager2D;
import tw.gui.GameGUI;
import tw.gui.GamePanel;
import tw.gui.animation.Animation;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.BlackCourtainAnimation;
import tw.gui.animation.DefaultAnimation;
import tw.gui.animation.FadeAnimation;
import tw.menu.MenuJuego;
import tw.tiles.Tile;
import tw.tiles.Tile.TileState;
import tw.utils.EntityFunctionProvider;
import tw.utils.SizedDrawable2D;
import tw.utils.LevelInstance;


/**
 * Game level, manages all game entities in a fixed map and its actions (Moving, AI, shooting).
 * This class manages all the game actions and events in a particular level, characterized by the map provided.
 */
public class Level implements SizedDrawable2D,LevelInstance
{
	private Map map;
	private GamePanel gp;
	private int testBulletNum=0;
	private BlackCourtainAnimation curtainEndgameAnimation;
	private GameGUI gameGUI;
	private Player[] players;
	public boolean canDraw=true;
	private boolean drawLock=false;
	private Boolean gameEnded=false;
	private boolean endgameScreenDisplayed=false;
	private static final float watingTimeTillEnd=0.5f;
	private float endingCont=watingTimeTillEnd;
	private boolean marinesWin=false;
	private final MenuJuego menu=new MenuJuego();
	//private UpdateThread updateThread; //Thread draw
	
	/*
	 * Astar
	 */
	private Astar astar; 
	
	
	private int[] teamMembers=new int[Faction.values().length];
	
	private List<Bullet> bulletArray=new LinkedList<Bullet>();
	private List<IAControlledCharacter> characterArray=new LinkedList<IAControlledCharacter>();
	private List<Animation> animationArray=new LinkedList<Animation>();
	private List<Animation> upperAnimationArray=new LinkedList<Animation>();
	
	public  Queue<IAControlledCharacter> auxCharacterArray=new LinkedList<IAControlledCharacter>();
	private Queue<Animation> auxAnimationArray=new LinkedList<Animation>();
	private Queue<Animation> auxUpperAnimationArray=new LinkedList<Animation>();
	
	private List<Consumable> consumableArray=new LinkedList<Consumable>();
	
	private float lastmx=0;
	private float lastmy=0;
	private boolean mouseBulletsActivated=false;
	
	public Level(Map m,final GamePanel gp)
	{
		AudioManager.getInstance().playMusic(Music.MUSIC_GAME);
		gp.getDrawingManager().preComputeMap(m.getTiles());
		this.map=m;
		this.gp=gp;
		
		this.astar=map.GetAstarAlgorithm();

		System.out.println("***********"+Main.Jugadores);
		
		int numj=0;
		if(Main.Jugadores<4)
		{
			numj=Main.Jugadores;
			this.players=new Player[Main.Jugadores];
			if(Main.Jugadores>=1) this.players[0]=new Player(0,EntitySpawner.CHARACTER_TYPE.TESTPLAYABLE,this.gp.getInputHandler());
			if(Main.Jugadores>=2)this.players[1]=new Player(1,EntitySpawner.CHARACTER_TYPE.TESTPLAYABLE,this.gp.getInputHandler());
			if(Main.Jugadores>=3)this.players[2]=new Player(2,EntitySpawner.CHARACTER_TYPE.TESTPLAYABLE,this.gp.getInputHandler());
		}
		else{
			numj=0;
			this.players=new Player[2];
			this.players[0]=new Player(0,EntitySpawner.CHARACTER_TYPE.TESTPLAYABLE,this.gp.getInputHandler());
			this.players[1]=new Player(1,EntitySpawner.CHARACTER_TYPE.SPIDER,this.gp.getInputHandler());
		}
		
		this.gameGUI=new GameGUI(this.players,this);

		if(this.players.length>1)this.respawnPlayer(this.players[1],Faction.ALIEN);
		this.respawnPlayer(this.players[0],Faction.MARINE);
		gp.addMouseListener(new MouseListener(){ //Clicking in a tile will display its info in the screen
			@Override 
			public void mouseClicked(MouseEvent e) 
			{  
				if(!mouseBulletsActivated)gp.getDrawingManager().mousePowerflowColor=new Color((float)Math.random(),(float)Math.random(),(float)Math.random());
				
				float nmx=e.getX()/30;
				float nmy=e.getY()/30;
				Tile t=map.getTileAt(e.getX()/30, e.getY()/30);
				System.out.println("Current: "+t.name);
				System.out.println("For "+EntityType.Role.GROUND+" -> "+t.getLimitsFor(EntityType.Role.GROUND,EntityType.Faction.MARINE));
				System.out.println("For "+EntityType.Role.AIR+" -> "+t.getLimitsFor(EntityType.Role.AIR,EntityType.Faction.MARINE));
				System.out.println("For "+EntityType.Role.BULLET+" -> "+t.getLimitsFor(EntityType.Role.BULLET,EntityType.Faction.MARINE));
				System.out.println("_______________________________________________________");
			
				mouseBulletsActivated=!mouseBulletsActivated;
				//addUpperAnimation(new Grenade(nmx,nmy,0));
				//spawnExplosion(AnimationMetrics2D.GRENADE,nmx, nmy,3,10,Faction.OTHER);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}
		});
		
		Random n=new Random();
		int xdestCharacter=n.nextInt(4);
		int xdestConsumables=n.nextInt(34);
		int ydest=n.nextInt(25);
		
		int flagBatallaMasiva=Main.Jugadores==5?9:1;
		
		//Spawn some enemies
		for(int i=0;i<20*(flagBatallaMasiva);i++)
		{
			if(this.astar.existNode(29+xdestCharacter, ydest)) this.spawnCharacter(CHARACTER_TYPE.SPIDER,(float)29+xdestCharacter,(float)ydest);
			xdestCharacter=n.nextInt(4);
			ydest=n.nextInt(25);
			
		}
		//Spawn some BIG enemies
		for(int i=0;i<2;i++)
		{
			if(this.astar.existNode(29+xdestCharacter, ydest))
				this.spawnCharacter(CHARACTER_TYPE.BIGSPIDER,(float)29+xdestCharacter,(float)ydest);
			xdestCharacter=n.nextInt(4);
			ydest=n.nextInt(25);
		}
		//Spawn some allies
		for(int i=0;i<35*flagBatallaMasiva-(numj*5);i++)
		{
			if(this.astar.existNode(xdestCharacter, ydest)) this.spawnCharacter(CHARACTER_TYPE.SOLDIER,(float)xdestCharacter,(float)ydest);
			xdestCharacter=n.nextInt(4);
			ydest=n.nextInt(25);
		}
		//Spawn some tanks
		for(int i=0;i<4-(Math.ceil(((float)(numj)/2))-0.001f);i++)
		{
			if(this.astar.existNode(xdestCharacter, ydest)) this.spawnCharacter
			(EntitySpawner.spawnCharacter(CHARACTER_TYPE.SOLDIER, this.map,xdestCharacter,ydest),
					CHARACTER_TYPE.TANK,xdestCharacter,ydest);
			xdestCharacter=n.nextInt(4);
			ydest=n.nextInt(25);
		}
		//Spawn some spider nests
		for(int i=0;i<6;i++)
		{
			float xnest=(float)Math.random()*this.map.getWidth();
			if(this.astar.existNode((int)xnest, ydest))
				this.spawnCharacter(CHARACTER_TYPE.SPIDER_NEST,(float)xnest,(float)ydest);
			xdestConsumables=n.nextInt(4);
			ydest=n.nextInt(25);
		}
		
		//Spawn some random consumables
		for(int i=0;i<2;i++)
		{
			if(this.astar.existNode(xdestConsumables, ydest))	this.spawnPowerUp(POWERUP_TYPE.WEAPON_NORMAL, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.WEAPON_PLASMA, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.WEAPON_FIRE, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.AMMO_GRENADE, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.SPIDERPLANT, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_BRONZE, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_SILVER, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_GOLD, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
			if(this.astar.existNode(xdestConsumables, ydest)) this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_SUPER, (float)xdestConsumables,(float)ydest);
			xdestConsumables=n.nextInt(34);
			ydest=n.nextInt(25);
		}
		
		final Level thisLevel=this;
		//new BlackCourtainAnimation((float)3,(float)getWidth(),(float)getHeight(),true
		this.curtainEndgameAnimation=new BlackCourtainAnimation(3, getWidth(), getHeight(), true,this){
			@Override
			public void onDisposeAfterDraw()
			{
				drawLock=true;
				UpdateThread updateThread=new UpdateThread(thisLevel,GamePanel.DEFAULT_FPS);
				updateThread.start();
			}
		};
		this.addAuxEntitiesToMainArrays();
		gp.registerInDrawLoop(this);
	}

	public void update(float tEl)
	{		
		if(tEl>0.05) tEl=0.05f; 
		
		// MOVEMENT TEST (Collision registering test with mouse)
		Point mpoint = MouseInfo.getPointerInfo().getLocation();
		//SwingUtilities.convertPointFromScreen(mpoint, gp);

		float nmx = (float) (mpoint.x) / 32;
		float nmy = (float) (mpoint.y) / 32;
		//float nmx = this.characterArray.get(0).getX();
		//float nmy = this.characterArray.get(0).getY();

		/*if(this.mouseBulletsActivated)
		{
			for (int i = 0; i < 20; i++) {
				double loc = Math.random();
				double tip=Math.random();
				BULLET_TYPE bulletType=tip>0.7f?BULLET_TYPE.SPIDER_SPLIT:tip>0.6f?BULLET_TYPE.FIRE:tip>0.3f?BULLET_TYPE.PLASMA:BULLET_TYPE.MACHINEGUN;
				//BULLET_TYPE bulletType=BULLET_TYPE.PLASMA;
				this.spawnBullet(Faction.OTHER, (float) (this.lastmx + (nmx - this.lastmx) * loc),
						(float) (this.lastmy + (nmy - this.lastmy) * loc),
						(float) (Math.random() * 20 - 10),
						(float) (Math.random() * 20 - 10),
						bulletType);
			}
			//spawnExplosion(AnimationMetrics2D.BIGEXPLOSION,nmx, nmy,3,10,Faction.OTHER);

		}*/

		this.lastmx = nmx;
		this.lastmy = nmy;

		
		for(int i=0;i<this.players.length;i++)
		{
			this.players[i].update(tEl,this);
		}
		
		synchronized(this.characterArray)
		{
			Iterator<IAControlledCharacter> ci = this.characterArray.iterator();
			while(ci.hasNext())
			{
				IAControlledCharacter c=ci.next();
				if(c.getLife()<=0){
					this.teamMembers[c.getFaction().ordinal()]--;
					IAControlledCharacter pilot=c.onDeath(this);
					if(pilot!=null) this.spawnCharacter(pilot);
					c.clean();
					ci.remove();
				}
				else c.update(tEl,this);
			}
		}
		
		synchronized(this.bulletArray)
		{
			Iterator<Bullet> bi = this.bulletArray.iterator();
			while(bi.hasNext()){
				Bullet b=bi.next();
				if(b.move(tEl)){
					b.clean();
					bi.remove();
				}
			}
		}
		
		synchronized(this.consumableArray)
		{
			Iterator<Consumable> coni = this.consumableArray.iterator();
			while(coni.hasNext()){
				Consumable con=coni.next();
				if(con.update(tEl)){
					con.clean();
					coni.remove();
				}
			}	
		}
		
		synchronized(this.animationArray)
		{
			Iterator<Animation> animi = this.animationArray.iterator();
			while(animi.hasNext()){
				Animation anim=animi.next();
				if(anim.update(tEl,this)){
					anim.onDispose(this);
					animi.remove();
				}
			}
		}
		
		synchronized(this.upperAnimationArray)
		{
			Iterator<Animation> uanimi = this.upperAnimationArray.iterator();
			while(uanimi.hasNext()){
				Animation anim=uanimi.next();
				if(anim.update(tEl,this)){
					anim.onDispose(this);
					uanimi.remove();
				}
			}
		}
		
		this.gameGUI.update(tEl);
		
		//SPAWN CONSUMABLES AT RANDOM YEAH
		if(Math.random()<0.25f*tEl)
		{
			double rand=Math.random();
			boolean locDecided=false;
			float locx=0;
			float locy=0;
			do
			{
				locx=(float)(Math.random()*this.getMap().getWidth());
				locy=(float)(Math.random()*this.getMap().getHeight());
				if(this.getMap().getTileAt(locx, locy).getLimitsFor(Role.GROUND, Faction.MARINE)==TileState.EMPTY) locDecided=true;
			}while(!locDecided);
			if(rand>0.8){
				this.spawnPowerUp(POWERUP_TYPE.SPIDERPLANT, locx, locy);
			}
			else if(rand>0.68){
				this.spawnPowerUp(POWERUP_TYPE.WEAPON_FIRE, locx, locy);
			}
			else if(rand>0.56){
				this.spawnPowerUp(POWERUP_TYPE.WEAPON_PLASMA, locx, locy);
			}
			else if(rand>0.44){
				this.spawnPowerUp(POWERUP_TYPE.WEAPON_NORMAL, locx, locy);
			}
			else if(rand>0.31){
				this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_BRONZE, locx, locy);
			}
			else if(rand>0.19){
				this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_SILVER, locx, locy);
			}
			else if(rand>0.05){
				this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_GOLD, locx, locy);
			}
			else {
				this.spawnPowerUp(POWERUP_TYPE.REINFORCEMENT_SUPER, locx, locy);
			}
		}
		
		
		addAuxEntitiesToMainArrays();
		
		int zeros=0;
		if(this.teamMembers[Faction.MARINE.ordinal()]+getAlivePlayersForFaction(Faction.MARINE)==0)zeros++;
		if(this.teamMembers[Faction.ALIEN.ordinal()]+getAlivePlayersForFaction(Faction.ALIEN)==0)zeros++;
		if(this.teamMembers[Faction.REBEL.ordinal()]+getAlivePlayersForFaction(Faction.REBEL)==0)zeros++;
		if(zeros>=2&&!this.endgameScreenDisplayed){
			if(this.teamMembers[Faction.MARINE.ordinal()]+getAlivePlayersForFaction(Faction.MARINE)!=0) this.marinesWin=true;
			if(this.endingCont>0) endingCont-=tEl;
			else
			{
				AudioManager.getInstance().playMusic(null);
				AnimationMetrics2D metrics=this.teamMembers[Faction.MARINE.ordinal()]+getAlivePlayersForFaction(Faction.MARINE)==0?AnimationMetrics2D.TEXT_DEFEAT:AnimationMetrics2D.TEXT_VICTORY;
				float xs=(this.map.getWidth()-(metrics.SIZEX/DrawingManager2D.TILE_SIZE))/2;
				float ys=(this.map.getHeight()-(metrics.SIZEY/DrawingManager2D.TILE_SIZE))/2;
				final Level level = this;
				this.addUpperAnimation(new FadeAnimation(2f,xs,ys,metrics)
				{
					private boolean courtainSpawned=false;
					@Override
					public void onDispose(Level l) {
						l.setGameEnded();
					}
					@Override
					protected void concreteUpdate(float totalTimeElapsed, Level l) {
						super.concreteUpdate(totalTimeElapsed, l);
						if(totalTimeElapsed>5&&!courtainSpawned){
							curtainEndgameAnimation=new BlackCourtainAnimation(3,5,getWidth(),getHeight(),false,level);
							this.courtainSpawned=true;
						}	
					}
				}
				);
				this.endgameScreenDisplayed=true;
			}
		}
		else this.endingCont=watingTimeTillEnd;
		//Updated. Notify draw thread
		synchronized(this)
		{
			this.canDraw=true;
			notifyAll();
		}
	}
	private int getAlivePlayersForFaction(Faction f)
	{
		int cont=0;
		for(Player p:this.players)
		{
			if(p.getCharacter()!=null && p.getCharacter().getFaction()==f) cont++;
		}
		return cont;
	}
	private void addAuxEntitiesToMainArrays()
	{
		synchronized(this.characterArray)
		{
			IAControlledCharacter aux;
			while((aux=this.auxCharacterArray.poll())!=null) {
				aux.registerEntityToMap();
				this.characterArray.add(aux);
			}
		}
		
		Animation auxAnim;
		synchronized(this.animationArray)
		{
			while((auxAnim=this.auxAnimationArray.poll())!=null) {
				this.animationArray.add(auxAnim);
			}
		}
		synchronized(this.upperAnimationArray)
		{
			while((auxAnim=this.auxUpperAnimationArray.poll())!=null) {
				this.upperAnimationArray.add(auxAnim);
			}
		}
	}
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g,float tEl) 
	{
		if(this.drawLock)
		{
			synchronized(this)
			{
				if(!this.canDraw){
					if(this.gameEnded()) return;
					try {
						this.wait();
						if(this.gameEnded()) return;
					} catch (InterruptedException e) {}
				}
				this.canDraw=false;
			}
		}
		//System.out.println(tEl);
		//If tEl is too high, some calcs will start to bug. We have to cap it at a maximum.
		//Capped at 20FPS
		if(tEl>0.05) tEl=0.05f; 
		
		//update(tEl);
		
		dm.drawPreComputedMap(g); //DIBUJAR MAPA PRECOMPUTADO
		
		synchronized(this.animationArray)
		{
			for(Animation a:this.animationArray)
			{
				a.draw2D(dm, g, tEl);
			}
		}

		this.map.drawAllRegisteredEntities2D(dm, g, tEl);
		
		synchronized(this.bulletArray)
		{
			for(Bullet b:this.bulletArray)
			{
				b.draw2D(dm, g, tEl);
			}
		}
		
		for(int i=0;i<this.players.length;i++)
		{
			this.players[i].draw2D(dm, g, tEl);
		}
		
		synchronized(this.upperAnimationArray)
		{
			for(Animation a:this.upperAnimationArray)
			{
				a.draw2D(dm, g, tEl);
			}
		}
		
		this.gameGUI.draw2D(dm, g, tEl);
		if(this.curtainEndgameAnimation!=null){
			this.curtainEndgameAnimation.draw2D(dm, g, tEl);
			if(this.curtainEndgameAnimation.hasFinished()){
				this.curtainEndgameAnimation=null;
			}
		}
	}
	
	public void applyFunctionToAllEntities(EntityFunctionProvider efp)
	{
		synchronized(this.characterArray)
		{
			for(Character c:this.characterArray)
			{
				efp.applyFunctionToEntity(c);
			}
		}
		
		synchronized(this.bulletArray)
		{
			for(Bullet b:this.bulletArray)
			{
				efp.applyFunctionToBullet(b);
			}
		}
		
		/*synchronized(this.consumableArray)
		{
			for(Consumable con:this.consumableArray)
			{
				efp.applyFunctionToEntity(con);
			}
		}*/
		for(int i=0;i<this.players.length;i++)
		{
			if(this.players[i].getCharacter()!=null) efp.applyFunctionToEntity(this.players[i].getCharacter());
		}
	}
	
	public boolean canRespawn(Faction f,float timeWaited)
	{
		if(f==null) return true;
		
		if(this.teamMembers[f.ordinal()]==0) return false;
		else if(this.teamMembers[f.ordinal()]<=5){
			return timeWaited >= 15-this.teamMembers[f.ordinal()];
		}
		else if(this.teamMembers[f.ordinal()]<=20){
			return timeWaited >= 10-(this.teamMembers[f.ordinal()]-5)/3;
		}
		else return timeWaited>=5f;
	}
	public void respawnPlayer(Player p,Faction f)
	{
		//p.setCharacter(new TestPlayableCharacter(this.map,8,8));
		//p.setCharacter(new TestPlayableCharacter(this.map,8,8));
		int ypos=8;
		int xpos=f!=Faction.MARINE?30:8;
		ShooterCharacter pilot=null;
		if(p.getPilotCharacterType()!=null){
			pilot=(ShooterCharacter) EntitySpawner.spawnCharacter(p.getPilotCharacterType(), map, xpos, ypos,Float.MAX_VALUE,false);
		}
		ShooterCharacter player=(ShooterCharacter) EntitySpawner.spawnCharacter(pilot,p.getCharacterType(), map, xpos, ypos,Float.MAX_VALUE,false);
		this.spawnExplosion(AnimationMetrics2D.BIGEXPLOSION,false, xpos+player.getSize()/2, ypos+player.getSize()/2, 1.5f, 100, player.getFaction());
		player.registerEntityToMap();
		
		p.setCharacter(player);
	}
	//|TODO test method
	public void spawnBullet(Faction f,float ix,float iy,float vx,float vy,BULLET_TYPE bulletType)
	{
		synchronized(this.bulletArray) 
		{
			this.bulletArray.add(EntitySpawner.spawnBullet(bulletType, this.map, ix, iy, vx, vy, f, this));
		}
	}
	//|TODO test method
	public boolean spawnCharacter(IAControlledCharacter pilot,EntitySpawner.CHARACTER_TYPE type, float locx,float locy)
	{
		IAControlledCharacter character=EntitySpawner.spawnCharacter(pilot,type, this.map,this.astar,this.characterArray, locx, locy);
		if(character!=null) {
			this.teamMembers[character.getFaction().ordinal()]++;
			this.auxCharacterArray.add(character);
			return true;
		}
		return false;
	}
	public boolean spawnCharacter(EntitySpawner.CHARACTER_TYPE type, float locx,float locy){
		if(this.astar.existNode((int)locx, (int)locy))
		return this.spawnCharacter(null,type, locx, locy);
		else return false; 
	}
	public boolean spawnCharacter(IAControlledCharacter c)
	{
		if(c!=null){
			this.teamMembers[c.getFaction().ordinal()]++;
			this.auxCharacterArray.add(c);
			return true;
		}
		return false;
	}
	public void spawnPowerUp(EntitySpawner.POWERUP_TYPE type,float locx,float locy)
	{
		Consumable e=EntitySpawner.spawnPowerup(type,this.map,this.astar,this.characterArray,locx,locy);
		e.registerEntityToMap();
		synchronized(this.consumableArray) {this.consumableArray.add(e);}
	}
	public void spawnExplosion(AnimationMetrics2D metrics,float ix,float iy,float radius,int damage,Faction f)
	{
		this.spawnExplosion(metrics, true, ix, iy, radius, damage, f);
	}
	public void spawnExplosion(AnimationMetrics2D metrics,AnimationMetrics2D impactAnimationMetrics,float ix,float iy,float radius,int damage,Faction f)
	{
		this.spawnExplosion(metrics,impactAnimationMetrics, true, ix, iy, radius, damage, f);
	}
	public void spawnExplosion(AnimationMetrics2D metrics,boolean upperAnimation,float ix,float iy,float radius,int damage,Faction f){
		this.spawnExplosion(metrics, null, upperAnimation, ix, iy, radius, damage, f);
	}
	public void spawnExplosion(AnimationMetrics2D metrics,AnimationMetrics2D impactAnimationMetrics,boolean upperAnimation,float ix,float iy,float radius,int damage,Faction f)
	{
		if(metrics!=null) {
			if(upperAnimation) this.addUpperAnimation(new DefaultAnimation(ix-radius,iy-radius,radius*2,metrics));
			else this.addAnimation(new DefaultAnimation(ix-radius,iy-radius,radius*2,metrics));
		}
		for(int w=(int)(ix-radius);w<=(int)(ix+radius);w++)
		{
			for(int h=(int)(iy-radius);h<=(int)(iy+radius);h++)
			{
				for(Entity e:this.map.getEntitiesAt(w, h))
				{
					if(e instanceof Character){
						Character c=(Character)e;
						if(c.getFaction()!=f)
						{
							boolean collides=false;
							if(c.getX()<=ix&&c.getX()+c.getSize()>=ix&&c.getY()<=iy&&c.getY()+c.getSize()>=iy) collides=true;
							else{
								if(Level.getDist(ix, iy,e.getX(), e.getY())<=radius) collides=true;
								else if(Level.getDist(ix, iy,e.getX()+e.getSize(), e.getY())<=radius) collides=true;
								else if(Level.getDist(ix, iy,e.getX(), e.getY()+e.getSize())<=radius) collides=true;
								else if(Level.getDist(ix, iy,e.getX()+e.getSize(), e.getY()+e.getSize())<=radius) collides=true;
							}
							if(collides && c.getLife()>0){
								c.setLife(c.getLife()-damage);
								if(impactAnimationMetrics!=null){
									this.addUpperAnimation(new DefaultAnimation(c.getX()+(c.getSize()/2),c.getY()+(c.getSize()/2),impactAnimationMetrics,true));
								}
							}
						}
					}
				}
			}
		}
	}
	//|TODO test method
	public void addAnimation(Animation a){
		this.auxAnimationArray.add(a);
	}
	public void addUpperAnimation(Animation a){
		this.auxUpperAnimationArray.add(a);
	}
	public boolean isPartOfPlayers(ShooterCharacter sc)
	{
		for(Player p:this.players)
		{
			if(p.getCharacter()!=null && p.getCharacter().equals(sc)) return true;
		}
		return false;
	}
	public boolean gameEnded()
	{
		synchronized(this.gameEnded)
		{
			return this.gameEnded.booleanValue();
		}
	}
	public void setGameEnded()
	{
		synchronized(this.gameEnded)
		{
			this.gameEnded.notifyAll();
			this.gameEnded=true;
		}
	}
	public java.lang.Object getGameEndedLock()
	{
		return this.gameEnded;
	}
	@Override
	public int getWidth() {
		return this.map.getWidth()*DrawingManager2D.TILE_SIZE;
	}

	@Override
	public int getHeight() {
		return this.map.getHeight()*DrawingManager2D.TILE_SIZE;
	}
	public Map getMap(){
		return this.map;
	}
	
	public int[] getTeamMembers()
	{
		return this.teamMembers;
	}
	/*public void fullClean()
	{
		this.map.fullClean();
		
	}*/
	
	private static float getDist(float ix,float iy,float fx,float fy)
	{
		return (float) Math.sqrt((fx-ix)*(fx-ix) + (fy-iy)*(fy-iy));
	}
	
	public Player[] GetPlayers(){
		return this.players;
	}
	
	public boolean hadMarinesWonTheGame()
	{
		return this.marinesWin;
	}
}
