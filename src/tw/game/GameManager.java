package tw.game;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Properties;
import org.lwjgl.LWJGLException;
import tw.Main;
import tw.audio.AudioManager;
import tw.audio.MutedAudioManager;
import tw.gui.DrawingManager;
import tw.gui.DrawingManager2D;
import tw.gui.GamePanel;
import tw.gui.Screen;
import tw.gui3d.Gui3DModule;
import tw.utils.InputHandler;
import tw.utils.MapManager;
import tw.utils.PlayerInputListener;

/**
 * Manages the game status, starting levels as needed
 */
public class GameManager 
{
	public GameManager(Properties properties,Screen screen,DrawingManager dm,boolean activate3DVisor)
	{
		//Creating gamePanel, in wich all game will be drawn
		GamePanel gp=new GamePanel((DrawingManager2D)dm,new InputHandler(4));
		//Ugly way for exiting, but nothing better for now
		gp.getInputHandler().registerPlayerListenerToKey(new PlayerInputListener(){
			@Override
			public void onPlayerInputPressed(KEY_TYPE kt) {
				System.exit(0);
			}
		}, KeyEvent.VK_ESCAPE);
		gp.getInputHandler().registerPlayerListenerToKey(new PlayerInputListener(){
			@Override
			public void onPlayerInputPressed(KEY_TYPE kt) {
				AudioManager.getInstance().stopAllMusic();
				AudioManager.getInstance().stopAllSounds();
				AudioManager.setAudioManager(new MutedAudioManager());
			}
		}, KeyEvent.VK_M);
		screen.setActiveView(gp);
		
		Gui3DModule gui3dmodule=null;
		if(activate3DVisor){
			try {
				gui3dmodule=new Gui3DModule();
			} catch (LWJGLException e) {
				System.err.println("Error initcializing 3d view module. Program will start without 3d");
				gui3dmodule=null;
				activate3DVisor=false;
			}
		}
		// 5 LEVELS WOW
		
		MapManager.MapCode[] levels={MapManager.MapCode.Level1,MapManager.MapCode.Level2,MapManager.MapCode.Level3,MapManager.MapCode.Level4,MapManager.MapCode.Level5,MapManager.MapCode.REAL1};
		int currentLevel=0;
		
		while(currentLevel<levels.length)
		{
			Level l=null;
			try
			{
				Map m=MapManager.getMap(levels[currentLevel]);
				l=new Level(m,gp);
				if(activate3DVisor) gui3dmodule.run(l);
				synchronized(l.getGameEndedLock())
				{
					while(!l.gameEnded())
					{
						try {
							System.out.println("ENTER");
							l.getGameEndedLock().wait();
							System.out.println("EXIT");
						} catch (InterruptedException e) {}
					}
				}
				if(l.hadMarinesWonTheGame()) {
					currentLevel++;
				}
			}	
			catch(Exception e)
			{
				System.err.println("An error has ocurred. Game will try to continue.");
				System.err.println("A error dump has been created in "+Main.errorDumpFile.getAbsolutePath());
				System.err.println("GAME MANAGER ERROR");
				e.printStackTrace();
			}
			finally{
				if(l!=null)l.setGameEnded();
				System.out.println("GAME ENDED");
				gp.registerInDrawLoop(null);
				gp.interruptPrintThread();
			}
		}
		System.out.println("GAME WON YEAH");
	}
}
