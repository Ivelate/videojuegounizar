package tw.game;

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

import tw.IA.Util.Astar.Astar;
import tw.LoadTMX.loader.TMXMapReader;
import tw.entities.Entity;
import tw.entities.Character;
import tw.gui.DrawingManager2D;
import tw.tiles.Tile;
import tw.tiles.TileLibrary;
import tw.utils.Drawable2D;

/**
 * Tiled particular map, with all the information it haves in the game,
 * from the tiles whom it is formed, to the entities placed in it.
 * Offers methods to manage collisions and control movements in it.
 */
public class Map implements Drawable2D
{
	private final byte[][] tiles;			//Map tiles, inmutable
	private final int width;
	private final int height;
	private LinkedList<Entity>[][] entities;//Location of entities in the map tiles
	private LinkedList<Entity> outOfTheMapEntities=new LinkedList<Entity>();
	private Astar aStar;
	
		/**
		 * Carga Un mapa a Partir de un TMX
		 * @param rutaTMX
		 * @throws Exception 
		 */
		@SuppressWarnings("unchecked")
		public Map(String mapFile) throws Exception
		{
			final TMXMapReader reader= new TMXMapReader();
			tw.LoadTMX.core.Map mapa=reader.readMap(mapFile);
			this.width=mapa.getWidth();
			this.height=mapa.getHeight();
			this.tiles=mapa.GetbytesMapTerrain();			
			this.aStar=new Astar(tiles);
			
			//Initcialize entities
				this.entities=new LinkedList[this.width][this.height];
				for(int w=0;w<entities.length;w++)
				{
					for(int h=0;h<entities[0].length;h++)
					{
						this.entities[w][h]=new LinkedList<Entity>();
					}
				}		
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public Map(byte[][] tiles)
	{
		this.tiles=tiles;
		this.width=tiles.length;
		this.height=tiles[0].length;
		this.aStar=new Astar(tiles);
		//inicialize entities
		this.entities=new LinkedList[this.width][this.height];
		for(int w=0;w<entities.length;w++)
		{
			for(int h=0;h<entities[0].length;h++)
			{
				this.entities[w][h]=new LinkedList<Entity>();
			}
		}
	}
	
	
	/**
	 * Updates in <entities> the positions occuped by <e>. If <e> hasnt been registered before, use registerFirstMovement(<e>) instead
	 */
	public synchronized void registerEntityMovement(Entity e,float lastx,float lasty)
	{
		if((e.getX()<0||e.getY()<0||e.getX()>=this.getWidth()||e.getY()>=this.getHeight())&&(e instanceof Character)) {((Character) e).setLife(-1);}
		//Iterating the last positions and checking if entity had abandoned them
		for(int x=(int)lastx;x<=(int)(lastx+e.getSize());x++)
		{
			for(int y=(int)lasty;y<=(int)(lasty+e.getSize());y++)
			{
				if( x<(int)(e.getX()) || x> (int)(e.getX()+e.getSize()) ||
					y<(int)(e.getY()) || y> (int)(e.getY()+e.getSize()) )	
					if(x<this.getWidth()&&y<this.getHeight()&&x>=0&y>=0) this.entities[x][y].remove(e);
			}
		}
		
		//Iterating over the new position and checking if entity was not registered in them
		//Iterating the last positions and checking if entity had abandoned them
		for(int x=(int)e.getX();x<=(int)(e.getX()+e.getSize());x++)
		{
			for(int y=(int)e.getY();y<=(int)(e.getY()+e.getSize());y++)
			{
				if( x<(int)(lastx) || x> (int)(lastx+e.getSize()) ||
					y<(int)(lasty) || y> (int)(lasty+e.getSize()) )	
					if(x<this.getWidth()&&y<this.getHeight()&&x>=0&&y>=0) this.entities[x][y].add(e);
					else if(e instanceof Character) ((Character) e).setLife(-1);
			}
		}
	}
	
	/**
	 * Registers in <entities> the positions occuped by <e>. This method must be called only one time by entity
	 */
	public synchronized void registerEntityFirstMovement(Entity e)
	{
		for(int x=(int)e.getX();x<=(int)(e.getX()+e.getSize());x++)
		{
			for(int y=(int)e.getY();y<=(int)(e.getY()+e.getSize());y++)
			{
				if(x<this.getWidth()&&y<this.getHeight()&&x>=0&&y>=0)  this.entities[x][y].add(e);
				else if(e instanceof Character) ((Character) e).setLife(-1);
			}
		}
	}
	
	/**
	 * Unregisters entity <e> from <entities>. Cleanup method
	 */
	public synchronized void unregisterEntityMovement(Entity e)
	{
		for(int x=(int)e.getX();x<=(int)(e.getX()+e.getSize());x++)
		{
			for(int y=(int)e.getY();y<=(int)(e.getY()+e.getSize());y++)
			{
				if(x<this.getWidth()&&y<this.getHeight()&&x>=0&&y>=0)  this.entities[x][y].remove(e);
			}
		}
	}
	
	public synchronized LinkedList<Entity> getEntitiesAt(float x,float y)
	{
		if(x<0||y<0) return this.outOfTheMapEntities;
		return this.getEntitiesAt((int)x, (int)y);
	}
	public synchronized LinkedList<Entity> getEntitiesAt(int x,int y)
	{
		if(x>=0&&y>=0&&x<this.getWidth()&&y<this.getHeight()) return this.entities[x][y];
		else return this.outOfTheMapEntities;
	}
	public synchronized LinkedList<LinkedList<Entity>> getEntitiesInRange(float ix,float iy,float fx,float fy)
	{
		LinkedList<LinkedList<Entity>> toRet=new LinkedList<LinkedList<Entity>>();
		for(int x=(int)ix;x<=(int)fx;x++)
		{
			for(int y=(int)iy;y<=(int)fy;y++)
			{
				if(x<this.getWidth()&&y<this.getHeight()&&x>=0&&y>=0) toRet.add(this.entities[x][y]);
			}
		}
		return toRet;
	}
	public Tile getTileAt(int x,int y)
	{
		if(x<this.getWidth()&&y<this.getHeight()&&x>=0&&y>=0) return TileLibrary.get(this.tiles[x][y]);
		else return TileLibrary.get((byte)(-1));
	}
	public Tile getTileAt(float x,float y)
	{
		if(x<0||y<0) return TileLibrary.DEFAULT_UNDEFINED_TILE;
		
		return getTileAt((int)x,(int)y);
	}
	
	public byte[][] getTiles()
	{
		return this.tiles;
	}
	
	public int getWidth()
	{
		return this.width;
	}
	
	public int getHeight()
	{
		return this.height;
	}
	
	public Astar GetAstarAlgorithm(){
		return this.aStar;
	}

	/**
	 * WARNING this will re-compute the map each time is used. Redraw precomputed map instead
	 */
	@Deprecated
	@Override
	public void draw2D(DrawingManager2D dm,Graphics2D g,float tEl) {
		dm.drawMap(tiles, g);
	}
	
	public synchronized void drawAllRegisteredEntities2D(DrawingManager2D dm,Graphics2D g,float tEl)
	{
		for(int h=0;h<this.entities[0].length;h++){
			for(int w=0;w<this.entities.length;w++){
				for(Entity e:this.entities[w][h]){
					if((int)e.getX() == w && (int)(e.getY()+e.getSize())==h){
						e.draw2D(dm, g, tEl);
					}
				}
			}
		}
	}
	
}
