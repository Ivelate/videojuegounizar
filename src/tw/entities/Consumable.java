package tw.entities;

import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.game.Level;
import tw.game.Map;

public abstract class Consumable extends Entity{

	private static final EntityType consumableType=new EntityType( Role.OTHER,Faction.OTHER);
	private boolean expired=false;
	protected float lifetime;
	private float maxlifetime;
	public Consumable(float lifetime,Map map, float firstX, float firstY) {
		super(map, firstX, firstY);
		this.maxlifetime=lifetime;
		this.lifetime=0;
	}
	public abstract boolean canBeConsumedBy(Character c,Level l);
	public void setConsumedBy(Character c,Level l)
	{
		if(!expired&&this.canBeConsumedBy(c,l))
		{
			this.expired=true;
			performConsumptionEffects(c,l);
		}
	}
	protected abstract void performConsumptionEffects(Character c,Level l);
	public boolean update(float tEl)
	{
		this.lifetime+=tEl;
		if(this.lifetime>=this.maxlifetime) this.expired=true;
		
		return this.expired;
	}
	@Override
	protected boolean collidesWith(Entity e) {
		return false;
	}
	@Override
	public EntityType getType() {
		return consumableType;
	}
	@Override
	public boolean isCollidable() {
		return false;
	}
}
