package tw.entities;

import java.util.LinkedList;

import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.tiles.Tile;
import tw.tiles.Tile.TileState;
import tw.utils.Coordinate;
import tw.utils.Shootable;
/**
 * CAUTION: THIS CLASS CONTAINS HIGHLY UNREADABLE (BUT FUNCTIONAL) CODE. PROCEED WITH CAUTION
 * SPAGUETTI CODE BELOW
 */
public abstract class Character extends Entity implements Shootable
{
	public enum Direction{UP,DOWN,LEFT,RIGHT,UPLEFT,UPRIGHT,DOWNLEFT,DOWNRIGHT}
	public enum CollisionCode
	{
		NONE,WITH_ENTITY,WITH_WALL,WITH_LIMIT;
		public static CollisionCode sortByPriority(CollisionCode cc1,CollisionCode cc2)
		{
			if(cc1==WITH_LIMIT||cc2==WITH_LIMIT) return CollisionCode.WITH_LIMIT;
			if(cc1==WITH_WALL||cc2==WITH_WALL) return CollisionCode.WITH_WALL;
			if(cc1==WITH_ENTITY||cc2==WITH_ENTITY) return CollisionCode.WITH_ENTITY;
			return CollisionCode.NONE;
		}
		public static CollisionCode sortByPriority(CollisionCode cc1,CollisionCode cc2,CollisionCode cc3)
		{
			return sortByPriority(sortByPriority(cc1,cc2),cc3);
		}
	};
	private int maxLife;
	private int life;
	
	private Direction lookingDirection=Direction.DOWN;
	
	public Character(int life,Map map, float firstX, float firstY) {
		super(map, firstX, firstY);
		this.life=life;
		this.maxLife=life;
	}
	
	/**
	 * Moves this entity in the direction <d>. Manages collisions with terrain
	 */
	public CollisionCode move(Direction d,Level l,float tEl)
	{
		float limx=this.getX();
		float limy=this.getY();
		switch(d)
		{
		case UP:
			limy=-1;
			break;
		case DOWN:
			limy=this.currentMap.getHeight()+1;
		case RIGHT:
			limx=this.currentMap.getWidth()+1;
			break;
		case LEFT:
			limx=-1;
			break;
		case UPLEFT:
			limx=-1;
			limy=-1;
			break;
		case UPRIGHT:
			limy=-1;
			limx=this.currentMap.getWidth()+1;
			break;
		case DOWNLEFT:
			limy=this.currentMap.getHeight()+1;
			limx=-1;
			break;
		case DOWNRIGHT:
			limy=this.currentMap.getHeight()+1;
			limx=this.currentMap.getWidth()+1;
			break;
		}
		return moveWithLimits(d,limx,limy,l,tEl);
	}
	
	public boolean relocateAvoidingCollisions()
	{
		this.moveWithLimits(Direction.RIGHT, this.getX(), this.getY(), null,1);
		this.moveWithLimits(Direction.UP, this.getX(), this.getY(), null,1);
		this.moveWithLimits(Direction.DOWN, this.getX(), this.getY(), null,1);
		return this.moveWithLimits(Direction.LEFT, this.getX(), this.getY(), null,1) != null;
	}
	
	/**
	 * Moves this entity in a direction <d> with coordinate limits x <limx> and y <limy>. Manages collisions with terrain
	 */
	public CollisionCode moveWithLimits(Direction d,float limx,float limy,Level l,float tEl)
	{
		boolean nolimitx=false;
		boolean nolimity=false;
		this.lookingDirection=d;
		CollisionCode toRet=CollisionCode.NONE;
		CollisionCode movCode=CollisionCode.NONE;
		CollisionCode movCode2=CollisionCode.NONE;
		if(limx<0) {limx=0; nolimitx=true;}
		if(limy<0) {limy=0; nolimity=true;}
		
		float xobj=this.getX();
		float yobj=this.getY();
		switch(d)
		{
		case UP:
			if(limy>this.getY()) limy=this.getY();
			yobj=yobj-this.getVel()*tEl;
			if(yobj<limy) {yobj=limy; toRet=CollisionCode.WITH_LIMIT;}
			movCode=moveTo(d,xobj,yobj,l);
			break;
		case DOWN:
			if(limy<this.getY()) limy=this.getY();
			yobj=yobj+this.getVel()*tEl;
			if(yobj>limy) {yobj=limy; toRet=CollisionCode.WITH_LIMIT;}
			movCode=moveTo(d,xobj,yobj,l);
			break;
		case RIGHT:
			if(limx<this.getX()) limx=this.getX();
			xobj=xobj+this.getVel()*tEl;
			if(xobj>limx) {xobj=limx; toRet=CollisionCode.WITH_LIMIT;}
			movCode=moveTo(d,xobj,yobj,l);
			break;
		case LEFT:
			if(limx>this.getX()) limx=this.getX();
			xobj=xobj-this.getVel()*tEl;
			if(xobj<limx) {xobj=limx; toRet=CollisionCode.WITH_LIMIT;}
			movCode=moveTo(d,xobj,yobj,l);
			break;
		//Diagonal movements compose lineal movements. 0.71 is the sin of 45�, as the character is moving in a diagonal line and
		//the linear velocity isnt mantained
		case UPLEFT:
			if(limy>this.getY()) limy=this.getY();
			if(limx>this.getX()) limx=this.getX();
			float amount=this.getVel()*tEl*0.71f;
			float amountx=amount;
			float amounty=amount;
			if(yobj-amount<limy) {amounty=yobj-limy; if(!nolimity)toRet=CollisionCode.WITH_LIMIT;}
			if(xobj-amount<limx) {amountx=xobj-limx; if(!nolimitx)toRet=CollisionCode.WITH_LIMIT;}
			if(!nolimitx&&!nolimity){
				if(amountx<amounty) amounty=amountx;
				else amountx=amounty;
			}
			else if(nolimitx&&!nolimity){
				if(amounty<amountx) amountx=amounty;
			}
			else if(!nolimitx&&nolimity){
				if(amountx<amounty) amounty=amountx;
			}
			yobj=yobj-amounty;
			movCode=this.moveTo(Direction.UP, xobj, yobj,l);
			xobj=xobj-amountx;
			movCode2=this.moveTo(Direction.LEFT, xobj, yobj,l);
			break;
		case UPRIGHT:
			if(limy>this.getY()) limy=this.getY();
			if(limx<this.getX()) limx=this.getX();
			amount=this.getVel()*tEl*0.71f;
			amountx=amount;
			amounty=amount;
			if(yobj-amount<limy) {amounty=yobj-limy; if(!nolimity)toRet=CollisionCode.WITH_LIMIT;}
			if(xobj+amount>limx) {amountx=limx-xobj; if(!nolimitx)toRet=CollisionCode.WITH_LIMIT;}
			if(!nolimitx&&!nolimity){
				if(amountx<amounty) amounty=amountx;
				else amountx=amounty;
			}
			else if(nolimitx&&!nolimity){
				if(amounty<amountx) amountx=amounty;
			}
			else if(!nolimitx&&nolimity){
				if(amountx<amounty) amounty=amountx;
			}
			yobj=yobj-amounty;
			movCode=this.moveTo(Direction.UP, xobj, yobj,l);
			xobj=xobj+amountx;
			movCode2=this.moveTo(Direction.RIGHT, xobj, yobj,l);
			break;
		case DOWNLEFT:
			if(limy<this.getY()) limy=this.getY();
			if(limx>this.getX()) limx=this.getX();
			amount=this.getVel()*tEl*0.71f;
			amountx=amount;
			amounty=amount;
			if(yobj+amount>limy) {amounty=limy-yobj; if(!nolimity)toRet=CollisionCode.WITH_LIMIT;}
			if(xobj-amount<limx) {amountx=xobj-limx; if(!nolimitx)toRet=CollisionCode.WITH_LIMIT;}
			if(!nolimitx&&!nolimity){
				if(amountx<amounty) amounty=amountx;
				else amountx=amounty;
			}
			else if(nolimitx&&!nolimity){
				if(amounty<amountx) amountx=amounty;
			}
			else if(!nolimitx&&nolimity){
				if(amountx<amounty) amounty=amountx;
			}
			yobj=yobj+amounty;
			movCode=this.moveTo(Direction.DOWN, xobj, yobj,l);
			xobj=xobj-amountx;
			movCode2=this.moveTo(Direction.LEFT, xobj, yobj,l);
			break;
		case DOWNRIGHT:
			if(limy<this.getY()) limy=this.getY();
			if(limx<this.getX()) limx=this.getX();
			amount=this.getVel()*tEl*0.71f;
			if(yobj+amount>limy) {amount=limy-yobj; toRet=CollisionCode.WITH_LIMIT;}
			if(xobj+amount>limx) {amount=limx-xobj; toRet=CollisionCode.WITH_LIMIT;}
			yobj=yobj+amount;
			movCode=this.moveTo(Direction.DOWN, xobj, yobj,l);
			xobj=xobj+amount;
			movCode2=this.moveTo(Direction.RIGHT, xobj, yobj,l);
			break;
		}
		
		return CollisionCode.sortByPriority(toRet, movCode, movCode2);
	}
	
	/**
	 * Moves entity in a direction <d> to <xobj>,<yobj> 
	 */
	private CollisionCode moveTo(Direction d,float xobj, float yobj,Level l)
	{
		CollisionCode toRet=CollisionCode.NONE;
		
		float fx=this.getX();
		float fy=this.getY();
		boolean collision=false;
		boolean firstIter=true;
		switch(d)
		{
		case RIGHT:
			for(float ix=(int)(this.getX()+this.getSize());ix<=xobj+this.getSize();ix++)
			{
				CornerCollisionResult r_col;
				if((int)this.getY()==(int)(this.getY()+this.getSize())){
					r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.RU_CORNER,ix,this.getY());
					if(r_col==null) r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.RD_CORNER,ix,this.getY()+this.getSize());
				}
				else
				{
					r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RU_CORNER,ix,this.getY());
					for(int iy=(int)(this.getY())+1;iy<(int)(this.getY()+this.getSize());iy++){
						CornerCollisionResult res=this.getBasicCollisionWith(this.currentMap.getTileAt(ix, iy), Direction.RIGHT);
						if(r_col==null) r_col=res;
						else r_col.accumulate(res, Direction.RIGHT);
					}
					if(r_col!=null) r_col.accumulate(getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RD_CORNER,ix,this.getY()+this.getSize()),Direction.RIGHT);
					else r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RD_CORNER,ix,this.getY()+this.getSize());
				}
				if(r_col!=null){
					if(r_col.containsPoint()){
						fx=(int)ix+r_col.getPx()-this.getSize();
						if(fx>xobj) fx=xobj;
						collision=true;
						break;
					}
					else if(!firstIter){
						fx=(int)(ix) -this.getSize()-0.001f;
						collision=true;
						break;
					}
				}
				firstIter=false;
			}
			break;
		case LEFT:
			for(float ix=this.getX();ix>=(int)(xobj);ix--)
			{
				CornerCollisionResult r_col;
				if((int)this.getY()==(int)(this.getY()+this.getSize())){
					r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.LU_CORNER,ix,this.getY());
					if(r_col==null) r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.LD_CORNER,ix,this.getY()+this.getSize());
				}
				else
				{
					r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.LU_CORNER,ix,this.getY());
					for(int iy=(int)(this.getY())+1;iy<(int)(this.getY()+this.getSize());iy++){
						CornerCollisionResult res=this.getBasicCollisionWith(this.currentMap.getTileAt(ix, iy), Direction.LEFT);
						if(r_col==null) r_col=res;
						else r_col.accumulate(res, Direction.LEFT);
					}
					if(r_col!=null) r_col.accumulate(getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.LD_CORNER,ix,this.getY()+this.getSize()),Direction.LEFT);
					else r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.LD_CORNER,ix,this.getY()+this.getSize());
				}
				if(r_col!=null){
					if(r_col.containsPoint()){
						fx=(int)(ix)+r_col.getPx();
						if(fx<xobj) fx=xobj;
						collision=true;
						break;
					}
					else if(!firstIter){
						fx=(int)(ix)+1.001f;
						collision=true;
						break;
					}
				}
				firstIter=false;
			}
			break;
		case UP:
			for(float iy=this.getY();iy>=(int)(yobj);iy--)
			{
				CornerCollisionResult r_col;
				if((int)this.getX()==(int)(this.getX()+this.getSize())){
					r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.LU_CORNER,this.getX(),iy);
					if(r_col==null) r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.RU_CORNER,this.getX()+this.getSize(),iy);
				}
				else
				{
					r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.LU_CORNER,this.getX(),iy);
					for(int ix=(int)(this.getX())+1;ix<(int)(this.getX()+this.getSize());ix++){
						CornerCollisionResult res=this.getBasicCollisionWith(this.currentMap.getTileAt(ix, iy), Direction.UP);
						if(r_col==null) r_col=res;
						else r_col.accumulate(res, Direction.UP);
					}
					if(r_col!=null) r_col.accumulate(getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RU_CORNER,this.getX()+this.getSize(),iy),Direction.UP);
					else r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RU_CORNER,this.getX()+this.getSize(),iy);
				}
				if(r_col!=null){
					if(r_col.containsPoint()){
						fy=(int)(iy)+r_col.getPy();
						if(fy<yobj) fy=yobj;
						collision=true;
						break;
					}
					else if(!firstIter){
						fy=(int)(iy)+1.001f;
						collision=true;
						break;
					}
				}
				firstIter=false;
			}
			break;
		case DOWN:
			for(float iy=(int)(this.getY()+this.getSize());iy<=yobj+this.getSize();iy++)
			{
				CornerCollisionResult r_col;
				if((int)this.getX()==(int)(this.getX()+this.getSize())){
					r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.LD_CORNER,this.getX(),iy);
					if(r_col==null) r_col=getCollisionPointWithCornerDoingLinealMov(d,TileState.RD_CORNER,this.getX()+this.getSize(),iy);
				}
				else
				{
					r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.LD_CORNER,this.getX(),iy);
					for(int ix=(int)(this.getX())+1;ix<(int)(this.getX()+this.getSize());ix++){
						CornerCollisionResult res=this.getBasicCollisionWith(this.currentMap.getTileAt(ix, iy), Direction.DOWN);
						if(r_col==null) r_col=res;
						else r_col.accumulate(res, Direction.DOWN);
					}
					if(r_col!=null) r_col.accumulate(getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RD_CORNER,this.getX()+this.getSize(),iy),Direction.DOWN);
					else r_col=getCollisionPointWithCornerDoingLinealMovStrict(d,TileState.RD_CORNER,this.getX()+this.getSize(),iy);
				}
				if(r_col!=null){
					if(r_col.containsPoint()){
						fy=(int)iy+r_col.getPy()-this.getSize();
						if(fy>yobj) fy=yobj;
						collision=true;
						break;
					}
					else if(!firstIter){
						fy=(int)(iy) -this.getSize()-0.001f;
						collision=true;
						break;
					}
				}
				firstIter=false;
			}
			break;
		}
		
		if(!collision) 
		{
			if(d==Direction.LEFT||d==Direction.RIGHT) fx=xobj; 
			if(d==Direction.UP||d==Direction.DOWN)  fy=yobj;
		}
		else toRet=CollisionCode.WITH_WALL;
		
		if(fx<0) fx=0; 
		if(fy<0) fy=0;
		
		//Collision with other entities in map
		Coordinate finalPoint=manageCollisionsWithMapEntitiesAndConsumeNearestConsumable(d,fx,fy,l);
		if(finalPoint!=null)
		{
			fx=finalPoint.getX(); 
			fy=finalPoint.getY();
			toRet=CollisionCode.WITH_ENTITY;
		}
		
		if(d==Direction.LEFT) if(fx<xobj)fx=xobj; 
		if(d==Direction.UP) if(fy<yobj) fy=yobj;
		
		this.setXY(fx, fy);

		return toRet;
	}
	
	/**
	 * Returns the coordinate that this entity has to set if it does not want to collide with other entities
	 */
	private Coordinate manageCollisionsWithMapEntitiesAndConsumeNearestConsumable(Direction d,float fx,float fy,Level l){
		LinkedList<LinkedList<Entity>> candidates=this.currentMap.getEntitiesInRange(fx, fy, fx+this.getSize(), fy+this.getSize());
		
		Coordinate coord=null;
		Coordinate consumableCoord=null;
		Consumable nearestConsumable=null;
		for(LinkedList<Entity> ll:candidates)
		{
			for(Entity e:ll)
			{
				if(e.isCollidable()&&(e.collidesWith(this)||this.collidesWith(e)))
				{
					if(e instanceof Character && ((Character)e).getLife()<=0) continue;
					
					Coordinate coll=this.getCollisionPointWith(e, d, fx, fy,true);
					if(coll!=null)
					{
						if(coord==null) coord=coll;
						else
						{
							switch(d)
							{
							case UP:
								if(coord.getY()<coll.getY()) {
									coord=coll;
									if(consumableCoord!=null&&consumableCoord.getY()<coll.getY()) consumableCoord=null;
								}
								break;
							case DOWN:
								if(coord.getY()>coll.getY()) {
									coord=coll;
									if(consumableCoord!=null&&consumableCoord.getY()>coll.getY()) consumableCoord=null;
								}
								break;
							case LEFT:
								if(coord.getX()<coll.getX()) {
									coord=coll;
									if(consumableCoord!=null&&consumableCoord.getX()<coll.getX()) consumableCoord=null;
								}
								break;
							case RIGHT:
								if(coord.getX()>coll.getX()) {
									coord=coll;
									if(consumableCoord!=null&&consumableCoord.getX()>coll.getX()) consumableCoord=null;
								}
								break;
							}
						}
					}
				}
				else if(e instanceof Consumable && l!=null)
				{
					Consumable c=(Consumable) e;
					if(c.canBeConsumedBy(this,l))
					{
						Coordinate coll=this.getCollisionPointWith(c, d, fx, fy,false);
						if(coll!=null)
						{
							if(coord==null) {consumableCoord=coll;nearestConsumable=c;}
							else
							{
								switch(d)
								{
								case UP:
									if(coord.getY()<coll.getY()){
										if(consumableCoord==null||consumableCoord.getY()<coll.getY()) {consumableCoord=coll;nearestConsumable=c;}
									}
									break;
								case DOWN:
									if(coord.getY()>coll.getY()){
										if(consumableCoord==null||consumableCoord.getY()>coll.getY()) {consumableCoord=coll;nearestConsumable=c;}
									}
									break;
								case LEFT:
									if(coord.getX()<coll.getX()){
										if(consumableCoord==null||consumableCoord.getX()<coll.getX()) {consumableCoord=coll;nearestConsumable=c;}
									}
									break;
								case RIGHT:
									if(coord.getX()>coll.getX()){
										if(consumableCoord==null||consumableCoord.getX()>coll.getX()) {consumableCoord=coll;nearestConsumable=c;}
									}
									break;
								}
							}
						}
					}
				}
			}
		}
		if(consumableCoord!=null) {
			nearestConsumable.setConsumedBy(this,l);
		}
		
		return coord;
	}
	
	/**
	 * Unused, diagonal movements are now a composition of lineal movements, so this method have no use
	 */
	@Deprecated
	private CornerCollisionResult getCollisionPointWithCornerDoingDiagonalMov(Direction d,TileState corner, float x,float y,Tile t)
	{
		TileState tileState=t.getLimitsFor(this.getType());
		
		if(tileState==TileState.EMPTY) return null;
		if(tileState==TileState.FULL || tileState!=corner) return new CornerCollisionResult();
		
		boolean weak=(d==Direction.DOWNLEFT&&corner!=TileState.LD_CORNER)||
				(d==Direction.DOWNRIGHT&&corner!=TileState.RD_CORNER)||
				(d==Direction.UPLEFT&&corner!=TileState.LU_CORNER) ||
				(d==Direction.UPRIGHT&&corner!=TileState.RU_CORNER);
		
		if(weak) return null;
		else return getCornerCollisionFor(d,tileState,x,y);
	}
	
	/**
	 * Gets the collision point (If exists) of the corner <corner> with terrain.
	 */
	private CornerCollisionResult getCollisionPointWithCornerDoingLinealMov(Direction d,TileState corner, float x,float y)
	{
		int ix=(int)x; 
		int iy=(int)y;
		return getCollisionPointWithCornerDoingLinealMov(d,corner,x-ix,y-iy,this.currentMap.getTileAt(ix, iy));
	}
	private CornerCollisionResult getCollisionPointWithCornerDoingLinealMov(Direction d,TileState corner, float x,float y,Tile t)
	{
		TileState tileState=t.getLimitsFor(this.getType());
		
		if(tileState==TileState.EMPTY) return null;
		if(tileState==TileState.FULL) return new CornerCollisionResult();
		
		switch(d)
		{
		case UP:
			if(tileState==TileState.U_HALF){
				return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(tileState==TileState.L_HALF&&corner==TileState.RU_CORNER) return null;
			else if(tileState==TileState.R_HALF&&corner==TileState.RU_CORNER) return x>0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.L_HALF&&corner==TileState.LU_CORNER) return x<0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.R_HALF&&corner==TileState.LU_CORNER) return null;
			else if(tileState==TileState.LU_CORNER||tileState==TileState.RU_CORNER) {
				if(tileState==corner)return getCornerCollisionFor(d,tileState,x,y);
				else return null;
			}
			break;
		case DOWN:
			if(tileState==TileState.D_HALF){
				return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(tileState==TileState.L_HALF&&corner==TileState.RD_CORNER) return null;
			else if(tileState==TileState.R_HALF&&corner==TileState.RD_CORNER) return x>0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.L_HALF&&corner==TileState.LD_CORNER) return x<0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.R_HALF&&corner==TileState.LD_CORNER) return null;
			else if(tileState==TileState.LD_CORNER||tileState==TileState.RD_CORNER) {
				if(tileState==corner)return getCornerCollisionFor(d,tileState,x,y);
				else return null;
			}
			break;
		case LEFT:
			if(tileState==TileState.L_HALF){
				return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(tileState==TileState.U_HALF&&corner==TileState.LD_CORNER) return null;
			else if(tileState==TileState.D_HALF&&corner==TileState.LD_CORNER) return y>0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.U_HALF&&corner==TileState.LU_CORNER) return y<0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.D_HALF&&corner==TileState.LU_CORNER) return null;
			else if(tileState==TileState.LD_CORNER||tileState==TileState.LU_CORNER) {
				if(tileState==corner)return getCornerCollisionFor(d,tileState,x,y);
				else return null;
			}
			break;
		case RIGHT:
			if(tileState==TileState.R_HALF){
				return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(tileState==TileState.U_HALF&&corner==TileState.RD_CORNER) return null;
			else if(tileState==TileState.D_HALF&&corner==TileState.RD_CORNER) return y>0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.U_HALF&&corner==TileState.RU_CORNER) return y<0.5f?new CornerCollisionResult():null;
			else if(tileState==TileState.D_HALF&&corner==TileState.RU_CORNER) return null;
			else if(tileState==TileState.RD_CORNER||tileState==TileState.RU_CORNER) {
				if(tileState==corner)return getCornerCollisionFor(d,tileState,x,y);
				else return null;
			}
			break;
		}
		
		return new CornerCollisionResult();
	}
	
	/**
	 * Gets the collision point (If exists) of the corner <corner> with terrain. If the corner collides with a triangular tile
	 * wich is not exactly oriented as the corner, it will be considered full.
	 */
	private CornerCollisionResult getCollisionPointWithCornerDoingLinealMovStrict(Direction d,TileState corner, float x,float y)
	{
		int ix=(int)x; 
		int iy=(int)y;
		return getCollisionPointWithCornerDoingLinealMovStrict(d,corner,x-ix,y-iy,this.currentMap.getTileAt(ix, iy));
	}
	private CornerCollisionResult getCollisionPointWithCornerDoingLinealMovStrict(Direction d,TileState corner, float x,float y,Tile t)
	{
		TileState tileState=t.getLimitsFor(this.getType());
		
		if(tileState==TileState.EMPTY) return null;
		if(tileState==TileState.FULL) return new CornerCollisionResult();
		if(!(tileState==TileState.R_HALF||tileState==TileState.U_HALF||tileState==TileState.D_HALF||tileState==TileState.L_HALF) && tileState!=corner) return new CornerCollisionResult();
		
		switch(d)
		{
		case UP:
			if(corner==TileState.LU_CORNER){
				if(tileState==TileState.L_HALF) return (x-(int)(x))<0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.R_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.LU_CORNER||tileState==TileState.RU_CORNER || tileState==TileState.U_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(corner==TileState.RU_CORNER){
				if(tileState==TileState.L_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.R_HALF) return (x-(int)(x))>0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.LU_CORNER||tileState==TileState.RU_CORNER || tileState==TileState.U_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			break;
		case DOWN:
			if(corner==TileState.LD_CORNER){
				if(tileState==TileState.L_HALF) return (x-(int)(x))<0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.R_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.LD_CORNER||tileState==TileState.RD_CORNER || tileState==TileState.D_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(corner==TileState.RD_CORNER){
				if(tileState==TileState.L_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.R_HALF) return (x-(int)(x))>0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.LD_CORNER||tileState==TileState.RD_CORNER || tileState==TileState.D_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			break;
		case LEFT:
			if(corner==TileState.LD_CORNER){
				if(tileState==TileState.D_HALF) return (y-(int)(y))>0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.U_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.LD_CORNER||tileState==TileState.LU_CORNER || tileState==TileState.L_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(corner==TileState.LU_CORNER){
				if(tileState==TileState.D_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.U_HALF) return (y-(int)(y))<0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.LD_CORNER||tileState==TileState.LU_CORNER || tileState==TileState.L_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			break;
		case RIGHT:
			if(corner==TileState.RD_CORNER){
				if(tileState==TileState.D_HALF) return (y-(int)(y))>0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.U_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.RD_CORNER||tileState==TileState.RU_CORNER || tileState==TileState.R_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			else if(corner==TileState.RU_CORNER){
				if(tileState==TileState.D_HALF) return new CornerCollisionResult();
				else if(tileState==TileState.U_HALF) return (y-(int)(y))<0.5f?new CornerCollisionResult() : null;
				else if(tileState==TileState.RD_CORNER||tileState==TileState.RU_CORNER || tileState==TileState.R_HALF) return getCornerCollisionFor(d,tileState,x,y);
			}
			break;
		}
		
		return new CornerCollisionResult();
	}
	
	/**
	 * Returns false if and only if the tile <t> is totally empty
	 */
	private CornerCollisionResult getBasicCollisionWith(Tile t,Direction d)
	{
		TileState ts=t.getLimitsFor(this.getType());
		if(ts==TileState.EMPTY) return null;
		else if(ts==TileState.D_HALF && d==Direction.DOWN) return new CornerCollisionResult(0,0.5f);
		else if(ts==TileState.U_HALF && d==Direction.UP) return new CornerCollisionResult(0,0.5f);
		else if(ts==TileState.L_HALF && d==Direction.LEFT) return new CornerCollisionResult(0.5f,0);
		else if(ts==TileState.R_HALF && d==Direction.RIGHT) return new CornerCollisionResult(0.5f,0);
		else return new CornerCollisionResult();
	}
	
	/**
	 * Gets the exact corner collision point of a corner colliding with a triangular tile
	 */
	private CornerCollisionResult getCornerCollisionFor(Direction d,TileState tileState,float x,float y)
	{
		int ex=(int)x;
		int ey=(int)y;
		float dx=x-ex;
		float dy=y-ey;
		switch(tileState)
		{
		case RD_CORNER:
			if(d==Direction.DOWNRIGHT) return new CornerCollisionResult(ex+(dx/(dx+dy)),ey+(dy/(dx+dy)));
			else if(d==Direction.DOWN) return new CornerCollisionResult(x,ey+(1-dx));
			else if(d==Direction.RIGHT) return new CornerCollisionResult(ex+(1-dy),y);
			break;
		case LD_CORNER:
			if(d==Direction.DOWNLEFT) return new CornerCollisionResult(ex+(dx/(dx+dy)),ey+(dy/(dx+dy)));
			else if(d==Direction.DOWN) return new CornerCollisionResult(x,ey+dx);
			else if(d==Direction.LEFT) return new CornerCollisionResult(ex+dy,y);
			break;
		case LU_CORNER:
			if(d==Direction.UPLEFT) return new CornerCollisionResult(ex+(dx/(dx+dy)),ey+(dy/(dx+dy)));
			else if(d==Direction.UP) return new CornerCollisionResult(x,ey+(1-dx));
			else if(d==Direction.LEFT) return new CornerCollisionResult(ex+(1-dy),y);
			break;
		case RU_CORNER:
			if(d==Direction.UPRIGHT) return new CornerCollisionResult(ex+(dx/(dx+dy)),ey+(dy/(dx+dy)));
			else if(d==Direction.UP) return new CornerCollisionResult(x,ey+dx);
			else if(d==Direction.RIGHT) return new CornerCollisionResult(ex+dy,y);
			break;
		case L_HALF:
			if(d==Direction.LEFT) return new CornerCollisionResult(ex+0.5f,y);
			break;
		case U_HALF:
			if(d==Direction.UP) return new CornerCollisionResult(x,ey+0.5f);
			break;
		case D_HALF:
			if(d==Direction.DOWN) return new CornerCollisionResult(x,ey+0.5f);
			break;
		case R_HALF:
			if(d==Direction.RIGHT) return new CornerCollisionResult(ex+0.5f,y);
			break;
		case FULL:
			//SHOULD NOT HAPPEN
			return new CornerCollisionResult();
		case EMPTY:
			//Should NOT HAPPEN
			return null;
		}
		return null;
	}
	
	@Override
	public boolean receiveShoot(Bullet b,Level l) {
		this.life-=b.getDamage();
		onShootReceivedAt(b.getX(),b.getY(),l);
		return this.life<=0;
	}
	
	public int getLife()
	{
		return this.life;
	}
	public int getMaxLife()
	{
		return this.maxLife;
	}
	public float getLifePercent()
	{
		return (float)(this.life)/this.maxLife;
	}
	
	public abstract float getVel();
	
	public Faction getFaction()
	{
		return this.getType().getFaction();
	}
	public Direction getLookingDirection()
	{
		return this.lookingDirection;
	}
	public boolean canBeShooted()
	{
		if(this.life<=0) return false;
		else return canBeShootedConcrete();
	}
	public abstract boolean canBeShootedConcrete();
	public void setLife(int amount)
	{
		this.life=amount;
	}
	public abstract IAControlledCharacter onDeath(Level l);
	protected abstract void onShootReceivedAt(float x,float y,Level l);
	protected boolean isCollidable()
	{
		return true;
	}
	
	private class CornerCollisionResult
	{
		private boolean containsPoint;
		private float px,py;
		public CornerCollisionResult(float px,float py)
		{
			this.containsPoint=true;
			this.px=px;
			this.py=py;
		}
		public CornerCollisionResult()
		{
			this.containsPoint=false;
		}
		public void accumulate(CornerCollisionResult ccr,Direction d){
			if(ccr==null) return;
			
			if(this.containsPoint&&!ccr.containsPoint){
				this.containsPoint=false;
			}
			else if(this.containsPoint&&ccr.containsPoint){
				switch(d)
				{
				case UP:
					if(this.py<ccr.py){this.py=ccr.py;this.px=ccr.px;}
					break;
				case DOWN:
					if(this.py>ccr.py){this.py=ccr.py;this.px=ccr.px;}
					break;
				case LEFT:
					if(this.px<ccr.px){this.py=ccr.py;this.px=ccr.px;}
					break;
				case RIGHT:
					if(this.px>ccr.px){this.py=ccr.py;this.px=ccr.px;}
					break;
				}
			}
		}
		public boolean containsPoint() {return this.containsPoint;}
		public float getPx(){return this.px;}
		public float getPy(){return this.py;}
	}
}
