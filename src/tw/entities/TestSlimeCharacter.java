package tw.entities;

import java.awt.Graphics2D;

import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;

public class TestSlimeCharacter extends TestCharacter
{
	private float size;
	public TestSlimeCharacter(float size,Map map, Faction faction, float firstX,
			float firstY) {
		super(map, 1,faction, firstX, firstY);
		this.size=size;
	}
	@Override
	public float getSize() {
		return this.size;
	}
	@Override
	public IAControlledCharacter onDeath(Level l) {
		if(this.getSize()>=0.5f){
			l.spawnCharacter(new TestSlimeCharacter(this.size/2,this.currentMap,this.getFaction(),this.getX(),this.getY()));
			l.spawnCharacter(new TestSlimeCharacter(this.size/2,this.currentMap,this.getFaction(),this.getX()+this.getSize()/2,this.getY()));
			l.spawnCharacter(new TestSlimeCharacter(this.size/2,this.currentMap,this.getFaction(),this.getX(),this.getY()+this.getSize()/2));
			return new TestSlimeCharacter(this.size/2,this.currentMap,this.getFaction(),this.getX()+this.getSize()/2,this.getY()+this.getSize()/2);
		}
		return null;
	}
	
	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) {
		/*if(this.getFaction()==Faction.MARINE) dm.drawSoldier(this, g);
		else dm.drawBigSpider(this, g); //TODO TEST*/
		dm.drawCharacterUsingMetrics(this,Metrics2D.BIGSPIDER, g);
		
	}

}
