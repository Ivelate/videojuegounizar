package tw.entities;

import java.awt.Graphics2D;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Music;
import tw.audio.AudioManager.Sound;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DeathAnimation;
import tw.gui.animation.DefaultAnimation;
import tw.weapons.BasicBulletWeapon;
import tw.weapons.ExplosivePack;
import tw.weapons.MachinegunWeapon;
import tw.weapons.GrenadeThrower;
import tw.weapons.Shotgun;
import tw.weapons.UltimaWeapon;

public class TestPlayableCharacter extends ShooterCharacter{
	
	public TestPlayableCharacter(Map map, float firstX, float firstY) {
		super(new MachinegunWeapon(),new GrenadeThrower(0),2,map, firstX, firstY);
		// TODO Auto-generated constructor stub
	}

	@Override
	public float getSize() {
		return 0.8f;
	}

	@Override
	protected void concreteClean() {
		
	}

	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) {
		dm.drawCharacterUsingMetrics(this, Metrics2D.SOLDIER, g);
	}

	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		return 2.4f;
	}

	@Override
	public EntityType getType() {
		return new EntityType(EntityType.Role.GROUND,EntityType.Faction.MARINE);
	}

	@Override
	protected boolean collidesWith(Entity e) {
		//if(e.getType().getFaction()!=this.getType().getFaction()) return true;
		//else return false;
		return false;
	}

	@Override
	public boolean occludesSameFactionBullets() {
		return false;
	}

	@Override
	public void concreteThink() {
		this.setMovementObj(0, 0);
	}

	@Override
	public boolean canBeShootedConcrete() {
		return true;
	}

	@Override
	public IAControlledCharacter onDeath(Level l) {
		AudioManager.getInstance().playSound(Sound.SOLDIER_DEATH);
		l.addAnimation(new DeathAnimation(this.getX(),this.getY(),this.getSize(),this.getLookingDirection(),0.15f,1.5f,Metrics2D.SOLDIER));
		return null;
	}

	@Override
	protected void updatePre2(float tEl, Level l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onShoot() {
		AudioManager.getInstance().playSound(Sound.SHOOT_MACHINEGUN);
	}

	@Override
	protected void onShootReceivedAt(float x, float y, Level l) 
	{
		l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.BLOOD,true));
		
	}


}
