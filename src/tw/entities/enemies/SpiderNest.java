package tw.entities.enemies;

import java.awt.Graphics2D;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Entity;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntityType;
import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.entities.IAControlledCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;

public class SpiderNest extends IAControlledCharacter
{
	private static final int SPIDERNEST_LIFE=17;
	private static final EntityType SPIDERNEST_TYPE=new EntityType(Role.GROUND,Faction.ALIEN);
	private static final float SPAWN_COOLDOWN=5;
	private static final float SPAWN_DURATION=3;
	private static final float SPAWN_PROB=0.2f;
	
	private float spawnCooldown=SPAWN_COOLDOWN;
	private boolean spawning=false;
	private boolean hasSpawned=false;
	private float spawnCont=0;
	
	public SpiderNest(Map map, float firstX, float firstY) {
		super(SPIDERNEST_LIFE, map, firstX, firstY);
	}

	@Override
	public boolean occludesSameFactionBullets() {
		return false;
	}

	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		if(this.spawning) {
			dm.drawAnimation((int)(((SPAWN_DURATION-this.spawnCont)*AnimationMetrics2D.SPIDERNEST_SPAWN.FRAMESNUM)/SPAWN_DURATION), this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.SPIDERNEST_SPAWN, g);
		}
		else dm.drawAnimation(0, this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.SPIDERNEST_SPAWN, g);
		
	}

	@Override
	protected void updatePre(float tEl, Level l)
	{
		if(spawning){
			this.spawnCont-=tEl;
			if(!this.hasSpawned&&this.spawnCont<SPAWN_DURATION/4){
				this.hasSpawned=true;
				l.spawnCharacter(CHARACTER_TYPE.SPIDER, this.getX(), this.getY());
			}
			if(this.spawnCont<=0){
				this.spawning=false;
				this.spawnCooldown=SPAWN_COOLDOWN;
			}
		}
		else{
			if(this.spawnCooldown>0)this.spawnCooldown-=tEl;
			else
			{
				if(Math.random()<SPAWN_PROB*tEl){
					this.spawning=true;
					this.hasSpawned=false;
					this.spawnCont=SPAWN_DURATION;
				}
			}
		}
	}

	@Override
	public void concreteThink() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getConcreteVel() {
		return 0;
	}

	@Override
	public boolean canBeShootedConcrete() {
		return true;
	}

	@Override
	public IAControlledCharacter onDeath(Level l) {
		AudioManager.getInstance().playSound(Sound.SPIDERNEST_DEATH);
		l.addAnimation(new DefaultAnimation(this.getX(),this.getY(),AnimationMetrics2D.SPIDERNEST_DEATH));
		return null;
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.SPIDERNEST_SPAWN.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	public EntityType getType() {
		return SPIDERNEST_TYPE;
	}

	@Override
	protected void concreteClean() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean collidesWith(Entity e) {
		if(e.getType().getFaction()!=this.getType().getFaction()) return true;
		else return false;
	}
	
	@Override
	protected void onShootReceivedAt(float x, float y, Level l) 
	{
		l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.BLOODSPIDER,true));
		
	}

}
