package tw.entities.enemies;

import java.awt.Graphics2D;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Entity;
import tw.entities.EntitySpawner;
import tw.entities.IAControlledCharacter;
import tw.entities.TestCharacter;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;
import tw.gui.animation.SpawningDeathAnimation;
import tw.weapons.FastAlienSplitWeapon;

public class TestBigSpider extends TestCharacter
{
	private static final float SPAWN_COOLDOWN=7;
	private static final float SPAWN_DURATION=1.5f;
	private static final float SPAWN_PROB=0.1f;
	
	private float spawnCooldown=SPAWN_COOLDOWN;
	private boolean spawning=false;
	private boolean hasSpawned=false;
	private float spawnCont=0;
	
	private Astar astar;
	private List<IAControlledCharacter> list;
	
	private List<IAControlledCharacter> notCollidableChildren=new LinkedList<IAControlledCharacter>();
	
	public TestBigSpider(Map map, float firstX, float firstY) {
		super(new FastAlienSplitWeapon(),map, 22,Faction.ALIEN, firstX, firstY);
	}
	public TestBigSpider(Map map,Astar a,List<IAControlledCharacter> list, float firstX, float firstY) {
		super(new FastAlienSplitWeapon(),map, a,list,20,Faction.ALIEN, firstX, firstY);
		this.astar=a;
		this.list=list;
	}
	
	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) 
	{
		dm.drawCharacterUsingMetrics(this, Metrics2D.BIGSPIDER, g);
	}
	@Override
	public float getSize() {
		return 1.6f;
	}
	@Override
	protected boolean collidesWith(Entity e) {
		if(notCollidableChildren.contains(e)) return false;
		return true;
	}
	@Override
	protected void updatePre2(float tEl, Level l) {
		super.updatePre2(tEl, l);

		if(spawning){
			this.spawnCont-=tEl;
			if(Math.random()<3*tEl) l.addUpperAnimation(new DefaultAnimation(this.getX()+(float)(Math.random()*0.95f)+(this.getSize()-0.95f)/2,this.getY()+(float)(Math.random()*0.95f)+(this.getSize()-0.95f)/2,AnimationMetrics2D.BLOODSPIDER,true));
			if(!this.hasSpawned&&this.spawnCont<SPAWN_DURATION/4){
				this.hasSpawned=true;
				IAControlledCharacter ts=EntitySpawner.spawnCharacter(CHARACTER_TYPE.SPIDER,this.currentMap,this.astar,this.list,this.getX()+(this.getSize()-0.95f)/2,this.getY()+(this.getSize()-0.95f)/2,0,false);
				this.notCollidableChildren.add(ts);
				l.spawnCharacter(ts);
			}
			if(this.spawnCont<=0){
				this.spawning=false;
				this.spawnCooldown=SPAWN_COOLDOWN;
			}
		}
		else{
			if(this.spawnCooldown>0)this.spawnCooldown-=tEl;
			else
			{
				if(Math.random()<SPAWN_PROB*tEl){
					this.spawning=true;
					this.hasSpawned=false;
					this.spawnCont=SPAWN_DURATION;
				}
			}
		}
		
		Iterator<IAControlledCharacter> it=this.notCollidableChildren.iterator();
		while(it.hasNext())
		{
			IAControlledCharacter ts=it.next();
			if(!this.isCollidingWith(ts,this.getX(),this.getY(),false)){
				it.remove();
			}
		}
	}
	@Override
	public IAControlledCharacter onDeath(Level l){
		AudioManager.getInstance().playSound(Sound.BIGSPIDER_DEATH);
		l.addAnimation(new SpawningDeathAnimation(this.getX(),this.getY(),this.getSize(),this.getLookingDirection(),0.15f,2f,0.5f,EntitySpawner.CHARACTER_TYPE.SPIDER,this.getX()-0.47f+(this.getSize()/2),this.getY()-0.47f+(this.getSize()/2),Metrics2D.BIGSPIDER));
		return null;
	}
	@Override
	protected void onShootReceivedAt(float x, float y, Level l) 
	{
		l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.BLOODSPIDER,true));
		
	}
	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		if(this.spawning) return 0.5f;
		return 2f;
	}
}
