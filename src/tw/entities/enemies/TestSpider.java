package tw.entities.enemies;

import java.awt.Graphics2D;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.entities.IAControlledCharacter;
import tw.entities.TestCharacter;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DeathAnimation;
import tw.gui.animation.DefaultAnimation;
import tw.weapons.AlienSplitWeapon;
import tw.weapons.BiteWeapon;

public class TestSpider extends TestCharacter{
	
	private IAControlledCharacter mutation=null;
	public TestSpider(Map map, float firstX, float firstY) {
		super(new AlienSplitWeapon(),new BiteWeapon(),map, 3,Faction.ALIEN, firstX, firstY);
	}
	public TestSpider(Map map,Astar a,List<IAControlledCharacter> list, float firstX, float firstY) {
		super(new AlienSplitWeapon(),new BiteWeapon(),map,a,list,3,Faction.ALIEN, firstX, firstY);
	}
	
	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) 
	{
		dm.drawCharacterUsingMetrics(this, Metrics2D.SPIDER, g);
	}
	@Override
	public float getSize() {
		return 0.95f;
	}
	@Override
	public IAControlledCharacter onDeath(Level l){
		//AudioManager.getInstance().playSound(Sound.SOUND_BIGSPIDER_DEATH);
		l.addAnimation(new DeathAnimation(this.getX(),this.getY(),this.getSize(),this.getLookingDirection(),0.12f,0.5f,Metrics2D.SPIDER));
		return this.mutation;
	}
	
	public void mutateTo(IAControlledCharacter tbs)
	{
		this.mutation=tbs;
		this.setLife(-1);
	}
	@Override
	protected void onShootReceivedAt(float x, float y, Level l) 
	{
		l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.BLOODSPIDER,true));
		
	}
	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		return 2.4f;
	}
}
