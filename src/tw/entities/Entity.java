package tw.entities;


import tw.entities.Character.Direction;
import tw.game.Map;
import tw.utils.Cleanable;
import tw.utils.Coordinate;
import tw.utils.Drawable2D;

public abstract class Entity implements Cleanable, Drawable2D
{
	private float xpos;
	private float ypos;
	
	private boolean registeredToMap=false;
	
	protected Map currentMap;
	
	public Entity(Map map,float firstX,float firstY)
	{
		this.currentMap=map;
		this.xpos=firstX;
		this.ypos=firstY;
	}
	
	/**
	 * MUST BE CALLED WHEN ENTITY IS ADDED TO THE GAME
	 */
	public void registerEntityToMap()
	{
		this.currentMap.registerEntityFirstMovement(this);
		this.registeredToMap=true;
	}
	public void registerEntityToMap(float ix,float iy)
	{
		this.xpos=ix;
		this.ypos=iy;
		this.registerEntityToMap();
	}

	/**
	 * Sets a new XY coordinates for this entity
	 */
	public final void setXY(float xpos,float ypos)
	{
		float lastx=this.getX();
		float lasty=this.getY();
		this.xpos=xpos;
		this.ypos=ypos;
		if(this.registeredToMap) this.currentMap.registerEntityMovement(this,lastx,lasty);
	}
	
	//***** GETTERS AND CLEANUP METHODS *****//
	
	/**
	 * Gets the entity y pos (Normalized scale, 1 tile 1 unit)
	 */
	public float getY() {
		return ypos;
	}
	
	/**
	 * Gets the entity x pos (Normalized scale, 1 tile 1 unit)
	 */
	public float getX() {
		return this.xpos;
	}
	
	/**
	 * Returns the entity size, in normalized scale (1 tile, 1 unit)
	 */
	public abstract float getSize();
	
	/**
	 * Returns the entity type
	 */
	public abstract EntityType getType();
	
	/**
	 * Lets all children of this class implement a concrete cleanup method for them, but forcing clean() to be used for that
	 */
	protected abstract void concreteClean();
	
	/**
	 * Cleans entity from the game
	 */
	public final void clean()
	{
		this.concreteClean();
		this.currentMap.unregisterEntityMovement(this);
		this.registeredToMap=false;
	}
	protected final Coordinate getCollisionPointWith(Entity e,Direction d,float currentX,float currentY,boolean checkCollides){
		if(this.isCollidingWith(e,currentX,currentY,checkCollides))
		{
			switch(d)
			{
			case UP:
				return new Coordinate(currentX,e.getY()+e.getSize());
			case DOWN:
				return new Coordinate(currentX,e.getY()-this.getSize());
			case RIGHT:
				return new Coordinate(e.getX()-this.getSize(),currentY);
			case LEFT:
				return new Coordinate(e.getX()+e.getSize(),currentY);
			case DOWNLEFT:
				break;
			case DOWNRIGHT:
				break;
			case UPLEFT:
				break;
			case UPRIGHT:
				break;
			default:
				break;
			}
		}
		return null;
	}
	public final boolean isCollidingWith(Entity e,float currentX,float currentY,boolean checkCollides)
	{
		if(e.equals(this)) return false;
		
		if(	currentX<e.getX()+e.getSize()	&&
			currentX+this.getSize()>e.getX()	&&
			currentY<e.getY()+e.getSize()	&&
			currentY+this.getSize()>e.getY()	) 
		{
			return !checkCollides || this.collidesWith(e)||e.collidesWith(this);
		}
		
		return false;
	}
	public final boolean isCollidingWith(Entity e,Direction d,float currentX,float currentY)
	{
		if(e.equals(this)) return false;
		
		switch(d)
		{
		case UP:
			if(	currentX<e.getX()+e.getSize()	&&
					currentX+this.getSize()>e.getX()	&&
					currentY<e.getY()+e.getSize()	&&
					currentY>e.getY()	) 
				{
					return this.collidesWith(e)||e.collidesWith(this);
				}
			break;
		case DOWN:
			if(	currentX<e.getX()+e.getSize()	&&
					currentX+this.getSize()>e.getX()	&&
					currentY<e.getY()+e.getSize()	&&
					currentY+this.getSize()>e.getY()	) 
				{
					return this.collidesWith(e)||e.collidesWith(this);
				}
			break;
			case RIGHT:
				if(	currentX<e.getX()+e.getSize()	&&
						currentX+this.getSize()>e.getX()	&&
						currentY<e.getY()+e.getSize()	&&
						currentY+this.getSize()>e.getY()	) 
					{
						return this.collidesWith(e)||e.collidesWith(this);
					}
				break;
			case LEFT:
				if(	currentX<e.getX()+e.getSize()	&&
						currentX>e.getX()	&&
						currentY<e.getY()+e.getSize()	&&
						currentY+this.getSize()>e.getY()	) 
					{
						return this.collidesWith(e)||e.collidesWith(this);
					}
				break;
		default:
			break;
		}
		
		return false;
	}
	protected abstract boolean isCollidable();
	protected abstract boolean collidesWith(Entity e);
	
}
