package tw.entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import tw.IA.Util.Astar.Astar;
import tw.IA.Util.Astar.Nodo;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;

public abstract class IAControlledCharacter extends Character
{
	private boolean iaActive=true;
	private CollisionCode currentCollisionCode=null;
	private Direction objDir=null;
	private float xobj;
	private float yobj;
	private float internalCounter=0;
	
	//IA
	private Astar astar;
	private ArrayList<Nodo> path;
	private Rectangle Vision;
	private int Vision_Width=1;
	private int Vision_Height=200;
	private CharacterStatModifier activeModifier=null;
	
	public IAControlledCharacter(int life,Map map, float firstX, float firstY) {
		super(life,map, firstX, firstY);
		this.astar=null;
		this.Vision=new Rectangle((int)this.getX()*30,(int) this.getY()*30, this.Vision_Width,this.Vision_Height);
		// TODO Auto-generated constructor stub
	}
	
	public IAControlledCharacter(int life,Map map,Astar astar, float firstX, float firstY) {
		super(life,map, firstX, firstY);
		this.astar=astar;
		/*
		 * Vision range
		 */
		//for small entities
		if((int)this.getSize()==0){
			 this.Vision_Width=2;
			 this.Vision_Height=200;
		}
		//for big entities
		else{
			this.Vision_Width=10;
			this.Vision_Height=400;
		}
		this.Vision=new Rectangle((int)this.getX()*30,(int) this.getY()*30, this.Vision_Width,this.Vision_Height);
		//PlaningPath(15,25);		
		// TODO Auto-generated constructor stub
	}
	
	public final void update(float tEl,Level l)
	{
		this.internalCounter+=tEl;
		if(this.activeModifier!=null) {
			if(this.activeModifier.update(tEl,l,this)) this.activeModifier=null;
		}
		updatePre(tEl,l);
		if(this.activeModifier!=null&&this.activeModifier.getDirectionLock()!=null){
			this.currentCollisionCode=this.move(this.activeModifier.getDirectionLock(), l, tEl);
		}
		else if(this.objDir!=null){
			this.currentCollisionCode=this.moveWithLimits(this.objDir, xobj, yobj,l, tEl);
		}
	}
	protected abstract void updatePre(float tEl,Level l);
	protected CollisionCode getCurrentCollisionCode()
	{
		return this.currentCollisionCode;
	}
	public void setIA(boolean active)
	{
		this.iaActive=active;
	}
	public boolean isIAActive()
	{
		return this.iaActive;
	}
	
	public Direction getObjDir()
	{
		if(this.activeModifier!=null&&this.activeModifier.getDirectionLock()!=null) return this.activeModifier.getDirectionLock();
		return this.objDir;
	}
	
	public void setMovementObj(Direction d)
	{
		if(d==null){
			this.objDir=null;
			return;
		}
		float limx=this.getX();
		float limy=this.getY();
		switch(d)
		{
		case UP:
			limy=-1;
			break;
		case DOWN:
			limy=Float.MAX_VALUE;
			break;
		case LEFT:
			limx=-1;
			break;
		case RIGHT:
			limx=Float.MAX_VALUE;
			break;
		case UPRIGHT:
			limx=Float.MAX_VALUE;
			limy=-1;
			break;
		case UPLEFT:
			limy=-1;
			limx=-1;
			break;
		case DOWNRIGHT:
			limy=Float.MAX_VALUE;
			limx=Float.MAX_VALUE;
			break;
		case DOWNLEFT:
			limy=Float.MAX_VALUE;
			limx=-1;
			break;
		}
		
		this.setMovementObj(d, limx, limy);
	}
	
	/**
	 * go to position -----
	 * @param xobj
	 * @param yobj
	 */
	public void setMovementObj(float xobj,float yobj)
	{
		Direction d = null;
		if (xobj > this.getX()) {
			if (yobj > this.getY())
				d = Direction.DOWNRIGHT;
			else if (yobj < this.getY())
				d = Direction.UPRIGHT;
			else
				d = Direction.RIGHT;
		} else if (xobj < this.getX()) {
			if (yobj > this.getY())
				d = Direction.DOWNLEFT;
			else if (yobj < this.getY())
				d = Direction.UPLEFT;
			else
				d = Direction.LEFT;
		} else if (yobj > this.getY())
			d = Direction.DOWN;
		else if (yobj < this.getY())
			d = Direction.UP;
		
		this.setMovementObj(d, xobj, yobj);
		this.VisionDirection(d);
	}
	public void setMovementObj(Direction d,float xobj,float yobj)
	{
		this.objDir=d;
		this.xobj=xobj;
		this.yobj=yobj;
	}
	
	public float getInternalCounter()
	{
		return this.internalCounter;
	}
	
	public boolean setStatModifier(CharacterStatModifier csm)
	{
		return this.setStatModifier(csm,false);
	}
	public boolean setStatModifier(CharacterStatModifier csm,boolean forceOverride)
	{
		if(this.activeModifier==null||forceOverride)
		{
			this.activeModifier=csm;
			return true;
		}
		return false;
	}
	
	public void think()
	{
		if(this.iaActive) concreteThink();
	}
	public abstract void concreteThink();
	
	@Override
	public final float getVel() {
		if(this.activeModifier!=null) return this.activeModifier.getSpeedModifier()*getConcreteVel();
		else return this.getConcreteVel();
	}
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) 
	{
		this.concreteDraw2D(dm,g,tEl);
		if(this.activeModifier!=null) this.activeModifier.draw2D(dm, g, tEl);
	}
	/*@Override
	public CollisionCode moveWithLimits(Direction d,float limx,float limy,Level l,float tEl){
		Direction dir=d;
		if(this.activeModifier!=null&&this.activeModifier.getDirectionLock()!=null){
			dir=this.activeModifier.getDirectionLock();
		}
		return super.moveWithLimits(dir, limx, limy, l, tEl);
	}*/
	@Override
	public Direction getLookingDirection()
	{
		if(this.activeModifier!=null&&this.activeModifier.getDirectionLock()!=null){
			return this.activeModifier.getDirectionLock();
		}
		return super.getLookingDirection();
	}
	public abstract float getConcreteVel();
	public abstract void concreteDraw2D(DrawingManager2D dm, Graphics2D g, float tEl); 
	
	
	/**
	 * IA PART
	 */
	public void PlaningPath(int x_tile,int y_tile){
		this.path=this.astar.calcularCamino
			(this.astar.GetNodeMatrix()[(int) this.getX()][(int) this.getY()],this.astar.GetNodeMatrix()[x_tile][y_tile], true);
	}
	public ArrayList<Nodo> GetPath(){
		return this.path;
	}
	public float GetXAstardestiniy(){
		return this.path.get(0).GetCenterX();
	}
	
	public float GetYAstardestiniy(){
		return this.path.get(0).GetCenterY();
	}
	public boolean ExistPath(){
		return this.path!=null;
	}
	
	public boolean NextPathStep(){
		if(this.path.size()>1) {
			this.path.remove(0);
			return true;
		}
		else return false;
	}
	
	
	public Rectangle GetVision(){
		return this.Vision;
	}
	
	
	public void VisionDirection(Direction d){
		switch(d){
		case DOWN:
			this.Vision.setBounds((int)(this.getX()*30)+15,(int) this.getY()*30, this.Vision_Width, this.Vision_Height);
			break;
		case DOWNRIGHT:			
			this.Vision.setBounds((int)this.getX()*30+this.Vision_Width,(int) this.getY()*30+this.Vision_Width,
					this.Vision_Height/2,this.Vision_Height/2);
			break;
		case DOWNLEFT:
			this.Vision.setBounds((int)this.getX()*30-this.Vision_Height,
					(int) this.getY()*30+this.Vision_Width, this.Vision_Height/2, this.Vision_Height/2);
			break;
		case UP:
			this.Vision.setBounds((int)(this.getX()*30)+15,(int) this.getY()*30-this.Vision_Height+this.Vision_Width, this.Vision_Width, this.Vision_Height);
			break;
		case UPRIGHT:
			this.Vision.setBounds((int)this.getX()*30+this.Vision_Width,(int) this.getY()*30-this.Vision_Height,
					this.Vision_Height/2, this.Vision_Height/2);
			break;
		case UPLEFT:
			this.Vision.setBounds((int)this.getX()*30-this.Vision_Height+15,
					(int) this.getY()*30-this.Vision_Height, this.Vision_Height/2, this.Vision_Height/2);		
			break;
		case RIGHT:
			this.Vision.setBounds((int)this.getX()*30+this.Vision_Height,(int) (this.getY()*30)+15, this.Vision_Height, this.Vision_Width);
			break;
		case LEFT:
			this.Vision.setBounds((int)this.getX()*30-this.Vision_Height,(int) this.getY()*30+15, 
					this.Vision_Height, this.Vision_Width);
			break;
		default:
			break;		
		}
	}
	
	
	public Rectangle GetContornOf(Entity e)
	{
		 return new Rectangle((int)(e.getX()*30), (int)(e.getY()*30),
				 (int)(e.getSize()*30), (int)(e.getSize()*30));
	}
	
	
	
	
	
	
}
