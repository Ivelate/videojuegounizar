package tw.entities;

import java.util.List;



import tw.game.Level;
import tw.game.Map;
import tw.IA.Util.Astar.Astar;
import tw.entities.EntityType.Faction;
import tw.entities.bullet.AlienSplitBullet;
import tw.entities.bullet.FireBullet;
import tw.entities.bullet.MachinegunBullet;
import tw.entities.bullet.PlasmaBullet;
import tw.entities.consumables.FireWeaponConsumable;
import tw.entities.consumables.GrenadeAmmoConsumable;
import tw.entities.consumables.MutationSpiderPlant;
import tw.entities.consumables.NormalWeaponConsumable;
import tw.entities.consumables.PlasmaWeaponConsumable;
import tw.entities.consumables.ReinforcementBronze;
import tw.entities.consumables.ReinforcementGold;
import tw.entities.consumables.ReinforcementSilver;
import tw.entities.consumables.ReinforcementSuper;
import tw.entities.consumables.ShotgunConsumable;
import tw.entities.enemies.*;
import tw.entities.allies.*;

public class EntitySpawner 
{
	public static enum CHARACTER_TYPE{SPIDER,BIGSPIDER,SOLDIER,TANK,TESTPLAYABLE,SPIDER_NEST};
	public static enum POWERUP_TYPE{WEAPON_SHOTGUN,WEAPON_NORMAL,WEAPON_PLASMA,WEAPON_FIRE,AMMO_GRENADE,SPIDERPLANT,REINFORCEMENT_BRONZE,REINFORCEMENT_SILVER,REINFORCEMENT_GOLD,REINFORCEMENT_SUPER};
	public static enum BULLET_TYPE{MACHINEGUN,PLASMA,SPIDER_SPLIT,FIRE};
	
	
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Map map,float locx,float locy,float diffDistanceAccepted,boolean avoidCollisions){
		boolean pilotAdded=false;
		IAControlledCharacter toRet=null;
		switch(type)
		{
		case SPIDER:
			toRet=new TestSpider(map,locx,locy);
			break;
		case BIGSPIDER:
			toRet=new TestBigSpider(map,locx,locy);
			break;
		case SOLDIER:
			toRet=new TestSoldier(map,locx,locy);
			break;
		case TANK:
			toRet=new TestTank(pilot,map,locx,locy);
			pilotAdded=true;
			break;
		case TESTPLAYABLE:
			toRet=new TestPlayableCharacter(map,locx,locy);
			break;
		case SPIDER_NEST:
			toRet=new SpiderNest(map,locx,locy);
			break;
		}
		if(pilot!=null){
			pilot.clean();
		}
		if(avoidCollisions){
			toRet.relocateAvoidingCollisions();
			if(getDistDiff(toRet,locx,locy)>diffDistanceAccepted) return null;
		}
		return toRet;
	}
	
	
	//with Astar parameter
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Astar astar,Map map,List<IAControlledCharacter> list
			,float locx,float locy,float diffDistanceAccepted,boolean avoidCollisions){
		@SuppressWarnings("unused")
		boolean pilotAdded=false;
		IAControlledCharacter toRet=null;
		switch(type)
		{
		case SPIDER:
			toRet=new TestSpider(map,astar,list,locx,locy);
			break;
		case BIGSPIDER:
			toRet=new TestBigSpider(map,astar,list,locx,locy);
			break;
		case SOLDIER:
			toRet=new TestSoldier(map,astar,list,locx,locy);
			break;
		case TANK:
			toRet=new TestTank(pilot,map,astar,list,locx,locy);
			pilotAdded=true;
			break;
		case TESTPLAYABLE:
			toRet=new TestPlayableCharacter(map,locx,locy);
			break;
		case SPIDER_NEST:
			toRet=new SpiderNest(map,locx,locy);
			break;
		}
		if(pilot!=null){
			pilot.clean();
		}
		if(avoidCollisions){
			toRet.relocateAvoidingCollisions();
			if(getDistDiff(toRet,locx,locy)>diffDistanceAccepted) return null;
		}
		return toRet;
	}
	
	
	
	
	public static IAControlledCharacter spawnCharacter(CHARACTER_TYPE type,Map map,float locx,float locy,float diffDistanceAccepted,boolean avoidCollisions){
		return EntitySpawner.spawnCharacter(null,type, map, locx, locy,diffDistanceAccepted,avoidCollisions);
	}
	
	public static IAControlledCharacter spawnCharacter(CHARACTER_TYPE type,Map map,Astar a,List<IAControlledCharacter> ch_list,float locx,float locy,float diffDistanceAccepted,boolean avoidCollisions){
		return EntitySpawner.spawnCharacter(null,type,a,map,ch_list, locx, locy,diffDistanceAccepted,avoidCollisions);
	}
	
	
	
	
	
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Map map,float locx,float locy){
		return spawnCharacter(pilot,type,map,locx,locy,0.01f);
	}
	
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Map map,Astar a
			,List<IAControlledCharacter> ch_list,float locx,float locy){
		return spawnCharacter(pilot,type,map,a,ch_list,locx,locy,0.01f);
	}
	public static IAControlledCharacter spawnCharacter(CHARACTER_TYPE type,Map map,float locx,float locy){
		return spawnCharacter(null,type,map,locx,locy);
	}
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Map map,float locx,float locy,float distDistanceAccepted){
		return spawnCharacter(pilot,type,map,locx,locy,distDistanceAccepted,true);
	}
	public static IAControlledCharacter spawnCharacter(IAControlledCharacter pilot,CHARACTER_TYPE type,Map map,Astar a,
			List<IAControlledCharacter> list,float locx,float locy,float distDistanceAccepted){
		return spawnCharacter(pilot,type,a,map,list,locx,locy,distDistanceAccepted,true);
	}
	public static IAControlledCharacter spawnCharacter(CHARACTER_TYPE type,Map map,float locx,float locy,float distDistanceAccepted){
		return spawnCharacter(null,type,map,locx,locy,distDistanceAccepted);
	}
	private static float getDistDiff(Entity e,float ix,float iy)
	{
		return (float) Math.sqrt((e.getX()-ix)*(e.getX()-ix) + (e.getY()-iy)*(e.getY()-iy));
	}
	public static Consumable spawnPowerup(POWERUP_TYPE type,Map map,float locx,float locy){
		Consumable toRet=null;
		switch(type)
		{
		case WEAPON_SHOTGUN:
			toRet=new ShotgunConsumable(map,locx,locy);
			break;
		case WEAPON_NORMAL:
			toRet=new NormalWeaponConsumable(map,locx,locy);
			break;
		case WEAPON_PLASMA:
			toRet=new PlasmaWeaponConsumable(map,locx,locy);
			break;
		case WEAPON_FIRE:
			toRet=new FireWeaponConsumable(map,locx,locy);
			break;
		case SPIDERPLANT:
			toRet=new MutationSpiderPlant(map,locx,locy);
			break;
		case AMMO_GRENADE:
			toRet=new GrenadeAmmoConsumable(map,locx,locy);
			break;
		case REINFORCEMENT_BRONZE:
			toRet=new ReinforcementBronze(60,map,locx,locy);
			break;
		case REINFORCEMENT_SILVER:
			toRet=new ReinforcementSilver(60,map,locx,locy);
			break;
		case REINFORCEMENT_GOLD:
			toRet=new ReinforcementGold(60,map,locx,locy);
			break;
		case REINFORCEMENT_SUPER:
			toRet=new ReinforcementSuper(60,map,locx,locy);
			break;
		}
		return toRet;
	}
	
	public static Consumable spawnPowerup(POWERUP_TYPE type,Map map,Astar a,List<IAControlledCharacter> list,float locx,float locy){
		Consumable toRet=null;
		switch(type)
		{
		case WEAPON_SHOTGUN:
			toRet=new ShotgunConsumable(map,locx,locy);
			break;
		case WEAPON_NORMAL:
			toRet=new NormalWeaponConsumable(map,locx,locy);
			break;
		case WEAPON_PLASMA:
			toRet=new PlasmaWeaponConsumable(map,locx,locy);
			break;
		case WEAPON_FIRE:
			toRet=new FireWeaponConsumable(map,locx,locy);
			break;
		case SPIDERPLANT:
			toRet=new MutationSpiderPlant(map,a,list,locx,locy);
			break;
		case AMMO_GRENADE:
			toRet=new GrenadeAmmoConsumable(map,locx,locy);
			break;
		case REINFORCEMENT_BRONZE:
			toRet=new ReinforcementBronze(60,map,locx,locy);
			break;
		case REINFORCEMENT_SILVER:
			toRet=new ReinforcementSilver(60,map,locx,locy);
			break;
		case REINFORCEMENT_GOLD:
			toRet=new ReinforcementGold(60,map,locx,locy);
			break;
		case REINFORCEMENT_SUPER:
			toRet=new ReinforcementSuper(60,map,locx,locy);
			break;
		}
		return toRet;
	}
	
	
	
	
	
	
	public static Bullet spawnBullet(BULLET_TYPE type,Map map,float locx,float locy,float vx,float vy,Faction f,Level l){
		Bullet toRet=null;
		switch(type)
		{
		case MACHINEGUN:
			toRet=new MachinegunBullet(locx,locy,vx,vy,f,map,l);
			break;
		case PLASMA:
			toRet=new PlasmaBullet(locx,locy,vx,vy,f,map,l);
			break;
		case SPIDER_SPLIT:
			toRet=new AlienSplitBullet(locx,locy,vx,vy,f,map,l);
			break;
		case FIRE:
			toRet=new FireBullet(locx,locy,vx,vy,f,map,l);
			break;
		}
		return toRet;
	}
}
