package tw.entities;

import java.awt.Graphics2D;

import tw.entities.Character.Direction;
import tw.game.Level;
import tw.gui.DrawingManager2D;
import tw.utils.Drawable2D;

public abstract class CharacterStatModifier implements Drawable2D
{
	protected float maxLifetime;
	protected float lifetime;
	public CharacterStatModifier(float maxLifetime)
	{
		this.lifetime=0;
		this.maxLifetime=maxLifetime;
	}
	public abstract float getSpeedModifier();
	public abstract Direction getDirectionLock();
	public boolean update(float tEl,Level l,IAControlledCharacter modifiedChar){
		concreteUpdate(tEl,l,modifiedChar);
		this.lifetime+=tEl;
		return this.lifetime>=this.maxLifetime;
	}
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) 
	{
		//AUTO OVERRIDE IF NEEDED
	}
	public void concreteUpdate(float tEl,Level l,IAControlledCharacter modifiedChar)
	{
		//AUTO OVERRIDE IF NEEDED
	}
}
