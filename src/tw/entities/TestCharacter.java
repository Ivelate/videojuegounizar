package tw.entities;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.List;
import java.util.Random;

import tw.IA.Util.Astar.Astar;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.game.Player;
import tw.gui.DrawingManager2D;
import tw.weapons.BasicBulletWeapon;
import tw.weapons.Weapon;

public class TestCharacter extends ShooterCharacter {

	private float xobj;
	private float yobj;
	private float changeCourseTimer = 0;
	private Faction faction;
	private int Shot_Times;
	private List<IAControlledCharacter> CharacterPositions;

	private enum State {
		SEARCHING, PATCHING, SHOTING
	};

	private Astar astar;

	private State currentState;

	public TestCharacter(Map map, int life, Faction faction, float firstX,
			float firstY) {
		super(new BasicBulletWeapon(), life, map, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
	}

	// con A*
	public TestCharacter(Map map, Astar astar,
			List<IAControlledCharacter> list, int life, Faction faction,
			float firstX, float firstY) {
		super(new BasicBulletWeapon(), life, map, astar, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.CharacterPositions = list;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
		this.astar = astar;
	}

	public TestCharacter(Weapon w, Map map, int life, Faction faction,
			float firstX, float firstY) {
		super(w, life, map, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.CharacterPositions = null;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
	}

	public TestCharacter(Weapon w, Map map, Astar a,
			List<IAControlledCharacter> list, int life, Faction faction,
			float firstX, float firstY) {
		super(w, life, map, a, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.CharacterPositions = list;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
		this.astar = a;
	}

	public TestCharacter(Weapon w, Weapon w2, Map map, int life,
			Faction faction, float firstX, float firstY) {
		super(w, w2, life, map, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
	}

	public TestCharacter(Weapon w, Weapon w2, Map map, Astar a,
			List<IAControlledCharacter> list, int life, Faction faction,
			float firstX, float firstY) {
		super(w, w2, life, map, a, firstX, firstY);
		this.setShootingTo(false);
		this.faction = faction;
		this.CharacterPositions = list;
		this.currentState = State.SEARCHING;
		this.Shot_Times = 0;
		this.astar = a;

	}

	@Override
	public float getSize() {
		return 1.9f;
	}

	@Override
	protected void concreteClean() {

	}

	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		/*
		 * if(this.getFaction()==Faction.MARINE) dm.drawSoldier(this, g); else
		 * dm.drawBigSpider(this, g); //TODO TEST
		 */
		dm.drawContornOf(this, g);

	}

	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		return 3.5f;
	}

	@Override
	public EntityType getType() {
		return new EntityType(EntityType.Role.GROUND, this.faction);
	}

	@Override
	protected boolean collidesWith(Entity e) {
		// if(e.getType().getFaction()!=this.getType().getFaction()) return
		// true;
		// else return false;
		return false;
	}

	@Override
	public boolean occludesSameFactionBullets() {
		return false;
	}

	@Override
	public void concreteThink() {
		this.setMovementObj(0, 0);
	}

	@Override
	public boolean canBeShootedConcrete() {
		return true;
	}

	@Override
	public IAControlledCharacter onDeath(Level l) {
		return null;
	}

	@Override
	protected void updatePre2(float tEl, Level l) {
		if (!this.isIAActive())
			return;
		IA(tEl, l);
		if (this.GetPath() == null)
			return;

		// else this.setShootingTo(true);
		// this.setMovementObj(this.xobj, this.yobj);
	}

	/**
	 * IA VICTOR (Posible uso si no se usa AStar)
	 */

	protected void Recalculate(float tEl, Level l) {

		if (!this.isIAActive())
			return;

		if (this.getCurrentCollisionCode() == null) {
			this.setShootingTo(true);
			this.xobj = (float) (Math.random() * this.currentMap.getWidth());
			this.yobj = (float) (Math.random() * this.currentMap.getHeight());
			this.setMovementObj(this.xobj, this.yobj);
			this.changeCourseTimer = 2f;
		} else if (this.getCurrentCollisionCode() == CollisionCode.WITH_LIMIT) {
			if (this.getX() == this.xobj && this.getY() == this.yobj) {
				this.setShootingTo(true);
				this.xobj = (float) (Math.random() * this.currentMap.getWidth());
				this.yobj = (float) (Math.random() * this.currentMap
						.getHeight());
			} else
				this.setMovementObj(this.xobj, this.yobj);
		} else if (this.getCurrentCollisionCode() == CollisionCode.WITH_ENTITY
				|| this.getCurrentCollisionCode() == CollisionCode.WITH_WALL) {
			if (this.getCurrentCollisionCode() == CollisionCode.WITH_WALL)
				this.changeCourseTimer -= tEl;
			if (this.changeCourseTimer < 0) {
				this.setShootingTo(true);
				this.xobj = (float) (Math.random() * this.currentMap.getWidth());
				this.yobj = (float) (Math.random() * this.currentMap
						.getHeight());
				this.setMovementObj(this.xobj, this.yobj);
				this.changeCourseTimer = 2f;
			}
		} else
			this.currentState = State.SEARCHING;
	}

	@Override
	protected void onShoot() {
		// AudioManager.getInstance().playSound(Sound.SOUND_SHOOT);
	}

	@Override
	protected void onShootReceivedAt(float x, float y, Level l) {
		// TODO Auto-generated method stub

	}

	private void IA(float tEl, Level l) {
		switch (this.currentState) {
		case SEARCHING:
			// Find an enemy
			this.setShootingTo(false);
			if (this.CharacterPositions != null) {
				Random n = new Random();
				int xdest = n.nextInt(34);
				int ydest = n.nextInt(25);
				// Make route
				if (this.astar.existNode(xdest, ydest)) {
					PlaningPath((int) xdest, (int) ydest);
					this.currentState = State.PATCHING;
				}
			}

			break;
		case PATCHING:
			Shot_Times = 0;
			if (this.ExistPath()) {
				if ((int) this.getSize() == 0
						&& Math.abs(this.getX() - this.GetXAstardestiniy()) == 0
						&& Math.abs(this.getY() - this.GetYAstardestiniy()) == 0) {

					if (this.NextPathStep()) {
						this.xobj = this.GetXAstardestiniy();
						this.yobj = this.GetYAstardestiniy();
					} else
						this.currentState = State.SEARCHING;
				} else if ((int) this.getSize() > 0
						&& Math.abs(this.getX() - this.GetXAstardestiniy()) < 0.5
						&& Math.abs(this.getY() - this.GetYAstardestiniy()) < 0.5) {

					if (this.NextPathStep()) {
						this.xobj = this.GetXAstardestiniy();
						this.yobj = this.GetYAstardestiniy();
					} else
						this.currentState = State.SEARCHING;
				}

				if (this.getCurrentCollisionCode() == null) {
					this.setMovementObj(this.xobj, this.yobj);
					this.changeCourseTimer = 2f;

				} else if (this.getCurrentCollisionCode() == CollisionCode.WITH_LIMIT) {
					if (this.getX() == this.xobj && this.getY() == this.yobj)
						Recalculate(tEl, l);
					else
						this.setMovementObj(this.xobj, this.yobj);
				} else if (this.getCurrentCollisionCode() == CollisionCode.WITH_ENTITY
						|| this.getCurrentCollisionCode() == CollisionCode.WITH_WALL) {
					this.changeCourseTimer -= tEl;
					Recalculate(tEl, l);
					if (this.changeCourseTimer < 0) {
						this.setMovementObj(this.xobj, this.yobj);
						this.changeCourseTimer = 2f;
					}
				}
				// Detect enemies
				this.VisionDirection(this.getLookingDirection());
				boolean enemy_find = false;
				if (this.getFaction() == Faction.ALIEN) {
					for (Player p : l.GetPlayers()) {
						if (p.getCharacter() != null
								&& this.GetVision().intersects(
										GetContornOf(p.getCharacter()))) {
							this.setShootingTo(true);
							enemy_find = true;
						}
					}
				}
				if (this.CharacterPositions != null) {
					for (IAControlledCharacter c : this.CharacterPositions) {
						if (this.getFaction() != c.getFaction()
								&& this.GetVision().getBounds()
										.intersects(c.GetContornOf(c))) {
							enemy_find = true;
						}
					}
				}
				if (enemy_find) {
					this.currentState = State.SHOTING;
				} else {
					this.currentState = State.PATCHING;
					this.setShootingTo(false);
				}

			} else
				this.currentState = State.SEARCHING;

			break;
		case SHOTING:
			if (Shot_Times < 10) {
				this.VisionDirection(this.getLookingDirection());
				boolean enemy_find = false;
				if (this.getFaction() == Faction.ALIEN) {
					for (Player p : l.GetPlayers()) {
						if (p.getCharacter() != null
								&& this.GetVision().intersects(
										GetContornOf(p.getCharacter()))) {
							enemy_find = true;
							this.setShootingTo(true);
						}
					}
				}
				if (this.CharacterPositions != null) {
					for (IAControlledCharacter c : this.CharacterPositions) {
						if (this.getFaction() != c.getFaction()
								&& this.GetVision().getBounds()
										.intersects(c.GetContornOf(c))) {
							enemy_find = true;
							this.setShootingTo(true);
							Shot_Times++;
						}
					}
				}
				if (!enemy_find)
					this.currentState = State.PATCHING;
			}
			else this.currentState = State.PATCHING;
			break;
		default:
			break;

		}
	}

}
