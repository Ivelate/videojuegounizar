package tw.entities.consumables;

import java.awt.Graphics2D;
import java.util.Random;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Character;
import tw.entities.Consumable;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;

public class ReinforcementBronze extends Consumable
{

	public ReinforcementBronze(float lifetime, Map map, float firstX,
			float firstY) {
		super(lifetime, map, firstX, firstY);
	}

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawAnimation((int)((this.lifetime/AnimationMetrics2D.REINFORCEMENTS_BRONZE.ANIMMOD)%AnimationMetrics2D.REINFORCEMENTS_BRONZE.FRAMESNUM),
				this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.REINFORCEMENTS_BRONZE, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(c instanceof ShooterCharacter)
		{
			return l.isPartOfPlayers((ShooterCharacter)c);
		}
		return false;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c.getFaction()==Faction.ALIEN) return;
		
		AudioManager.getInstance().playSound(Sound.REINFORCEMENTS_BRONZE);
		if(c.getFaction()==Faction.MARINE){
			Random n=new Random();
			int x=n.nextInt(4);
			int y=n.nextInt(33);
			l.spawnCharacter(CHARACTER_TYPE.TANK,x,y);
		}
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.REINFORCEMENTS_BRONZE.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	protected void concreteClean() {
		
	}

}
