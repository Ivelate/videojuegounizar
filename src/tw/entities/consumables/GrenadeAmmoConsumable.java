package tw.entities.consumables;

import java.awt.Graphics2D;

import tw.entities.Character;
import tw.entities.Consumable;
import tw.entities.Entity;
import tw.entities.EntityType;
import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.weapons.GrenadeThrower;
import tw.weapons.MachinegunWeapon;

public class GrenadeAmmoConsumable extends Consumable
{
	private static final float DEFAULT_LIFETIME=30;
	public GrenadeAmmoConsumable(Map map, float firstX,float firstY) {
		super(DEFAULT_LIFETIME, map, firstX, firstY);
	}

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawAnimation((int)((this.lifetime/AnimationMetrics2D.WEAPON_GRENADE.ANIMMOD)%AnimationMetrics2D.WEAPON_GRENADE.FRAMESNUM),
				this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.WEAPON_GRENADE, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(c instanceof ShooterCharacter && c.getFaction()!=Faction.ALIEN &&
				((ShooterCharacter) c).hasSecondaryWeapon() &&
				((ShooterCharacter) c).getSecondaryWeapon() instanceof GrenadeThrower &&
				((ShooterCharacter) c).getSecondaryWeapon().getMaxAmmo()>((ShooterCharacter) c).getSecondaryWeapon().getCurrentAmmo()) 
			return true;
		
		return false;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c instanceof ShooterCharacter) {
			ShooterCharacter sc=(ShooterCharacter) c;
			if(sc.getSecondaryWeapon()!=null) sc.getSecondaryWeapon().reloadAmmo(1);
		}
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.WEAPON_GRENADE.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	protected void concreteClean() {
		// TODO Auto-generated method stub
		
	}

	
	
}
