package tw.entities.consumables;

import java.awt.Graphics2D;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Character;
import tw.entities.CharacterPlaceholder;
import tw.entities.Consumable;
import tw.entities.EntitySpawner;
import tw.entities.IAControlledCharacter;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntityType.Faction;
import tw.entities.enemies.TestSpider;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.Animation;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;

public class MutationSpiderPlant extends Consumable
{
	private Astar a;
	private List<IAControlledCharacter> list;
	public MutationSpiderPlant(Map map, float firstX,
			float firstY) {
		super(Float.MAX_VALUE, map, firstX, firstY);
	}
	
	public MutationSpiderPlant(Map map,Astar a,List<IAControlledCharacter> list, float firstX,
			float firstY) {
		super(Float.MAX_VALUE, map, firstX, firstY);
		this.a=a;
		this.list=list;
	}
	

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		int frame=(int)(this.lifetime/AnimationMetrics2D.SPIDERPLANT.ANIMMOD);
		if(frame>=(AnimationMetrics2D.SPIDERPLANT.FRAMESNUM-2)) frame=AnimationMetrics2D.SPIDERPLANT.FRAMESNUM-3+(frame%3);
		dm.drawAnimation(frame,
				this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.SPIDERPLANT, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(this.lifetime<=AnimationMetrics2D.SPIDERPLANT.ANIMMOD*(AnimationMetrics2D.SPIDERPLANT.FRAMESNUM-3)) return false;
		
		if(c.getFaction()==Faction.ALIEN){
			if(c instanceof TestSpider) return true;
			else return false;
		}
		else return true;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c instanceof TestSpider){
			AudioManager.getInstance().playSound(Sound.BIGSPIDER_MUTATE);
			final float x=c.getX()+(c.getSize()-(Metrics2D.BIGSPIDER.SIZE/DrawingManager2D.TILE_SIZE))/2;
			final float y=c.getY()+(c.getSize()-(Metrics2D.BIGSPIDER.SIZE/DrawingManager2D.TILE_SIZE))/2;
			final Map map=this.currentMap;
			final CharacterPlaceholder mutation=new CharacterPlaceholder(EntitySpawner.spawnCharacter(CHARACTER_TYPE.BIGSPIDER, map,this.a,this.list,x,y,0,false),map){
				@Override
				public boolean canBeShootedConcrete() {
					return true;
				}
			};
			((TestSpider) c).mutateTo(mutation);
			l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.SPIDER_EVOLVE){
				@Override
				public void onDispose(Level l)
				{
					mutation.setLife(-1);
				}
			});
		}
		else{
			AudioManager.getInstance().playSound(Sound.PLANTCRUSH);
			final float x=this.getX();
			final float y=this.getY();
			final float size=this.getSize();
			l.addAnimation(new Animation(){
				@Override
				public void concreteUpdate(float tEl,Level l){}
				@Override
				public void onDispose(Level l){}
				@Override
				public boolean drawAnimation(DrawingManager2D dm,Graphics2D g,float tEl)
				{
					int frame=(int)(3-(2*this.getTotalTimeElapsed()/AnimationMetrics2D.SPIDERPLANT.ANIMMOD));
					if(frame<0) return true;
					dm.drawAnimation(frame, x, y, size, AnimationMetrics2D.SPIDERPLANT, g);
					return false;
				}
			});
		}
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.SPIDERPLANT.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	protected void concreteClean() {
		
	}

}
