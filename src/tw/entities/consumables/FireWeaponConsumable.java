package tw.entities.consumables;

import java.awt.Graphics2D;

import tw.entities.Character;
import tw.entities.Consumable;
import tw.entities.Entity;
import tw.entities.EntityType;
import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.weapons.FireWeapon;
import tw.weapons.MachinegunWeapon;

public class FireWeaponConsumable extends Consumable
{
	private static final float DEFAULT_LIFETIME=30;
	public FireWeaponConsumable(Map map, float firstX,float firstY) {
		super(DEFAULT_LIFETIME, map, firstX, firstY);
	}

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawAnimation((int)((this.lifetime/AnimationMetrics2D.WEAPON_FIRE.ANIMMOD)%AnimationMetrics2D.WEAPON_FIRE.FRAMESNUM),
				this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.WEAPON_FIRE, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(c instanceof ShooterCharacter && c.getFaction()!=Faction.ALIEN) return true;
		return false;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c instanceof ShooterCharacter && c.getFaction()!=Faction.ALIEN) {
			ShooterCharacter sc=(ShooterCharacter) c;
			sc.setWeapon(new FireWeapon());
		}
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.WEAPON_FIRE.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	protected void concreteClean() {
		// TODO Auto-generated method stub
		
	}

	
	
}
