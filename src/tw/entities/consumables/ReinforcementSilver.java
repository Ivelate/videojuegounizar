package tw.entities.consumables;

import java.awt.Graphics2D;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.Character;
import tw.entities.Consumable;
import tw.entities.ShooterCharacter;
import tw.entities.EntitySpawner.CHARACTER_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;

public class ReinforcementSilver extends Consumable
{

	public ReinforcementSilver(float lifetime, Map map, float firstX,
			float firstY) {
		super(lifetime, map, firstX, firstY);
	}

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawAnimation((int)((this.lifetime/AnimationMetrics2D.REINFORCEMENTS_SILVER.ANIMMOD)%AnimationMetrics2D.REINFORCEMENTS_SILVER.FRAMESNUM),
				this.getX(), this.getY(), this.getSize(), AnimationMetrics2D.REINFORCEMENTS_SILVER, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(c instanceof ShooterCharacter)
		{
			return l.isPartOfPlayers((ShooterCharacter)c);
		}
		return false;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c.getFaction()==Faction.ALIEN) return;
		
		AudioManager.getInstance().playSound(Sound.REINFORCEMENTS_SILVER);
		if(c.getFaction()==Faction.MARINE){
		for(int i=0;i<3;i++){
		float x=(float)1;
		float y=(float)7+i;
		l.spawnCharacter(CHARACTER_TYPE.SOLDIER,x,y);
		}
		}
		
	}

	@Override
	public float getSize() {
		return (float)(AnimationMetrics2D.REINFORCEMENTS_SILVER.SIZEX)/DrawingManager2D.TILE_SIZE;
	}

	@Override
	protected void concreteClean() {
		
	}

}
