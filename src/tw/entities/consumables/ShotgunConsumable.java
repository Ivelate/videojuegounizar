package tw.entities.consumables;

import java.awt.Graphics2D;

import tw.entities.Character;
import tw.entities.Consumable;
import tw.entities.Entity;
import tw.entities.EntityType;
import tw.entities.ShooterCharacter;
import tw.entities.TestPlayableCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.weapons.MachinegunWeapon;
import tw.weapons.Shotgun;

public class ShotgunConsumable extends Consumable
{
	private static final float DEFAULT_LIFETIME=60;
	public ShotgunConsumable(Map map, float firstX,float firstY) {
		super(DEFAULT_LIFETIME, map, firstX, firstY);
	}

	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawContornOf(this, g);
	}

	@Override
	public boolean canBeConsumedBy(Character c,Level l) {
		if(c instanceof TestPlayableCharacter) return true;
		return false;
	}

	@Override
	protected void performConsumptionEffects(Character c,Level l) {
		if(c instanceof TestPlayableCharacter) {
			ShooterCharacter sc=(ShooterCharacter) c;
			sc.setWeapon(new Shotgun());
		}
	}

	@Override
	public float getSize() {
		return 0.2f;
	}

	@Override
	protected void concreteClean() {
		// TODO Auto-generated method stub
		
	}

	
	
}
