package tw.entities.allies;

import java.awt.Graphics2D;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.EntityType.Faction;
import tw.entities.Entity;
import tw.entities.IAControlledCharacter;
import tw.entities.TestCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;
import tw.weapons.MachinegunWeapon;

public class TestTank extends TestCharacter{
	
	private IAControlledCharacter pilot;
	public TestTank(Map map, float firstX, float firstY) {
		this(null,map,firstX, firstY);
	}
	public TestTank(IAControlledCharacter pilot,Map map, float firstX, float firstY) {
		super(new MachinegunWeapon(),map, 15,Faction.MARINE, firstX, firstY);
		this.pilot=pilot;
	}
	
	public TestTank(IAControlledCharacter pilot,Map map,Astar a,List<IAControlledCharacter> list, float firstX, float firstY) {
		super(new MachinegunWeapon(),map,a,list, 10,Faction.MARINE, firstX, firstY);
		this.pilot=pilot;
	}
	
@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) {
		dm.drawCharacterUsingMetrics(this, Metrics2D.TANK, g);
	}
	@Override
	public float getSize() {
		return 1.33f;
	}
	@Override
	protected boolean collidesWith(Entity e) {
		return true;
	}
	@Override
	public boolean occludesSameFactionBullets()
	{
		return true;
	}
	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		return 1.5f;
	}
	@Override
	public IAControlledCharacter onDeath(Level l) {
		AudioManager.getInstance().playSound(Sound.EXPLOSION);
		//l.addAnimation(new DeathAnimation(this.getX(),this.getY(),this.getSize(),this.getLookingDirection(),0.15f,1.5f,Metrics2D.SOLDIER));
		l.addUpperAnimation(new DefaultAnimation(this.getX()+this.getSize()/2,this.getY()+this.getSize()/2,AnimationMetrics2D.BIGEXPLOSION,true));
		if(pilot!=null) this.pilot.setXY(this.getX(), this.getY());
		return this.pilot;
	}

}
