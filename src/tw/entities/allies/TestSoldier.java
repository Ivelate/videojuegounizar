package tw.entities.allies;

import java.awt.Graphics2D;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.entities.EntityType.Faction;
import tw.entities.IAControlledCharacter;
import tw.entities.TestCharacter;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.gui.Metrics2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DeathAnimation;
import tw.gui.animation.DefaultAnimation;

public class TestSoldier extends TestCharacter {
	List<IAControlledCharacter> list;
	
	public TestSoldier(Map map, float firstX, float firstY) {
		super(map, 2,Faction.MARINE, firstX, firstY);
	}
	public TestSoldier(Map map,Astar astar,List<IAControlledCharacter> list,float firstX, float firstY) {
		super(map,astar,list, 2,Faction.MARINE, firstX, firstY);
		this.list=list;
	}
	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g,float tEl) {
		dm.drawCharacterUsingMetrics(this, Metrics2D.SOLDIER, g);
	}
	@Override
	public float getSize() {
		return 0.733333f;
	}
@Override
	public IAControlledCharacter onDeath(Level l) {
		l.addAnimation(new DeathAnimation(this.getX(),this.getY(),this.getSize(),this.getLookingDirection(),0.15f,1.5f,Metrics2D.SOLDIER));
		return null;
	}
	@Override
	protected void onShootReceivedAt(float x, float y, Level l) 
	{
		l.addUpperAnimation(new DefaultAnimation(x,y,AnimationMetrics2D.BLOOD,true));
		
	}
	@Override
	public float getConcreteVel() {
		// TODO Auto-generated method stub
		return 2.4f;
	}

}
