package tw.entities;

import tw.IA.Util.Astar.Astar;
import tw.game.Level;
import tw.game.Map;
import tw.weapons.Weapon;

public abstract class ShooterCharacter extends IAControlledCharacter
{
	private Weapon currentWeapon;
	private Weapon secondaryWeapon;
	private boolean shooting=false;
	private boolean shootingSecondary=false;
	
	public ShooterCharacter(Weapon defaultWeapon,int life, Map map, float firstX, float firstY) {
		this(defaultWeapon,null,life,map,firstX,firstY);
	}
	
	public ShooterCharacter(Weapon defaultWeapon,Weapon secondaryWeapon,int life, Map map, float firstX, float firstY) {
		super(life, map, firstX, firstY);
		this.currentWeapon=defaultWeapon;
		this.secondaryWeapon=secondaryWeapon;
	}
	
	public ShooterCharacter(Weapon defaultWeapon, int life,Map map, Astar astar, float firstX, float firstY) {
		super(life, map,astar, firstX, firstY);		
		this.currentWeapon=defaultWeapon;
	}
	public ShooterCharacter(Weapon defaultWeapon,Weapon secondaryWeapon,int life, Map map,Astar astar,float firstX, float firstY) {
		super(life, map,astar, firstX, firstY);
		this.currentWeapon=defaultWeapon;
		this.secondaryWeapon=secondaryWeapon;
	}
	@Override
	protected final void updatePre(float tEl, Level l) {
		updatePre2(tEl,l);
		if(this.shooting) {
			if(this.currentWeapon.reloadAndShootFrom(this, l, tEl)) onShoot();
		}
		else this.currentWeapon.reload(tEl);
		if(this.secondaryWeapon!=null)
		{
			if(this.shootingSecondary) {
				if(this.secondaryWeapon.reloadAndShootFrom(this, l, tEl)) onShoot();
			}
			else this.secondaryWeapon.reload(tEl);
		}
	}
	
	protected abstract void onShoot();
	protected abstract void updatePre2(float tEl,Level l);
	
	public void setShootingTo(boolean val)
	{
		this.shooting=val;
	}
	public void setShootingSecondaryTo(boolean val)
	{
		this.shootingSecondary=val;
	}
	public boolean isShooting()
	{
		return this.shooting;
	}
	public boolean isShootingSecondary()
	{
		return this.shootingSecondary;
	}
	public void setWeapon(Weapon w)
	{
		this.currentWeapon=w;
	}
	public void setSecondaryWeapon(Weapon w)
	{
		this.secondaryWeapon=w;
	}
	public boolean hasSecondaryWeapon()
	{
		return this.secondaryWeapon!=null;
	}
	public Weapon getPrimaryWeapon()
	{
		return this.currentWeapon;
	}
	public Weapon getSecondaryWeapon()
	{
		return this.secondaryWeapon;
	}
}
