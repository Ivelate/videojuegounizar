package tw.entities;

public class EntityType 
{
	public enum Role {GROUND,AIR,BULLET, OTHER}
	public enum Faction {MARINE,ALIEN,REBEL,OTHER}
	
	private Role role;
	private Faction faction;
	public EntityType(Role role,Faction faction)
	{
		this.role=role;
		this.faction=faction;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Faction getFaction() {
		return faction;
	}
	public void setFaction(Faction faction) {
		this.faction = faction;
	}
}
