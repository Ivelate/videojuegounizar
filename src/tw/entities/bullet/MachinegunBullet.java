package tw.entities.bullet;

import java.awt.Graphics2D;

import tw.entities.Bullet;
import tw.entities.Character.Direction;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DirectedAnimationMetrics2D;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;
import tw.gui.animation.DefaultDirectedAnimation;

public class MachinegunBullet extends Bullet
{
	private final static float scaledSize=(float)(DirectedAnimationMetrics2D.NORMALBULLET.SIZE)/DrawingManager2D.TILE_SIZE;
	private final static float BULLET_DURATION=5f;
	
	public MachinegunBullet(float ix, float iy, float vx, float vy,Faction faction, Map m,Level l) 
	{
		super(ix, iy, vx, vy, BULLET_DURATION, faction, m,l);
		l.addUpperAnimation(new DefaultDirectedAnimation(this.getX(),this.getY(),DirectedAnimationMetrics2D.BULLETSPAWN,this.getBulletDirection(),true));
	}

	@Override
	public int getDamage() {
		return 1;
	}

	@Override
	public float getSize() {
		return scaledSize;
	}

	@Override
	public DirectedAnimationMetrics2D getMetrics() {
		return DirectedAnimationMetrics2D.NORMALBULLET;
	}
	@Override
	protected void onExpiration(boolean collidedWithUnit, Level l) {
		l.addUpperAnimation(new DefaultAnimation(this.getX(),this.getY(),AnimationMetrics2D.MACHINEGUN_EXP,true));
	}
}
