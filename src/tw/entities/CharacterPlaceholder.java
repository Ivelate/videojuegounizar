package tw.entities;

import java.awt.Graphics2D;
import java.util.List;

import tw.IA.Util.Astar.Astar;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DrawingManager2D;
import tw.weapons.EmptyWeapon;

public class CharacterPlaceholder extends ShooterCharacter{

	private IAControlledCharacter placingEntity;
	public CharacterPlaceholder(IAControlledCharacter placingEntity,Map map) {
		super(new EmptyWeapon(), Integer.MAX_VALUE, map, placingEntity.getX(), placingEntity.getY());
		this.placingEntity=placingEntity;
	}
	
	public CharacterPlaceholder(IAControlledCharacter placingEntity,Map map,Astar a) {
		super(new EmptyWeapon(), Integer.MAX_VALUE, map, a,placingEntity.getX(), placingEntity.getY());
		this.placingEntity=placingEntity;
	}

	@Override
	public boolean occludesSameFactionBullets() {
		return false;
	}

	@Override
	public void concreteThink() {
	}

	@Override
	public float getConcreteVel() {
		return 0;
	}

	@Override
	public boolean canBeShootedConcrete() {
		return false;
	}

	@Override
	public IAControlledCharacter onDeath(Level l) {
		return this.placingEntity;
	}

	@Override
	public float getSize() {
		return this.placingEntity.getSize();
	}

	@Override
	public EntityType getType() {
		return this.placingEntity.getType();
	}

	@Override
	protected void concreteClean() {
	}

	@Override
	protected boolean collidesWith(Entity e) {
		return this.placingEntity.collidesWith(e);
	}

	@Override
	protected void onShoot() {
	}

	@Override
	protected void updatePre2(float tEl, Level l) {
	}

	@Override
	protected void onShootReceivedAt(float x, float y, Level l) {
	}

	@Override
	public void concreteDraw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
	}

}
