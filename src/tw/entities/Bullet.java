package tw.entities;

import java.awt.Graphics2D;

import tw.entities.Character.Direction;
import tw.entities.EntityType.Faction;
import tw.entities.EntityType.Role;
import tw.game.Level;
import tw.game.Map;
import tw.gui.DirectedAnimationMetrics2D;
import tw.gui.DrawingManager2D;
import tw.tiles.Tile.TileState;
import tw.utils.Cleanable;
import tw.utils.Drawable2D;
import tw.utils.Shootable;

/**
 * Bullet class. Contains the position and velocity of a bullet, travelling trough the map. Collides with terrain
 * and shootable entities.
 */
public abstract class Bullet implements Cleanable, Drawable2D 
{
	private float vx;
	private float vy;
	private float xpos;
	private float ypos;
	private Map map;
	private Level level;
	private float lifetime;
	private float timeAlive=0;
	private EntityType bulletType;
	
	private Direction bulletDirection;
	
	
	public Bullet(float ix,float iy,float vx,float vy,float lifetime,Faction faction,Map m,Level l)
	{
		this.map=m;
		this.level=l;
		this.vx=vx;
		this.vy=vy;
		this.xpos=ix;
		this.ypos=iy;
		this.lifetime=lifetime;
		this.bulletType=new EntityType(Role.BULLET,faction);
		
		if(vy==0){
			if(vx>0) this.bulletDirection=Direction.RIGHT;
			else this.bulletDirection=Direction.LEFT;
		}
		else if(vy>0){
			float dif=vy-vx;
			if(dif>3*vy) this.bulletDirection=Direction.LEFT;
			else if(dif>1.5f*vy) this.bulletDirection=Direction.DOWNLEFT;
			else if(dif>0.5f*vy) this.bulletDirection=Direction.DOWN;
			else if(dif>-vy) this.bulletDirection=Direction.DOWNRIGHT;
			else this.bulletDirection=Direction.RIGHT;
		}
		else{
			float dif=-vy-vx;
			if(dif>3*-vy) this.bulletDirection=Direction.LEFT;
			else if(dif>1.5f*-vy) this.bulletDirection=Direction.UPLEFT;
			else if(dif>0.5f*-vy) this.bulletDirection=Direction.UP;
			else if(dif>vy) this.bulletDirection=Direction.UPRIGHT;
			else this.bulletDirection=Direction.RIGHT;
		}
	}
	
	/**
	 * Moves the bullet the position corresponding to the elapsed time <tEl>. This method is FPS safe: If tEl is too high, it will
	 * simulate the collisions trough the intermediate tiles the bullet has to traverse.
	 */
	public boolean move(float tEl)
	{
		boolean expires=false;
		float lastTimeAlive=this.timeAlive;
		this.timeAlive+=tEl;
		
		if(this.timeAlive>=this.lifetime){
			expires=true;
			tEl=tEl+this.lifetime-this.timeAlive;
		}
		
		boolean collidedWithUnit=false;
		boolean ended=false;
		TileState currentTileState;
		while(!ended)
		{
			float colTime=getCollisionTimeFor(this.xpos,this.ypos,this.vx,this.vy,this.bulletType,this.map);
			if(colTime>0)
			{
				float timpx=this.vx==0 ? Float.MAX_VALUE :
							this.vx>0  ? (((int)(this.xpos)+1)-this.xpos)/this.vx :
										 -(0.0001f+this.xpos-((int)this.xpos))/this.vx;
				
				float timpy=this.vy==0 ? Float.MAX_VALUE :
							this.vy>0  ? (((int)(this.ypos)+1)-this.ypos)/this.vy :
										 -(0.0001f+this.ypos-((int)this.ypos))/this.vy;
				
				float advtime;
				if(timpx>timpy) advtime=timpy;
				else advtime=timpx;
				
				if(advtime>tEl){
					advtime=tEl;
					ended=true;
				}
				if(advtime>colTime){
					advtime=colTime;
					ended=true;
					expires=true;
				}
				else tEl-=advtime;
				
				float tNearestEntityCollision;
				if(lastTimeAlive>0.001f){
					tNearestEntityCollision=findAndShootNearestEntity(this.xpos,this.ypos,this.vx,this.vy,advtime,map);
				}
				else tNearestEntityCollision=-1;
				if(tNearestEntityCollision>=0){
					advtime=tNearestEntityCollision;
					ended=true;
					expires=true;
					collidedWithUnit=true;
				}
				else{
					this.xpos+=this.vx*advtime;
					this.ypos+=this.vy*advtime;
				}
			}
			else
			{
				expires=true;
				ended=true;
			}
		}
		
		if(expires) onExpiration(collidedWithUnit,this.level);
		return expires;
	}
	
	/**
	 * Returns how much travelling time is needed for this bullet to collide with the terrain tile it's currently in.
	 */
	private static float getCollisionTimeFor(float ix,float iy,float vx,float vy,EntityType et,Map map)
	{
		TileState tileState=map.getTileAt(ix, iy).getLimitsFor(et);
		ix=ix-(int)(ix);
		iy=iy-(int)(iy);
		if(tileState==TileState.FULL) return -1f;
		else if(tileState==TileState.EMPTY) return Float.MAX_VALUE;
		
		float timTillCol=0;
		switch(tileState)
		{
		case LD_CORNER:
			if(ix-iy<0) return -1;
			timTillCol=(ix-iy)/(vy-vx);
			break;
		case LU_CORNER:
			if(ix+iy<1) return -1;
			timTillCol=(1-iy-ix)/(vx+vy);	
			break;
		case RD_CORNER:
			if(ix+iy>1) return -1;
			timTillCol=(1-iy-ix)/(vx+vy);
			break;
		case RU_CORNER:
			if(ix-iy>0) return -1;
			timTillCol=(ix-iy)/(vy-vx);
			break;
		case L_HALF:
			if(ix<=0.5f) return -1;
			timTillCol=(ix-0.5f)/(-vx);
			break;
		case R_HALF:
			if(ix>=0.5f) return -1;
			timTillCol=(0.5f-ix)/(vx);
			break;
		case U_HALF:
			if(iy<=0.5f) return -1;
			timTillCol=(iy-0.5f)/(-vy);
			break;
		case D_HALF:
			if(iy>=0.5f) return -1;
			timTillCol=(0.5f-iy)/(vy);
			break;
		}
		
		if(timTillCol>0) return timTillCol;
		else return Float.MAX_VALUE;
	}
	
	private float findAndShootNearestEntity(float ix,float iy,float vx,float vy,float tAdv,Map map)
	{
		float time=-1;
		Shootable nearest=null;
		float fx=ix+vx*tAdv;
		float fy=iy+vy*tAdv;
		
		for(Entity e:map.getEntitiesAt(ix, iy))
		{
			//System.out.println(ix+" "+iy+" "+fx+" "+fy);
			//System.out.println("-> "+e.getX()+" "+e.getY()+" "+e.getX()+e.getSize()+" "+e.getY()+e.getSize());
			if(e instanceof Shootable)
			{
				Shootable s=(Shootable)e;
				if(s.canBeShooted() && (s.getFaction()!=this.getFaction() || s.occludesSameFactionBullets()))
				{
					float entTime=-1;
					//Calc dist
					if(vx>0)
					{
						if(vy>0){
							if(ix<e.getX()+e.getSize()&&iy<e.getY()+e.getSize()&&fx>e.getX()&&fy>e.getY()) {
								float dx=(e.getX()-ix)/vx; if(dx<0) dx=0;
								float dy=(e.getY()-iy)/vy; if(dy<0) dy=0;
								entTime= (float)(Math.min(dx, dy));
							}
						}
						else{
							if(ix<e.getX()+e.getSize()&&fy<e.getY()+e.getSize()&&fx>e.getX()&&iy>e.getY()) {
								float dx=(e.getX()-ix)/vx; if(dx<0) dx=0;
								float dy=(e.getY()+e.getSize()-iy)/vy; if(dy<0) dy=0;
								entTime= (float)(Math.min(dx, dy));
							}
						}
					}
					else
					{
						if(vy>0){
							if(fx<e.getX()+e.getSize()&&iy<e.getY()+e.getSize()&&ix>e.getX()&&fy>e.getY()) {
								float dx=(e.getX()+e.getSize()-ix)/vx; if(dx<0) dx=0;
								float dy=(e.getY()-iy)/vy; if(dy<0) dy=0;
								entTime= (float)(Math.min(dx, dy));
							}
						}
						else{
							if(fx<e.getX()+e.getSize()&&fy<e.getY()+e.getSize()&&ix>e.getX()&&iy>e.getY()) {
								float dx=(e.getX()+e.getSize()-ix)/vx; if(dx<0) dx=0;
								float dy=(e.getY()+e.getSize()-iy)/vy; if(dy<0) dy=0;
								entTime= (float)(Math.min(dx, dy));
							}
						} 
					}
					
					if(entTime>=0 && (entTime<time || time<0)){
						time=entTime;
						nearest=s;
						if(entTime<=0) break;
					}
				}
			}
		}
		
		if(nearest!=null && nearest.getFaction()!=this.getFaction())  {
			this.xpos+=this.vx*time;
			this.ypos+=this.vy*time;
			nearest.receiveShoot(this,this.level);
		}
		return time;
	}

	@Override
	public void clean() {
		//Bullets will be cleaned in the iterator, hence there is no need to tell the EM to delete them here
		
	}
	
	public Faction getFaction()
	{
		return this.bulletType.getFaction();
	}
	public float getX()
	{
		return this.xpos;
	}
	public float getY()
	{
		return this.ypos;
	}
	//TODO abstract in the future
	public abstract int getDamage();
	
	public Direction getBulletDirection()
	{
		return this.bulletDirection;
	}
	public float getTimeAlive()
	{
		return this.timeAlive;
	}
	
	@Override
	public void draw2D(DrawingManager2D dm, Graphics2D g, float tEl) {
		dm.drawAnimation((int)((this.getTimeAlive()/this.getMetrics().ANIMMOD)%this.getMetrics().ANIMNUM), this.getX()-(this.getSize()/2), this.getY()-(this.getSize()/2), this.getSize(), this.getMetrics(), getBulletDirection(), g);
	}
	
	public abstract float getSize();
	public abstract DirectedAnimationMetrics2D getMetrics();
	protected abstract void onExpiration(boolean collidedWithUnit,Level l);
}