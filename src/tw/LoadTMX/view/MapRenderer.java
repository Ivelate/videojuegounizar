package tw.LoadTMX.view;

import java.awt.Dimension;
import java.awt.Graphics2D;

import tw.LoadTMX.core.TileLayer;

/**
 * An interface defining methods to render a map.
 */
public interface MapRenderer
{
    /**
     * Calculates the dimensions of the map this renderer applies to.
     *
     * @return the dimensions of the given map in pixels
     */
    public Dimension getMapSize();

    /**
     * Paints the given tile layer, taking into account the clip rect of the
     * given graphics context.
     *
     * @param g     the graphics context to paint to
     * @param layer the layer to paint
     */
    public void paintTileLayer(Graphics2D g, TileLayer layer);
}
