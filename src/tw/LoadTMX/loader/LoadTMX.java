package tw.LoadTMX.loader;

import java.io.File;
import java.io.IOException;

import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class LoadTMX {
	
	private Document doc;
	private int Width,Height,mapTileWidth,mapTileHeight,tileWidth,tileHeight;
	
	public LoadTMX(String ruta){
		
		try {
			File fichero= new File(ruta);
			DocumentBuilderFactory dbFactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			this.doc = dBuilder.parse(fichero);
			doc.getDocumentElement().normalize();
			GetMapAtributes();
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Cargamos las propiedades del mapa (altura, anchura, n tiles y su tama�o)
	 */
	private void GetMapAtributes(){
		NodeList n= doc.getElementsByTagName("map");
		for(int i=0;i<n.getLength();i++){
			Node nodo=n.item(i);
			if(nodo.getNodeType()== Node.ELEMENT_NODE){
				Element element=(Element) nodo;
				this.mapTileWidth=Integer.parseInt(element.getAttribute("width"));
				this.mapTileHeight=Integer.parseInt(element.getAttribute("height"));
				
				this.tileWidth=Integer.parseInt(element.getAttribute("tilewidth"));
				this.tileHeight=Integer.parseInt(element.getAttribute("tileheight"));
			}
		}
		this.setWidth(mapTileWidth*tileWidth);
		this.setHeight(mapTileHeight*tileHeight);
	}


	public int getWidth() {
		return Width;
	}


	public void setWidth(int width) {
		Width = width;
	}


	public int getHeight() {
		return Height;
	}


	public void setHeight(int height) {
		Height = height;
	}

}
