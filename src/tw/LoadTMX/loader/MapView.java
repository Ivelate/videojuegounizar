package tw.LoadTMX.loader;

import java.awt.Color;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import tw.LoadTMX.core.Map;
import tw.LoadTMX.core.MapLayer;
import tw.LoadTMX.core.ObjectGroup;
import tw.LoadTMX.core.TileLayer;
import tw.LoadTMX.view.MapRenderer;
import tw.LoadTMX.view.OrthogonalRenderer;


/**
 * Clase que permite visualizar un mapa en un panel
 * @author joseangel
 *
 */
public class MapView extends JPanel implements Scrollable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map map;
    private MapRenderer renderer;

    public MapView(Map map) throws IOException{
        this.map = map;
        renderer = createRenderer(map);

        setPreferredSize(renderer.getMapSize());
        setOpaque(true);
    }

    public MapRenderer GetRender(){
    	return this.renderer;
    }
    
    public void paintComponent(Graphics g) {
        final Graphics2D g2d = (Graphics2D) g.create();
        final Rectangle clip = g2d.getClipBounds();

        // Draw a gray background
        g2d.setPaint(new Color(100, 100, 100));
        g2d.fill(clip);

        // Draw each tile map layer
        for (MapLayer layer : map) {
            if (layer instanceof ObjectGroup) {
                ObjectGroup o = (ObjectGroup) layer;
                System.out.println(o.getName());
            }
            
                if (layer instanceof TileLayer) {
                renderer.paintTileLayer(g2d, (TileLayer) layer);
            }
        }


//        int x=0;
//        int y=0;
        
    }

    private static MapRenderer createRenderer(Map map) {
        switch (map.getOrientation()) {
            case Map.ORIENTATION_ORTHOGONAL:
                return new OrthogonalRenderer(map);
            default:
                return null;
        }
    }

    public Dimension getPreferredScrollableViewportSize() {
        return getPreferredSize();
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect,
                                          int orientation, int direction) {
        if (orientation == SwingConstants.HORIZONTAL)
            return map.getTileWidth();
        else
            return map.getTileHeight();
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect,
                                           int orientation, int direction) {
        if (orientation == SwingConstants.HORIZONTAL) {
            final int tileWidth = map.getTileWidth();
            return (visibleRect.width / tileWidth - 1) * tileWidth;
        } else {
            final int tileHeight = map.getTileHeight();
            return (visibleRect.height / tileHeight - 1) * tileHeight;
        }
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    public boolean getScrollableTracksViewportHeight() {
        return false;
    }
}
