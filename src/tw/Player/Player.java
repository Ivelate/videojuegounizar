package tw.Player;

import java.awt.Graphics;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;

import javax.swing.JComponent;

import tw.Characters.Animation.AnimationControl;
import tw.Characters.Animation.AnimationsLoader;

public class Player extends JComponent implements KeyListener {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int x_pos;
	private int y_pos;
	private AnimationsLoader Sprites;	
	private  Actions accionActual;
	private AnimationControl animationControl;
	
	
	
	private Hashtable<Integer, Boolean> botones_pulsados;
	
	public Player(AnimationsLoader Sprites,AnimationControl control_animacion){
		//this.push_buttons=0b0000;
		this.Sprites=Sprites;
		this.x_pos=(getWidth()/2)-32;
		this.y_pos=(getHeight()/2)-32;
		this.botones_pulsados=new Hashtable<Integer,Boolean>();
				botones_pulsados.put(KeyEvent.VK_W, false);
				botones_pulsados.put(KeyEvent.VK_A, false);
				botones_pulsados.put(KeyEvent.VK_S, false);
				botones_pulsados.put(KeyEvent.VK_D, false);
		
				
		this.accionActual=Actions.parado;
		this.animationControl=control_animacion;
		control_animacion.start();
		
	}

	

	@Override
	public void keyPressed(KeyEvent e) {
		botones_pulsados.put(e.getKeyCode(), true);
		Actualizar();
		
	}
	
	private void Actualizar(){
		
		if(botones_pulsados.get(KeyEvent.VK_W) && botones_pulsados.get(KeyEvent.VK_D) ) {
			y_pos-=3;
			x_pos+=3;
			accionActual=Actions.caminar_arriba_derecha;
		}
		
		
		else if(botones_pulsados.get(KeyEvent.VK_W) && botones_pulsados.get(KeyEvent.VK_A) ) {
			y_pos-=3;
			x_pos-=3;
			accionActual=Actions.caminar_arriba_izq;
		}	
		
		else if(botones_pulsados.get(KeyEvent.VK_S) && botones_pulsados.get(KeyEvent.VK_A) ) {
			y_pos+=3;
			x_pos-=3;
			accionActual=Actions.caminar_abajo_izq;
		}	
		
		else if(botones_pulsados.get(KeyEvent.VK_S) && botones_pulsados.get(KeyEvent.VK_D) ) {
			y_pos+=3;
			x_pos+=3;
			accionActual=Actions.caminar_abajo_drch;
		}	
		
		else if(botones_pulsados.get(KeyEvent.VK_W)) {
			y_pos-=3;
			accionActual=Actions.caminar_arriba;
		}
		else if(botones_pulsados.get(KeyEvent.VK_A)){
			x_pos-=3; 
			accionActual=Actions.caminar_izquierda;
		}
		else if(botones_pulsados.get(KeyEvent.VK_S)){
			y_pos+=3;
			accionActual=Actions.caminar_abajo;
		}	
		else if(botones_pulsados.get(KeyEvent.VK_D)){
			x_pos+=3;
			accionActual=Actions.caminar_derecha;
		}
		animationControl.animate(Sprites.GetAnimation(accionActual.GetValue()-1));
		
	}
		


	@Override
	public void keyReleased(KeyEvent e) {
		botones_pulsados.put(e.getKeyCode(), false);
		Actualizar();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}	
	
	public void dibujar(Graphics g){
		g.drawImage(Sprites.getFrame(accionActual.GetValue()-1, animationControl.GetFrame())
				, x_pos,y_pos, null);
	}

}
