package tw.Player;


public enum Actions{
	parado(1),
	caminar_abajo(2),
	caminar_abajo_izq(1),
	caminar_abajo_drch(3),
	caminar_izquierda(4),
	caminar_arriba_izq(5),
	caminar_derecha(6),
	caminar_arriba_derecha(7),
	caminar_arriba(8);
	
	
	private int value;
	
	private Actions(int i){
		this.value=i;
	}
	
	public void SetValue(int n){
		this.value=n;
	}
	public int GetValue(){
		return value;
	}


}
	
	
