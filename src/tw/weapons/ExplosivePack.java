package tw.weapons;

import tw.entities.ShooterCharacter;
import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public class ExplosivePack extends Weapon{
	private static final float DEFAULT_MAX_BULLET_VEL=10;
	private static final float DEFAULT_MAX_BULLET_LIFE=2f;
	
	public ExplosivePack() {
		super(1f);
	}

	@Override
	protected float performShoot(ShooterCharacter sc, Level l, float tEl) {
		for(int b=0;b<5000;b++) {
			double ang=Math.random()*2*Math.PI;
			float vel=(float)(DEFAULT_MAX_BULLET_VEL*Math.random());
			spawnBullet(sc.getX()+sc.getSize()/2,sc.getY()+sc.getSize()/2,(float)Math.cos(ang)*vel,(float)Math.sin(ang)*vel,sc.getFaction(),l);
		}
		return 0;
	}
	
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		l.spawnBullet(Faction.OTHER, ix, iy, vx,vy,BULLET_TYPE.PLASMA);//(float)(DEFAULT_MAX_BULLET_LIFE/2)+(float)(Math.random()*DEFAULT_MAX_BULLET_LIFE/2));
	}
}
