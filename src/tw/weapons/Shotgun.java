package tw.weapons;

import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public class Shotgun extends BulletWeapon{

	private static final float DEFAULT_BULLET_VEL=10;
	private static final float DEFAULT_SHOOT_DELAY=0.6f;
	
	public Shotgun() {
		super(DEFAULT_BULLET_VEL, DEFAULT_SHOOT_DELAY);
	}

	@Override
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		float ymod=0.2f*vx;
		float xmod=0.2f*vy;
		l.spawnBullet(f, ix, iy, vx,vy,BULLET_TYPE.PLASMA);
		l.spawnBullet(f, ix, iy, vx+xmod,vy-ymod,BULLET_TYPE.PLASMA);
		l.spawnBullet(f, ix, iy, vx-xmod,vy+ymod,BULLET_TYPE.PLASMA);
	}

}