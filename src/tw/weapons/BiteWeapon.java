package tw.weapons;

import tw.entities.Character.Direction;
import tw.entities.CharacterStatModifier;
import tw.entities.ShooterCharacter;
import tw.entities.IAControlledCharacter;
import tw.game.Level;
import tw.gui.animation.AnimationMetrics2D;

public class BiteWeapon extends Weapon
{

	public BiteWeapon() {
		super(1f);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected float performShoot(ShooterCharacter sc, Level l, float tEl) {
		final Direction lookingDir=sc.getLookingDirection();
		sc.setStatModifier(new CharacterStatModifier(0.3f){
			@Override
			public float getSpeedModifier() {
				return 4*(this.maxLifetime-this.lifetime)/this.maxLifetime;
			}
			@Override
			public Direction getDirectionLock() {
				return lookingDir;
			}
			//|TODO chapuza. No deberia spawnear sangre humana, sino la sangre por defecto del caracter correspondiente
			@Override
			public void concreteUpdate(float tEl,Level l,IAControlledCharacter source)
			{
				l.spawnExplosion(null,AnimationMetrics2D.BLOOD, source.getX()+source.getSize()/2, source.getY()+source.getSize()/2,( source.getSize()/2)-0.01f, 5, source.getFaction());
			}
		});
		return 0;
	}

}
