package tw.weapons;

import tw.entities.ShooterCharacter;
import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public class UltimaWeapon extends Weapon{

	private static final float DEFAULT_MAX_BULLET_VEL=10;
	private static final float DEFAULT_SPAWN_TIME=0.0005f;
	
	public UltimaWeapon() {
		super(DEFAULT_SPAWN_TIME);
	}

	@Override
	protected float performShoot(ShooterCharacter sc, Level l, float tEl) {
		for(float b=0;b<tEl;b+=DEFAULT_SPAWN_TIME) spawnBullet(sc.getX()+sc.getSize()/2,sc.getY()+sc.getSize()/2,(float)Math.random()*DEFAULT_MAX_BULLET_VEL*2 - DEFAULT_MAX_BULLET_VEL,(float)Math.random()*DEFAULT_MAX_BULLET_VEL*2 -DEFAULT_MAX_BULLET_VEL,sc.getFaction(),l);
		return 0;
	}
	
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		l.spawnBullet(f, ix, iy, vx,vy,BULLET_TYPE.PLASMA);
	}

}