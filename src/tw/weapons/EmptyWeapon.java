package tw.weapons;

import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;

public class EmptyWeapon extends Weapon
{
	public EmptyWeapon() {
		super(Float.MAX_VALUE);
	}

	@Override
	protected float performShoot(ShooterCharacter sc, Level l, float tEl) {
		return 0;
	}

}
