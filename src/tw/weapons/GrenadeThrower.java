package tw.weapons;

import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;

public class GrenadeThrower extends BulletWeapon{

	private static final float GRENADE_VEL=5;
	private static final float GRENADE_DELAY=1;
	public GrenadeThrower(int initialAmmo) {
		super(GRENADE_VEL, GRENADE_DELAY,initialAmmo);
	}

	@Override
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		l.addUpperAnimation(new Grenade(ix,iy,vx,vy,f));
		
	}

	@Override
	public int getMaxAmmo() {
		return 8;
	}



}
