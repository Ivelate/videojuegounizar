package tw.weapons;

import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public class MachinegunWeapon extends BulletWeapon{

	private static final float DEFAULT_BULLET_VEL=10;
	private static final float DEFAULT_SHOOT_DELAY=0.3f;
	
	public MachinegunWeapon() {
		super(DEFAULT_BULLET_VEL, DEFAULT_SHOOT_DELAY);
	}

	@Override
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		l.spawnBullet(f, ix, iy, vx,vy,BULLET_TYPE.MACHINEGUN);
	}

}