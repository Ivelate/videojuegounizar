package tw.weapons;

import tw.entities.ShooterCharacter;
import tw.game.Level;

public abstract class Weapon 
{
	private float maxShootDelay;
	private float currentShootDelay;
	private int currentAmmo;
	public Weapon(float shootDelay)
	{
		this(shootDelay,-1); //Unlimited ammo
	}
	public Weapon(float shootDelay,int ammo)
	{
		this.maxShootDelay=shootDelay;
		this.currentShootDelay=shootDelay;
		this.currentAmmo=ammo;
	}
	public boolean reload(float tEl)
	{
		this.currentShootDelay-=tEl;
		return this.currentShootDelay<=0;
	}
	private void shootFrom(ShooterCharacter sc,Level l,float tEl)
	{
		float timeSaved=performShoot(sc,l,tEl);
		this.currentShootDelay=this.maxShootDelay-timeSaved;
	}
	protected abstract float performShoot(ShooterCharacter sc,Level l,float tEl);
	public boolean reloadAndShootFrom(ShooterCharacter sc,Level l,float tEl)
	{
		if(this.currentShootDelay<0) this.currentShootDelay=0;
		
		if(this.currentShootDelay-tEl<=0){
			if(this.currentAmmo!=0){
				if(this.currentAmmo>0) this.currentAmmo--;
				shootFrom(sc,l,tEl-this.currentShootDelay);
				return true;
			}
		}
		else this.currentShootDelay-=tEl;
		return false;
	}
	public float getMaxShootDelay()
	{
		return this.maxShootDelay;
	}
	
	public int getCurrentAmmo()
	{
		return this.currentAmmo;
	}
	/**
	 * Weapons have unlimited ammo unless specifically overriden
	 */
	public int getMaxAmmo(){
		return -1;
	}
	public void reloadAmmo(int amount)
	{
		if(this.currentAmmo>=0) this.currentAmmo+=amount;
		if(this.currentAmmo>this.getMaxAmmo()&&this.getMaxAmmo()>0) this.currentAmmo=this.getMaxAmmo();
	}
	public void setAmmo(int amount)
	{
		if(this.currentAmmo>=0) this.currentAmmo=amount;
		if(this.currentAmmo>this.getMaxAmmo()&&this.getMaxAmmo()>0) this.currentAmmo=this.getMaxAmmo();
	}
}
