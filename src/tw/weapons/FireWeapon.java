package tw.weapons;

import tw.entities.EntitySpawner.BULLET_TYPE;
import tw.entities.EntityType.Faction;
import tw.game.Level;

public class FireWeapon extends BulletWeapon{

	private static final float DEFAULT_BULLET_VEL=10;
	private static final float DEFAULT_SHOOT_DELAY=0.15f;
	
	public FireWeapon() {
		super(DEFAULT_BULLET_VEL, DEFAULT_SHOOT_DELAY);
	}

	@Override
	protected void spawnBullet(float ix, float iy, float vx, float vy,
			Faction f, Level l) {
		float ymod=0.5f*vx;
		float xmod=0.5f*vy;
		float rand=(float)(Math.random())-0.5f;
		l.spawnBullet(f, ix, iy, vx+(xmod*rand),vy-(ymod*rand),BULLET_TYPE.FIRE);
	}

}