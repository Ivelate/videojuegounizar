package tw.weapons;

import tw.entities.EntityType.Faction;
import tw.entities.ShooterCharacter;
import tw.game.Level;

public abstract class BulletWeapon extends Weapon
{
	private float bulletVel;
	public BulletWeapon(float bulletVel,float shootDelay) {
		super(shootDelay);
		this.bulletVel=bulletVel;
	}
	public BulletWeapon(float bulletVel,float shootDelay,int initialAmmo) {
		super(shootDelay,initialAmmo);
		this.bulletVel=bulletVel;
	}

	@Override
	protected float performShoot(ShooterCharacter sc, Level l, float tEl) {
		float ix=0;
		float iy=0;
		float vx=0;
		float vy=0;
		switch(sc.getLookingDirection())
		{
			case UP:
				ix=sc.getX()+sc.getSize()/2;
				iy=sc.getY()-0.001f;
				vy+=-1;
				break;
			case DOWN:
				ix=sc.getX()+sc.getSize()/2;
				iy=sc.getY()+sc.getSize()+0.001f;
				vy+=1;
				break;
			case LEFT:
				ix=sc.getX()-0.001f;
				iy=sc.getY()+sc.getSize()/2;
				vx+=-1;
				break;
			case RIGHT:
				ix=sc.getX()+sc.getSize()+0.001f;
				iy=sc.getY()+sc.getSize()/2;
				vx+=1;
				break;
			case UPRIGHT:
				ix=sc.getX()+sc.getSize()+0.001f;
				iy=sc.getY()-0.001f;
				vy+=-0.71f;
				vx+=0.71f;
				break;
			case UPLEFT:
				ix=sc.getX()-0.001f;
				iy=sc.getY()-0.001f;
				vy+=-0.71f;
				vx+=-0.71f;
				break;
			case DOWNRIGHT:
				ix=sc.getX()+sc.getSize()+0.001f;
				iy=sc.getY()+sc.getSize()+0.001f;
				vy+=0.71f;
				vx+=0.71f;
				break;
			case DOWNLEFT:
				ix=sc.getX()-0.001f;
				iy=sc.getY()+sc.getSize()+0.001f;
				vy+=0.71f;
				vx+=-0.71f;
				break;
		}
		vx*=this.bulletVel;
		vy*=this.bulletVel;
		float usedTime=0;
		for(float btim=0;btim<=tEl;btim+=this.getMaxShootDelay())
		{
			usedTime=btim;
			spawnBullet(ix+(vx*btim),iy+(vy*btim),vx,vy,sc.getFaction(),l);
		}
		
		return tEl-usedTime;
	}
	protected abstract void spawnBullet(float ix,float iy,float vx,float vy,Faction f,Level l);

}
