package tw.weapons;

import java.awt.Graphics2D;

import tw.audio.AudioManager;
import tw.audio.AudioManager.Sound;
import tw.entities.EntityType.Faction;
import tw.game.Level;
import tw.gui.DrawingManager2D;
import tw.gui.animation.AnimationMetrics2D;
import tw.gui.animation.DefaultAnimation;

public class Grenade extends DefaultAnimation
{
	private static final float DEFAULT_SPEED=5;
	private float ix;
	private float iy;
	private float vx;
	private float vy;
	private Faction faction;
	public Grenade(float x, float y,float ang,Faction faction) {
		this(x,y,(float)Math.cos(ang)*DEFAULT_SPEED,(float)Math.sin(ang)*DEFAULT_SPEED,faction);
	}
	public Grenade(float x, float y,float vx,float vy,Faction faction) {
		super(x, y, AnimationMetrics2D.GRENADE);
		this.ix=x;
		this.iy=y;
		this.vx=vx;
		this.vy=vy;
		this.faction=faction;
	}
	@Override
	public void onDispose(Level l) {
		AudioManager.getInstance().playSound(Sound.GRENADE);
		l.spawnExplosion(AnimationMetrics2D.GRENADE_EXPLOSION, this.x, this.y, 1.5f, 100, this.faction);	
	}
	@Override
	protected void concreteUpdate(float totalTimeElapsed, Level l) {
		this.x=ix+ vx*totalTimeElapsed;
		this.y=iy+ vy*totalTimeElapsed;
	}

}
