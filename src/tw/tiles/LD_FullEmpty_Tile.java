package tw.tiles;

import tw.entities.EntityType;

public class LD_FullEmpty_Tile extends Tile{

	public LD_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.LD_CORNER;
	}

}
