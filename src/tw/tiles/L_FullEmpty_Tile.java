package tw.tiles;

import tw.entities.EntityType;

public class L_FullEmpty_Tile extends Tile{

	public L_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.L_HALF;
	}

}
