package tw.tiles;

import tw.entities.EntityType;

public class EmptyTile extends Tile{

	public EmptyTile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.EMPTY;
	}

}
