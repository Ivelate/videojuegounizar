package tw.tiles;

import tw.entities.EntityType;

public class RU_FullEmpty_Tile extends Tile{

	public RU_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.RU_CORNER;
	}

}
