package tw.tiles;

import tw.entities.EntityType;

public class LU_FullEmpty_Tile extends Tile{

	public LU_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.LU_CORNER;
	}

}
