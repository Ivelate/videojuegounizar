package tw.tiles;

import tw.entities.EntityType;

public class RD_FullEmpty_Tile extends Tile{

	public RD_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.RD_CORNER;
	}

}
