package tw.tiles;

import tw.entities.EntityType;

public class R_FullEmpty_Tile extends Tile{

	public R_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.R_HALF;
	}

}
