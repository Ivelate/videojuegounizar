package tw.tiles;

import tw.entities.EntityType;

public abstract class Tile 
{
	public enum TileState {EMPTY,FULL,LU_CORNER,LD_CORNER,RU_CORNER,RD_CORNER,R_HALF,U_HALF,D_HALF,L_HALF}
	
	public final String name;
	public Tile(String name)
	{
		this.name=name;
	}
	public abstract TileState getLimitsFor(EntityType et);
	public final TileState getLimitsFor(EntityType.Role role,EntityType.Faction faction)
	{
		return getLimitsFor(new EntityType(role,faction));
	}
}
