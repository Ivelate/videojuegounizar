package tw.tiles;

import tw.entities.EntityType;

public class WaterTile extends Tile
{

	public WaterTile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		switch(et.getRole())
		{
		case GROUND:
			return TileState.FULL;
		case AIR:
			return TileState.EMPTY;
		case BULLET:
			return TileState.EMPTY;
		default:
			break;
		}
		return TileState.FULL;
	}

}
