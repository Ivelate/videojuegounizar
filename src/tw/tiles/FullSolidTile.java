package tw.tiles;

import tw.entities.EntityType;

public class FullSolidTile extends Tile{

	public FullSolidTile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) 
	{
		return TileState.FULL;
	}

}
