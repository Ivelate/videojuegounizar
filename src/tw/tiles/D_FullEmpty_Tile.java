package tw.tiles;

import tw.entities.EntityType;

public class D_FullEmpty_Tile extends Tile{

	public D_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.D_HALF;
	}

}
