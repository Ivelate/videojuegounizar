package tw.tiles;

import tw.entities.EntityType;

public class U_FullEmpty_Tile extends Tile{

	public U_FullEmpty_Tile(String name) {
		super(name);
	}

	@Override
	public TileState getLimitsFor(EntityType et) {
		return TileState.U_HALF;
	}

}
