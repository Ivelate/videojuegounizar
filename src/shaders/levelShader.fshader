#version 150
uniform sampler2D tiles;
uniform float alpha;
in vec3 Location;
in float TileID;
const vec4 fogcolor = vec4(0.7, 0.9, 1.0, 1.0);
const float fogdensity = .00001;

void main(){
vec4 outColor;
	/*if(Properties.y>0)
	{
		outColor=texture2D(tiles,
			vec2(
				(fract(Location.x)+Properties.x)/16,
				(fract(Location.z)+floor(((Properties.x)+0.001)/16))/16
				))*vec4(1.0,1.0,1.0,alpha);//*vec4(fract(Location.x/10),fract(Location.y/10),fract(Location.z/10),1.0);
		//outColor=vec4(Properties.y/128,Properties.y/128,Properties.y/128,1.0);
		/*int px=int(floor(Properties.x+0.01));
		int py=int(floor(Properties.y+0.01));
		int pz=int(floor(Properties.z+0.01));
		outColor=vec4(px%3,py%3,pz%3,1.0);
		//outColor=vec4(Properties.y/128,Properties.y/128,Properties.y/128,1.0);
		//if(Properties.y<15) {outColor=vec4(0.0,0.0,Properties.y/32,1.0);}
	}
	else
	{
		outColor=texture2D(tiles,
			vec2(
				((fract(Location.x+Location.z)+Properties.x)/16) +(1.0/1920),
				((1-fract(Location.y))+floor((Properties.x+0.001)/16))/16
				))*vec4(1.0,1.0,1.0,alpha);//*vec4(fract(Properties.x/10),fract(Properties.y/10),fract(Properties.z/10),1.0);
		//outColor=vec4(Properties.y/32,Properties.y/32,Properties.y/32,1.0);
	}*/
	outColor=texture(tiles,
			vec2(
				((fract(Location.x)+TileID)/16),
				(fract(Location.z)+floor(((TileID)+0.001)/16))/16
				));
	
	/*float z = gl_FragCoord.z / gl_FragCoord.w;
 	float fog = clamp(exp(-fogdensity * z * z), 0.2, 1);
  	gl_FragColor = mix(fogcolor, outColor, fog);*/
  	gl_FragColor=outColor;
  	//gl_FragColor = gl_FragColor - vec4(0.35,0.35,0.35,0.35);
  	//gl_FragColor =outColor;
}