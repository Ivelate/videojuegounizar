#version 150

uniform vec3 baseColor;
uniform vec3 colorDegrades;

in vec3 Location;

void main(){
	vec3 sum=Location.y*baseColor + (1-Location.y)*colorDegrades;
  	gl_FragColor=vec4(sum.xyz,1.0);
}