#version 150

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 location;

out vec3 Location;

void main(){
	Location=vec3(location.x,location.y,location.z);
	gl_Position=projectionMatrix * viewMatrix * modelMatrix * vec4(location.xyz,1.0);
}