#version 150

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

in vec3 location;
in float tileID;

out vec3 Location;
out float TileID;

void main(){
	//Properties=modelMatrix*vec4(properties.xyz,1.0);
	TileID=tileID;
	Location=vec3(location.x+0.001,location.y,location.z+0.001);
	gl_Position=projectionMatrix * viewMatrix * modelMatrix * vec4(location.xyz,1.0);
}