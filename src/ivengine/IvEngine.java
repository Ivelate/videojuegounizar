package ivengine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class IvEngine 
{
	public static void configDisplay(int screenWidth,int screenHeight,String title,boolean VSYNC,boolean resizable,boolean fullscreen) throws LWJGLException
	{
		Display.setTitle(title); //title of our window
		Display.setResizable(resizable); //whether our window is resizable
		Display.setDisplayMode(new DisplayMode(screenWidth,screenHeight)); //resolution of our display
		Display.setVSyncEnabled(VSYNC); //whether hardware VSync is enabled
		Display.setFullscreen(fullscreen); //whether fullscreen is enabled
		
		Display.create();
	}
}
