package ivengine.view;

import static org.lwjgl.opengl.GL20.glUniformMatrix4;
import static org.lwjgl.opengl.GL20.glUniform3f;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class MatrixHelper 
{
	public static final Vector3f zAxis=new Vector3f(0,0,1);
	public static final Vector3f yAxis=new Vector3f(0,1,0);
	public static final Vector3f xAxis=new Vector3f(1,0,0);
	public static final Matrix4f identity=new Matrix4f();
	
	private static FloatBuffer matrix44Buffer=BufferUtils.createFloatBuffer(16);
	private static FloatBuffer vec3Buffer=BufferUtils.createFloatBuffer(3);
	
	public static Matrix4f createProjectionMatrix(float znear,float zfar,float fov,float arat)
	{
		// Setup projection matrix
		Matrix4f projectionMatrix = new Matrix4f();

		float y_scale = coTangent(degreesToRadians(fov / 2f));
		float x_scale = y_scale / arat;
		float frustum_length = zfar - znear;

		projectionMatrix.m00 = x_scale;
		projectionMatrix.m11 = y_scale;
		projectionMatrix.m22 = -((zfar + znear) / frustum_length);
		projectionMatrix.m23 = -1;
		projectionMatrix.m32 = -((2 * znear * zfar) / frustum_length);
		projectionMatrix.m33 = 0;
		
		return projectionMatrix;
	}
	public static Vector4f multiply(Matrix4f op1,Vector4f op2)
	{
		return new Vector4f((op1.m00*op2.x)+(op1.m10*op2.y)+(op1.m20*op2.z)+(op1.m30*op2.w),
				(op1.m01*op2.x)+(op1.m11*op2.y)+(op1.m21*op2.z)+(op1.m31*op2.w),
				(op1.m02*op2.x)+(op1.m12*op2.y)+(op1.m22*op2.z)+(op1.m32*op2.w),
				(op1.m03*op2.x)+(op1.m13*op2.y)+(op1.m23*op2.z)+(op1.m33*op2.w));
	}
	public static void uploadMatrix(Matrix4f mat,int dest)
	{
		matrix44Buffer.rewind();mat.store(matrix44Buffer); matrix44Buffer.flip();
	    glUniformMatrix4(dest, false, matrix44Buffer);
	}
	public static void uploadVector(Vector3f vec,int dest)
	{
		glUniform3f(dest,vec.x,vec.y,vec.z);
	}
	private static float coTangent(float angle) {
		return (float)(1f / Math.tan(angle));
	}
	
	private static float degreesToRadians(float degrees) {
		return degrees * (float)(Math.PI / 180d);
	}
}
